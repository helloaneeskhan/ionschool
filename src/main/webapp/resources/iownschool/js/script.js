$(function() {

	var availableTags = [ "ActionScript", "AppleScript", "Asp", "BASIC", "C",
			"C++", "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran",
			"Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl", "PHP",
			"Python", "Ruby", "Scala", "Scheme" ];

	$("#mysearch232").autocomplete({
		source : function(request, response) {
			$.ajax({
				url : "/project/static/search",
				type : "GET",
				data : {
					term : request.term
				},

				dataType : "json",

				success : function(data) {
					response($.map(data, function(v, i) {
						return {
							label : v.schoolId,
							value : v.schoolId
						};
					}));
				}
			});
		}
	});

	$("#mysearcsdh").autocomplete({
		source : function(request, response) {
			$.getJSON("/project/static/search", request, function(result) {
				response($.map(result, function(item) {
					return {

						label : item.schoolId,
						value : item.schoolId,

					}
				}));
			});
		}
	});
});