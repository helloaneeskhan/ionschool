<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${context_school.fullName}</title>
</head>
<body>


	<form:form modelAttribute="schoolPro.about"
		action="/project/tester/posttest">
		<form:input path="topic" /><br>
		<c:forEach items="${ about.aboutItem}" var="item" varStatus="loop">

			<form:input path="aboutItem[${loop.index }].heading" />
			<form:input path="aboutItem[${loop.index }].subHeading" />

		</c:forEach>
		<input type="submit" />
	</form:form>

</body>
</html>