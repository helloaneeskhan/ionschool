<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ include file="/WEB-INF/views/augment/jsp-f/urls.jspf"%>
<!DOCTYPE HTML>
<html>
<head>
<title>${context_school.fullName}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Augment Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- //lined-icons -->

<%@ include file="/WEB-INF/views/augment/jsp-f/design-css.jspf"%>
<%@ include file="/WEB-INF/views/augment/jsp-f/head-script.jspf"%>

<!--clock init-->
</head>
<body>
	<div class="page-container">
		<!--/content-inner-->
		<div class="left-content">
			<div class="inner-content">
				<!-- header-starts -->

				<%@ include file="/WEB-INF/views/augment/jsp-f/header.jspf"%>

				<!-- //header-ends -->
				<!--//outer-wp-->
				<div class="outter-wp">
					<div class="profile-widget">
						<img src="${getStudentPro}?id=${get_student.studentId}"
							style="width: 150px; height: 150px;" alt=" " />
						<h2>${get_student.firstName}${get_student.lastName }</h2>
						<p>Admin</p>
					</div>
					<!--/profile-inner-->
					<div class="profile-section-inner">
						<!-- <div class="panel panel-primary">						 -->




						<!-- 						<div class="col-md-12"> -->
						<div class="col-md-12 graph-12">
							<div class="panel panel-primary">
								<h3 class="panel-heading">Personal Information</h3>
								<div class="panel-body">
									<!-- 										<div class="main-grid3"> -->

									<div class="p-20">
										<div class="about-info-p">
											<strong>Full Name</strong> <br>
											<p class="text-muted">${teacher.fullName}</p>
										</div>
										<div class="about-info-p">
											<strong>Mobile</strong> <br>
											<p class="text-muted"><c:if test="${empty teacher.mobile }">Unavailable</c:if>${teacher.mobile }</p>
										</div>
										<div class="about-info-p m-b-0">
											<strong>Address</strong> <br>
											<p class="text-muted"><c:if test="${empty teacher.address }">Unavailable</c:if>${teacher.address }</p>
										</div>

										<div class="about-info-p">
											<strong>Email</strong> <br>
											<p class="text-muted">
											
												<c:if test="${empty teacher.email }">Unavailable</c:if>${teacher.email }
											</p>
											<div class="input-group">
												<input type="text" class="form-control1 icon">
												<div class="input-group-addon">
													<input type="button" value="Send Message"
														placeholder="Message to send">
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>

						<div class="clearfix"></div>
					</div>

					<!--//profile-inner-->
					<!--//profile-->
				</div>
				<!--//outer-wp-->
				<!--footer section start-->
				<%@ include file="/WEB-INF/views/augment/jsp-f/footer.jspf"%>
				<!--footer section end-->
			</div>
		</div>
		<!--//content-inner-->
		<!--/sidebar-menu-->
		<%@ include file="/WEB-INF/views/augment/jsp-f/sidebar.jspf"%>
	</div>
	<%@ include file="/WEB-INF/views/augment/jsp-f/body-script.jspf"%>
</body>
</html>