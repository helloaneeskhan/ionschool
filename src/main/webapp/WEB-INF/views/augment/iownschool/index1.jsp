<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<title>Form Using Tabs Responsive Widget Template :: w3layouts</title>
<link href="<c:url value = '/resources/iownschool/css/style.css'/>"
	rel='stylesheet' type='text/css' />
<link href='//fonts.googleapis.com/css?family=Merienda+One'
	rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Acme' rel='stylesheet'
	type='text/css'>


<!-- For-Mobile-Apps-and-Meta-Tags -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Form Using Tabs Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
<script type="application/x-javascript">
	
	
	
	
	
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 







</script>
<!-- //For-Mobile-Apps-and-Meta-Tags -->

<script src="<c:url value = '/resources/iownschool/js/jquery.min.js'/>"></script>
<script
	src="<c:url value = '/resources/iownschool/js/easyResponsiveTabs.js'/>"
	type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#horizontalTab').easyResponsiveTabs({
			type : 'default', //Types: default, vertical, accordion           
			width : 'auto', //auto or any width like 600px
			fit : true
		// 100% fit in a container
		});
	});
</script>
</head>
<body>
<!-- 	<h1>IOwnSchool! Create Your Own School</h1> -->
	<div class="main-content" style = "margin-left: ;">
		<div class="form">
			<div class="sap_tabs">
				<div id="horizontalTab"
					style="display: block; width: 100%; margin: 0px;">
					<ul>
						<li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>Create
								Account</span></li>
						<li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span>Sign
								in</span></li>
					</ul>
					<div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
						<div class="facts">
							<div class="register">
								<form:form modelAttribute="newHead" method="POST"
									action="/project/">


									<!-- <hr>Fil For School Data<hr> -->
									<h3>School Id (Sub URL):</h3>
									<form:input path="school.schoolId" />
									<h3>Short Name</h3>
									<form:input path="school.shortName" />
									<h3>Full Name:</h3>
									<form:input path="school.fullName" />
									<h3>Password:</h3>
									<form:input path="password" />
									<h3>Confirm Password:</h3>
									<input type="password" />
									<h3>E-mail:</h3>
									<form:input path="email" />

									<div class="sign-up">
										<input type="submit" value="Create Account" />
									</div>
								</form:form>
							</div>
						</div>
					</div>

					<!-- 					<div class="tab-2 resp-`tab-content" aria-labelledby="tab_item-1"> -->
					<!-- 						<div class="facts"> -->
					<!-- 							<div class="register"> -->
					<%-- 								<form> --%>
					<!-- 									<h3>E-mail:</h3> -->
					<!-- 									<input placeholder="Email Address" class="mail" type="text" -->
					<!-- 										required=""> -->
					<!-- 									<h3>Password:</h3> -->
					<!-- 									<input placeholder="Password" class="lock" type="password" -->
					<!-- 										required=""> -->
					<!-- 									<div class="sign-up"> -->
					<!-- 										<input type="submit" value="Sign in" /> -->
					<!-- 									</div> -->
					<%-- 								</form> --%>
					<!-- 							</div> -->
					<!-- 						</div> -->
					<!-- 					</div> -->
				</div>
			</div>
		</div>
		<div class="right">
			<h4>why you'll love it</h4>
			<ul>
				<li><p>See all your accounts in one place</p></li>
				<li><p>Cloud storage for the win</p></li>
				<li><p>Fewer downloads.Better collaboration</p></li>
			</ul>
			<h5>Sign in with:</h5>
			<div class="socialicons">
				<ul>
					<li><a class="facebook" href="#"></a></li>
					<li><a class="twitter" href="#"></a></li>
					<li><a class="google" href="#"></a></li>
				</ul>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="footer">
		<p>
			&copy; 2016 Form Using Tabs. All Rights Reserved | Design by <a
				href="http://w3layouts.com">W3layouts</a>
		</p>
	</div>
</body>
</html>
