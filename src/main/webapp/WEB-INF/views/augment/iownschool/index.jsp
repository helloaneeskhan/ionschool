<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>Create Your Own School Profile | Home :: IOnSchool</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Attention Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">
	
	
	
	
	
	
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } 







</script>


<!-- //for-mobile-apps -->
<link href="<c:url value = '/resources/iownschool/css/bootstrap.css'/>"
	rel="stylesheet" type="text/css" media="all" />
<link href="<c:url value = '/resources/iownschool/css/style.css'/>"
	rel="stylesheet" type="text/css" media="all" />


<script type="text/javascript"
	src="<c:url value='/resources/iownschool/js/jquery-ui.js'/>"></script>
<script type="text/javascript"
	src="<c:url value='/resources/iownschool/js/jquery-1.8.3.min.js'/>"></script>
<script type="text/javascript"
	src="<c:url value='/resources/iownschool/js/jquery-ui.min.js'/>"></script>

<link rel="stylesheet"
	href="<c:url value = '/resources/iownschool/css/jquery-ui.css'/>"
	type="text/css" />

<!-- js -->
<!-- <script -->
<%-- 	src="<c:url value = '/resources/iownschool/js/jquery-1.11.1.min.js'/>"></script> --%>
<!-- //js -->

<!--script-->
<script
	src="<c:url value = '/resources/iownschool/js/jquery.chocolat.js'/>"></script>
<link rel="stylesheet"
	href="<c:url value = '/resources/iownschool/css/chocolat.css'/>"
	type="text/css" media="screen" charset="utf-8">

<link rel="stylesheet"
	href="<c:url value = '/resources/iownschool/css/jquery-ui.css'/>"
	type="text/css" />


<!--light-box-files-->
<script type="text/javascript" charset="utf-8">
	$(function() {
		$('.gallery-grid a').Chocolat();
	});
</script>
<!--script-->
<!-- start-smoth-scrolling -->
<script type="text/javascript"
	src="<c:url value = '/resources/iownschool/js/move-top.js'/>"></script>
<script type="text/javascript"
	src="<c:url value = '/resources/iownschool/js/easing.js'/>"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});

	});
</script>


<script src="<c:url value = '/resources/iownschool/js/script.js'/>"></script>

<!-- start-smoth-scrolling -->
<link
	href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Questrial'
	rel='stylesheet' type='text/css'>


<script type="text/javascript">
	$(function() {

		var availableTags = [ "ActionScript", "AppleScript", "Asp", "BASIC",
				"C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang",
				"Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp",
				"Perl", "PHP", "Python", "Ruby", "Scala", "Scheme" ];

		$("#mysearch").autocomplete({
			source : function(request, response) {
				$.ajax({
					url : "/project/static/search",
					type : "GET",
					data : {
						term : request.term
					},

					dataType : "json",

					minLength : 5,

					success : function(data) {
						response($.map(data, function(item) {
							return {
								label : item.schoolId,
								value : item.schoolId
							};
						}));
					}
				});
			}
		});

		// 		.data("autocomplete")._renderItem = function(ul, item){
		// 			return $("li")
		// 			.data("item.autocomplete", item)
		// 			.append(item.label+"<img src=''/>")
		// 			.appendTo(ul);

		// 		};

		$("#mysearchsd").autocomplete({
			source : function(request, response) {
				$.getJSON("/project/static/search", request, function(result) {
					response($.map(result, function(item) {
						return {
							label : item.schoolId,
							value : item.schoolId,

						}
					}));
				});
			}
		});
	});
</script>



</head>

<body>
	<!-- header -->

	<div class="header">
		<div class="container">
			<div class="header-left">
				<a href=""><i class="glyphicon glyphicon-book"
					aria-hidden="true"></i>IOwnSchool <span>We Build Your School Digital</span></a>
			</div>

			<div class="clearfix"></div>
		</div>
	</div>

	<!-- header-nav -->
	<div class="header-nav">
		<div class="container">
			<div class="header-nav-bottom">
				<nav class="navbar navbar-default">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->

					<div class="collapse navbar-collapse nav-wil"
						id="bs-example-navbar-collapse-1">

						<ul class="nav navbar-nav">
							<li class="active"><a href="">Home</a></li>
							<li><a href="#contact" class="scroll">Create Your School</a></li>

						</ul>

						<div class="header-top-right">
							<!-- 							<form> -->
							<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
							<form:form method="POST" action="/project/static/school-id">
								<div>
									<input id="mysearch" type="text" name="mysearch"
										value="Search With School Id..." onfocus="this.value = '';"
										onblur="if (this.value == '') {this.value = 'Search With School Id...';}" />
								</div>
							</form:form>
						</div>

					</div>
					<!-- /.navbar-collapse -->
				</nav>
			</div>
		</div>
	</div>

	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<div class="testimonials">
		<div class="container">
			<h3>Our Schools</h3>
			<div class="testimonials-grids">
				<ul id="flexiselDemo1">
					<c:forEach items="${schoolList}" var="item">


						<li>
							<div class="testimonials-grid">
								<div class="col-xs-5 testimonials-grid-left">
									<a href="<c:url value = '${item.schoolId}/'/>"> <img
										src="data:image/jpeg;base64,${item.encodedLogo}"
										class="img-responsive"
										style="width: 130px; height: 130px; border-radius: 65px;" /></a>
								</div>
								<div class="col-xs-7 testimonials-grid-right">
<!-- 									<div class="rating"> -->
<!-- 										<span>*</span> <span>*</span> <span>*</span> <span>*</span> <span>*</span> -->
<!-- 									</div> -->
									<h1>${fn:substring(item.shortName,0,9)}</h1>
									<p>
										${fn:substring(item.fullName,0,27)}<span>${item.schoolId}</span>
									</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>

					</c:forEach>


				</ul>

				<script type="text/javascript">
					$(window).load(function() {
						$("#flexiselDemo1").flexisel({
							visibleItems : 3,
							animationSpeed : 1000,
							autoPlay : true,
							autoPlaySpeed : 3000,
							pauseOnHover : true,
							enableResponsiveBreakpoints : true,
							responsiveBreakpoints : {
								portrait : {
									changePoint : 480,
									visibleItems : 1
								},
								landscape : {
									changePoint : 640,
									visibleItems : 2
								},
								tablet : {
									changePoint : 768,
									visibleItems : 2
								}
							}
						});

					});
				</script>
				<script type="text/javascript"
					src="<c:url value = '/resources/iownschool/js/jquery.flexisel.js'/>"></script>
			</div>
		</div>
	</div>
	<!-- //testimonials -->



	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<div id="contact" class="contact">
		<div class="container">

			<div class="col-md-8 contact-left">
				<!-- 				<h4>Contact Form</h4> -->
				<h1>Public Your School</h1>
				<hr>
				<br>

				<form:form modelAttribute="newHead" method="POST" action="/project/">


					<!-- <hr>Fil For School Data<hr> -->
					<h3>School Id:</h3>
					<form:input path="school.schoolId" />
					<hr>
					<br>
					<h3>Short Name:</h3>
					<form:input path="school.shortName" />
					<hr>
					<h3>Full Name:</h3>
					<form:input path="school.fullName" />
					<hr>
					<h3>Password:</h3>
					<form:input path="password" />


					<hr>
					<h3>E-mail:</h3>
					<form:input path="email" />

					<div class="sign-up">
						<input type="submit" value="Create School" />
					</div>
				</form:form>
				<%-- 				<form> --%>
				<!-- 					<input type="text" value="Name" onfocus="this.value = '';" -->
				<!-- 						onblur="if (this.value == '') {this.value = 'Name';}" required=""> -->
				<!-- 					<input type="email" value="Email" onfocus="this.value = '';" -->
				<!-- 						onblur="if (this.value == '') {this.value = 'Email';}" required=""> -->
				<!-- 					<input type="text" value="Telephone" onfocus="this.value = '';" -->
				<!-- 						onblur="if (this.value == '') {this.value = 'Telephone';}" -->
				<!-- 						required=""> -->
				<!-- 					<textarea type="text" onfocus="this.value = '';" -->
				<!-- 						onblur="if (this.value == '') {this.value = 'Message...';}" -->
				<!-- 						required="">Message...</textarea> -->
				<!-- 					<input type="submit" value="Submit"> <input type="reset" -->
				<!-- 						value="Clear"> -->

				<%-- 				</form> --%>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<script src="<c:url value = '/resources/iownschool/js/bootstrap.js'/>">
		
	</script>
	<!-- //for bootstrap working -->
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			 */

			$().UItoTop({
				easingType : 'easeOutQuart'
			});

		});
	</script>
	<!-- //here ends scrolling icon -->
</body>
</html>