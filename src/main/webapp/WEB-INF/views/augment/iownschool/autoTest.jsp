<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<!-- <head> -->
<!-- <title>Chat - Spring MVC + Hibernate + JQuery</title> -->
<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"> </script> -->
<!--  <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"> </script> -->
<!--  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" /> -->
<%-- <link href="<c:url value='/resources/css/main.css'/>" rel="stylesheet" --%>
<!-- 	type="text/css" /> -->
<!-- <script type="text/javascript" -->
<%-- 	src="<c:url value='/resources/js/jquery-1.8.3.min.js'/>"></script> --%>
<!-- <script type="text/javascript" -->
<%-- 	src="<c:url value='/resources/js/script.js'/>"></script> --%>
<!-- </head> -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- <link rel="stylesheet" -->
<!-- 	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->
<!-- <script type="text/javascript" -->
<!-- 	src="https://code.jquery.com/jquery-2.1.1.min.js"></script> -->
<!-- <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script> -->
<script type="text/javascript"
	src="<c:url value='/resources/iownschool/js/jquery-ui.js'/>"></script>
<script type="text/javascript"
	src="<c:url value='/resources/iownschool/js/jquery-1.8.3.min.js'/>"></script>
<script type="text/javascript"
	src="<c:url value='/resources/iownschool/js/jquery-ui.min.js'/>"></script>

<title>Spring Auto-complete</title>
<script type="text/javascript">
	$(function() {

		var availableTags = [ "ActionScript", "AppleScript", "Asp", "BASIC",
				"C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang",
				"Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp",
				"Perl", "PHP", "Python", "Ruby", "Scala", "Scheme" ];
		$("#id").autocomplete({
			source : availableTags
		// 				function(request, response) {
		// 				$.getJSON("${pageContext.request.contextPath}/static", {
		// 					term : request.term
		// 				}, response);
		// 			}
		});

		$("#tagsName").autocomplete({
			source : function(request, response) {
				$.ajax({
					url : "/project/static/search",
					type : "GET",
					data : {
						term : request.term
					},

					dataType : "json",

					success : function(data) {
						response($.map(data, function(v, i) {
							return {
								label : v.empName,
								value : v.empName
							};
						}));
					}
				});
			}
		});

	});
</script>
</head>
<body>
	<form:form method="GET">
		<div>
			<label for="employee">Employee: </label> <input id="tagsName" />
		</div>
	</form:form>
	<br>

</body>
</html>