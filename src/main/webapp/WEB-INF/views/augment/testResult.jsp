<%@page import="org.springframework.ui.Model"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<%@ include file="/WEB-INF/views/augment/jsp-f/contextSetting.jspf"%>
<%@ include file="/WEB-INF/views/augment/jsp-f/urls.jspf"%>

<!DOCTYPE HTML>
<html>
<head>
<title>${context_school.fullName}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Augment Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- //lined-icons -->
<%@ include file="/WEB-INF/views/augment/jsp-f/design-css.jspf"%>
<%@ include file="/WEB-INF/views/augment/jsp-f/head-script.jspf"%>
<!--clock init-->
</head>
<body>
	<div class="page-container">
		<!--/content-inner-->
		<div class="left-content">
			<div class="inner-content">
				<!-- header-starts -->

				<%@ include file="/WEB-INF/views/augment/jsp-f/header.jspf"%>

				<!-- //header-ends -->
				<!--//outer-wp-->
				<div class="outter-wp">
					<!--sub-heard-part-->
					<div class="sub-heard-part">
						<ol class="breadcrumb m-b-0">
							<li><a href="index.html">Home</a></li>
							<li class="active">Blank Page</li>
						</ol>
					</div>
					<!--//sub-heard-part-->
					<div class="graph-visual tables-main">
						<h2 class="inner-tittle">Test Result</h2>

						<security:authorize access="hasRole('ROLE_STUDENT')">
							<c:if test="${not empty testRecord}"><%@ include
									file="/WEB-INF/views/augment/jsp-f/testResultBlock.jspf"%>
							</c:if>


							<c:if test="${not empty testRecords}">
								<c:if test="${not empty compResult}">
									<%@ include
										file="/WEB-INF/views/augment/jsp-f/compositeResultBlock.jspf"%>
								</c:if>
								<%@ include
									file="/WEB-INF/views/augment/jsp-f/testResultListBlock.jspf"%>
							</c:if>
							<c:if test="${empty testRecords and empty testRecord }">
							Empty Records</c:if>

						</security:authorize>

						<security:authorize access="hasRole('ROLE_GUARDIAN')">
							<c:if test="${not empty testRecord}"><%@ include
									file="/WEB-INF/views/augment/jsp-f/testResultBlock.jspf"%>
							</c:if>


							<c:if test="${not empty testRecords}">
								<c:if test="${not empty compResult}">
									<%@ include
										file="/WEB-INF/views/augment/jsp-f/compositeResultBlock.jspf"%>
								</c:if>
								<%@ include
									file="/WEB-INF/views/augment/jsp-f/testResultListBlock.jspf"%>
							</c:if>
							<c:if test="${empty testRecords and empty testRecord }">
							Empty Records</c:if>

						</security:authorize>

						<security:authorize access="hasRole('ROLE_TEACHER')">
							<c:if test="${not empty testRecords}">
								<%@ include
									file="/WEB-INF/views/augment/jsp-f/testResultListBlock.jspf"%></c:if>
							<c:if test="${empty testRecords}">
							Empty Records</c:if>

						</security:authorize>

					</div>

					<!--//graph-visual-->
				</div>
				<!--//outer-wp-->
				<!--footer section start-->
				<%@ include file="/WEB-INF/views/augment/jsp-f/footer.jspf"%>
				<!--footer section end-->
			</div>
		</div>
		<!--//content-inner-->
		<!--/sidebar-menu-->
		<%@ include file="/WEB-INF/views/augment/jsp-f/sidebar.jspf"%>
	</div>
	<%@ include file="/WEB-INF/views/augment/jsp-f/body-script.jspf"%>
</body>
</html>