<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ include file="/WEB-INF/views/augment/jsp-f/urls.jspf"%>
<%@ include file="/WEB-INF/views/augment/jsp-f/contextSetting.jspf"%>
<!DOCTYPE HTML>
<html>
<head>
<title>${context_school.fullName}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Augment Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- //lined-icons -->
<%@ include file="/WEB-INF/views/augment/jsp-f/design-css.jspf"%>
<%@ include file="/WEB-INF/views/augment/jsp-f/head-script.jspf"%>
<script type="text/javascript"
	src="<c:url value='/resources/iownschool/js/amcharts.js'/>"></script>

<script type="text/javascript"
	src="<c:url value='/resources/iownschool/js/serial.js'/>"></script>

<script type="text/javascript"
	src="<c:url value='/resources/iownschool/js/light.js'/>"></script>

<script type="text/javascript"
	src="<c:url value='/resources/iownschool/js/radar.js'/>"></script>

<script type="text/javascript"
	src="<c:url value='/resources/iownschool/js/jquery-1.10.2.min.js'/>"></script>








<!--clock init-->
</head>
<body>

	<div class="page-container">
		<!--/content-inner-->
		<div class="left-content">
			<div class="inner-content">
				<!-- header-starts -->

				<%@ include file="/WEB-INF/views/augment/jsp-f/header.jspf"%>

				<!-- //header-ends -->
				<!--//outer-wp-->

				<div class="outter-wp">
					<!--/sub-heard-part-->
					<div class="sub-heard-part">
						<ol class="breadcrumb m-b-0">
							<li><a href="index.html">Home</a></li>
							<li class="active">Subject</li>
						</ol>
					</div>
					<!--/sub-heard-part-->
					<!--/inbox-->



					<div class="mail-toolbar clearfix">
						<div class="float-left">
							<div class="btn-group m-r-sm mail-hidden-options"
								style="display: inline-block;">
								<security:authorize access="hasRole('ROLE_STUDENT')">
									<div class="btn-group">
										<a class="btn btn-default dropdown-toggle"
											data-toggle="dropdown" aria-expanded="false"><i
											class="fa fa-folder"></i> <span class="caret"></span></a>
										<ul class="dropdown-menu dropdown-menu-right" role="menu">

											<li><a
												href="${myClass}/${subject.subjectId}/view-result-history">Test
													Result History</a></li>

										</ul>
									</div>
								</security:authorize>
								<security:authorize access="hasRole('ROLE_TEACHER')">
									<div class="btn-group">
										<a class="btn btn-default dropdown-toggle"
											data-toggle="dropdown" aria-expanded="false"><i
											class="fa fa-tags"></i> <span class="caret"></span></a>
										<ul class="dropdown-menu dropdown-menu-right" role="menu">
											<li><a
												href="${mySubject}/${subject.subjectId}/create-test">Create
													New Test</a></li>
											<li><a href="${mySubject}/${subject.subjectId}/all-test">View
													All Test</a></li>
											<li><a
												href="${mySubject}/${subject.subjectId}/view-students-result">View
													Current Test's Result</a></li>
											<li><a href="#">View Current Result</a></li>


											<li><a href="#">Promotions</a></li>
											<li><a href="#">Forums</a></li>
										</ul>
									</div>
								</security:authorize>
							</div>



						</div>
						<div class="float-right">
							<div class="dropdown">
								<a href="#" title="" class="btn btn-default"
									data-toggle="dropdown" aria-expanded="false"> <i
									class="fa fa-cog icon_8"></i> <i
									class="fa fa-chevron-down icon_8"></i>
									<div class="ripple-wrapper"></div></a>
								<ul class="dropdown-menu float-right">
									<li><a href="#" title=""> <i
											class="fa fa-pencil-square-o icon_9"></i> Edit
									</a></li>
									<li><a href="#" title=""> <i
											class="fa fa-calendar icon_9"></i> Schedule
									</a></li>
									<li><a href="#" title=""> <i
											class="fa fa-download icon_9"></i> Download
									</a></li>

									<li><a href="#" class="font-red" title=""> <i
											class="fa fa-times" icon_9=""></i> Delete
									</a></li>
								</ul>
							</div>


						</div>

					</div>








					<div class="inbox-mail">

						<!-- tab content -->
						<div class="col-md-12 tab-content tab-content-in">
							<div class="tab-pane active text-style" id="tab1">
								<div class="inbox-right">

									<div class="mailbox-content">

										<table class="table">
											<tbody>

												<form:form methdo="POST" modelAttribute="newPost"
													action="/project/static/new-post">

													<tr class="table-row" style="padding: 10px;">
														<td class="table-img" style="padding: 10px;"><img
															src="data:image/jpeg;base64,<security:authentication
					property='principal.encodedProfilePhoto' />"
															style="width: 80px; height: 80px" /></td>
														<td class="table-text" style="font-size: x-large;"
															colspan="3">
															<h6>
																<security:authentication property="principal.fullName" />
															</h6>
															<p style="font-size: x-large;">
																<form:textarea rows="3" cols="78"
																	placeholder="New Post. . . " path="text" />

																<input type="hidden" name="subjectId"
																	value="${subject.subjectId}" />
															</p> <input style="margin-top: 0pt;" type="submit"
															value="Post this" />

														</td>

													</tr>

												</form:form>

												<c:forEach items="${subject.posts}" var="postItem">
													<tr class="table-row" style="padding: 10px; margin: 30px;">
														<td class="table-img" style="padding: 10px;"><img
															src="data:image/jpeg;base64,${postItem.account.encodedProfilePhoto}"
															alt="" style="width: 80px; height: 80px" /></td>
														<td class="table-text" style="font-size: x-large;">
															<h6>${postItem.account.fullName}</h6>

															<p style="font-size: x-large;">${postItem.text}</p>
															<table class="table">
																<tbody>
																	<c:forEach items="${postItem.comments}"
																		var="commentItem">
																		<tr class="table-row">
																			<td><img
																				src="/project/static/get-profile-photo/${commentItem.accountId}"
																				alt="" style="width: 35px; height: 35px;" />${commentItem.authorName }<br>
																				<p style="margin-left: 37px;">${commentItem.text }</p></td>
																		</tr>
																	</c:forEach>
																	<tr class="table-row">
																		<form:form action="/project/static/new-comment"
																			modelAttribute="newComment" method="POST">
																			<td style="font-size: large;"><img
																				src="data:image/jpeg;base64,<security:authentication
					property='principal.encodedProfilePhoto' />"
																				style="width: 35px; height: 35px;" /> <form:input
																					path="text"
																					style="padding-left: 2px; height: 28px;" size="90%"
																					placeholder="comment here. . ." /></td>
																			<form:hidden path="postId" value="${postItem.id}" />
																			<!-- 																			<input type="hidden" name="postId" -->
																			<%-- 																				value="${postItem.id}" /> --%>
																			<input type="hidden" name="subjectId"
																				value="${subject.subjectId}" />


																		</form:form>
																	</tr>

																</tbody>
															</table>
														</td>
														<c:if test="${postItem.authType eq 'Student'  }">
															<td><span class="ur">${postItem.authType }</span></td>
														</c:if>

														<c:if test="${postItem.authType eq 'Teacher'  }">
															<td><span class="fam">${postItem.authType }</span></td>
														</c:if>

														<c:if test="${postItem.authType eq 'Head'  }">
															<td><span class="mar">${postItem.authType }</span></td>
														</c:if>

														<!-- 														<td class="march">in 5 days</td> -->

														<!-- 														<td><i class="fa fa-star-half-o icon-state-warning"></i> -->

														</td>
													</tr>


												</c:forEach>









											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						<div class="clearfix"></div>
					</div>


				</div>



				<!--//outer-wp-->
				<!--footer section start-->
				<%@ include file="/WEB-INF/views/augment/jsp-f/footer.jspf"%>
				<!--footer section end-->
			</div>
		</div>
		<!--//content-inner-->
		<!--/sidebar-menu-->
		<%@ include file="/WEB-INF/views/augment/jsp-f/sidebar.jspf"%>
	</div>
	<%@ include file="/WEB-INF/views/augment/jsp-f/body-script.jspf"%>
</body>
</html>