<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<!-- items -->
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/views/augment/front/urls-profile.jspf"%>
<title>${schoolProfile.fullName}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Career Builder Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- css links -->
<style type="text/css">
.slider1 {
	background:
		url("data:image/jpeg;base64,${schoolProfile.encodedCovers[0]}")
		no-repeat 0px 0px;
	background-size: cover;
	min-height: 725px;
	/* 	725px; */
}

.slider2 {
	background:
		url("data:image/jpeg;base64,${schoolProfile.encodedCovers[1]}");
	background-size: cover;
	min-height: 725px;
	background-size: cover;
}

.slider3 {
	background:
		url("data:image/jpeg;base64,${schoolProfile.encodedCovers[2]}");
	background-size: cover;
	min-height: 725px;
	background-size: cover;
}
</style>
<link
	href="<c:url value = '/resources/front_resources/css/bootstrap.min.css'/>"
	rel="stylesheet" type="text/css" media="all">
<link
	href="<c:url value = '/resources/front_resources/css/slider.css'/>"
	rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" type="text/css"
	href="<c:url value = '/resources/front_resources/css/facultystyle.css'/>" />
<link href="<c:url value = '/resources/front_resources/css/style.css'/>"
	rel="stylesheet" type="text/css" media="all">
<!-- /css links -->
<link href='//fonts.googleapis.com/css?family=Dancing+Script:400,700'
	rel='stylesheet' type='text/css'>
<link
	href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	rel='stylesheet' type='text/css'>
<link
	href='//fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800'
	rel='stylesheet' type='text/css'>
<script
	src="<c:url value = '/resources/ront_resources/js/SmoothScroll.min.js'/>"></script>
<script type="text/javascript"
	src="<c:url value = '/resources/front_resources/js/modernizr.custom.js'/>"></script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar"
	data-offset="60">


	<!-- Fixed navbar -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">

			<div class="navbar-header">

				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">

					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>

				<a class="navbar-brand" href="index.html">${schoolProfile.shortName }</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="#myPage">HOME</a></li>
					<li><a href="#about">ABOUT</a></li>
					<li><a href="#gallery">GALLERY</a></li>
					<li><a href="#faculty">FACULTY</a></li>
					<li><a href="#events">EVENTS</a></li>
					<li><a href="#contact">CONTACT</a></li>

					<security:authorize access="isAuthenticated()">
						<li><a
							href="<c:url value = '/${schoolProfile.schoolId }/' />">PORTAL</a></li>
					</security:authorize>

					<security:authorize access="isAnonymous()">
						<li><a href="#" id="customize" data-toggle="modal"
							data-target="#login">LOGIN</a></li>
					</security:authorize>


					<security:authorize access="hasRole('ROLE_HEAD')">
						<li><a href="#" id="customize" data-toggle="modal"
							data-target="#setting">CUSTOMIZE</a></li>
					</security:authorize>
				</ul>

			</div>

			<!--/.nav-collapse -->
		</div>
	</nav>

	<!-- /Fixed navbar -->
	<div class="modal fade" id="login" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Login</h4>
					<hr>
					<img src="data:image/jpeg;base64,${schoolProfile.encodedCovers[1]}">

					<p>
					<form action="/project/j_spring_security_check" method="POST">
						Account Id:<input type="text" id="j_username" name="j_username" />
						<!-- 						<input type="text" id="j_username" name="j_username" -->
						<!-- 							onfocus="this.value = '';" -->
						<%-- 							onblur="if(this.value !='') {this.value = this.value+'@'+'${schoolProfile.schoolId}';}" /> --%>
						Password:<input type="text" id="j_password" name="j_password" />


						<input type="submit" value="click to login" style="width: 100px;" />
						Remmember Me:<input type="checkbox" />
					</form>

					</p>
				</div>
			</div>
		</div>
	</div>




	<!-- /Fixed navbar -->
	<div class="modal fade" id="setting" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4>Customize Your School Profile</h4>
					<hr>

					<img alt="Kangoo_image"
						src="data:image/jpeg;base64,${schoolProfile.about.encodedCenterImage }" />
					<!-- 							style="height: 363px; width: 541px;" /> -->
					<!-- 					<img -->
					<%-- 						src="<c:url value = '/resources/front_resources/images/banner3.jpg'/>"> --%>
<!-- 					<p> -->

						<form:form modelAttribute="schoolProfile"
							enctype="multipart/form-data" action="${updateSchoolProfile }"
							method="POST">

							<hr>
							<h3>School</h3>
							<hr>
							<form:hidden path="schoolId" />
							Short Name:
							<form:input path="shortName" />
							Full Name:
							<form:input path="fullName" />
							<br>
School's Logo: <input type="file" name="logo_image" id="logo_image" />
							<form:hidden path="encodedLogo" />
							<br>
							Cover 1: <input type="file" name="coverOne_image" id = "coverOne_image" />
							<input type="hidden" />
							<form:hidden path="encodedCovers" />
					
							Cover 2: <input type="file" name="coverTwo_image" id="coverTwo_image" />
							Cover 3: <input type="file" name="coverThree_image"
								id="coverThree_image" />

							<hr>
							<h3>${schoolProfile.about.heading}</h3>
							<hr>
							Description: <form:input path="about.description" />
							About Us: <form:input path="about.wordAboutUs" />
							Center Image: <input type="file" name="centerImage" id = "centerImage"/>
							<form:hidden path="about.id" />
							<form:hidden path="about.encodedCenterImage" />

							<hr>
							<h3>${schoolProfile.event.heading}</h3>
							<hr>
							Description: <form:input path="event.description" />
							<form:hidden path="event.id" />

							<hr>
							<h3>${schoolProfile.faculty.heading}</h3>
							<hr>
							Description: <form:input path="faculty.description" />
							<form:hidden path="faculty.id" />


							<hr>
							<h3>${schoolProfile.gallery.heading}</h3>
							<hr>
							Description: <form:input path="gallery.description" />
							<form:hidden path="gallery.id" />

							<hr>
							<h3>Contact</h3>
							<hr>
							Facebook Address: <form:input path="facebookAddress" />
							Google Address: <form:input path="googleAddress" />
							LinkedIn Address: <form:input path="linkedinAddress" />
							Pinterest Address: <form:input path="pinterestAddress" />
							Twitter Address: <form:input path="twitterAddress" />

							<input type="submit" value="Udate School Profile" />
						</form:form>

<!-- 					</p> -->
				</div>
			</div>
		</div>
	</div>






	<!-- img Banner -->
	<div class="slider">

		<div id="top" class="callbacks_container-wrap">
			<ul class="rslides" id="slider3">
				<li>
					<div class="myslider1 slider1"></div>
				</li>
				<li>
					<div class="myslider1 slider2"></div>
				</li>
				<li>
					<div class="myslider1 slider3"></div>
				</li>
			</ul>

		</div>

	</div>
	<!-- /Banner -->


	<!-- 	<a href="#" id="exp">Exp new</a> -->
	<!-------------------- About---------------------- -->
	<section id="about">
		<div class="about">
			<div class="container">
				<div class="abouttop">
					<h3>${schoolProfile.about.heading}</h3>
					<label class="line"></label>
					<h6>${fn:substring(schoolProfile.about.description,0,152) }</h6>
					<div class="col-md-3 col-sm-3 aboutleft">
						<h4>A WORD ABOUT US</h4>
					</div>
					<div class="col-md-6 col-sm-6 aboutmiddle">
						<div class="aboutleftbottom">
							<p>${fn:substring(schoolProfile.about.wordAboutUs,0,150) }..
								.</p>
							<a href="#" data-toggle="modal" data-target="#myModal">EXPLORE</a>
							<!-- Modal -->
							<div class="modal fade" id="myModal" role="dialog">
								<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<p>${schoolProfile.about.wordAboutUs}</p>

										</div>
									</div>
								</div>
							</div>
						</div>
						<img alt="Kangoo_image"
							src="data:image/jpeg;base64,${schoolProfile.about.encodedCenterImage }"
							style="height: 363px; width: 541px;" />



					</div>
<!-- 					<div class="col-md-3 col-sm-3 aboutright"> -->
<!-- 						<div class="aboutrighttop"> -->
<!-- 							<div class='numscroller numscroller-big-bottom' data-slno='1' -->
<!-- 								data-min='0' data-max='70000' data-delay='.5' -->
<!-- 								data-increment="100">70000</div> -->
<!-- 							<label class="aline"></label> -->
<!-- 							<p class="stats-info">Students</p> -->
<!-- 						</div> -->
<!-- 						<div class="aboutrightbottom"> -->
<!-- 							<div class='numscroller numscroller-big-bottom' data-slno='1' -->
<!-- 								data-min='0' data-max='600' data-delay='.5' data-increment="5">600</div> -->
<!-- 							<label class="aline"></label> -->
<!-- 							<p class="stats-info">Faculty</p> -->
<!-- 						</div> -->
<!-- 					</div> -->
					<div class="clearfix"></div>
				</div>
			</div>


			<div class="aboutbottom">


				<c:forEach var="item" varStatus="loop"
					items="${schoolProfile.about.items}">

					<c:if test="${loop.index==0 or loop.index==1 }">
						<div class="col-md-3 col-sm-3 aboutimg arrow">
							<!-- 							<img -->
							<%-- 								src="<c:url value = '/resources/front_resources/images/b1.jpg'/>" --%>
							<img alt="Kangoo_image"
								src="data:image/jpeg;base64,${item.encodedImage }"
								style="width: 397px; height: 397px;">
						</div>
					</c:if>


					<div class="col-md-3 col-sm-3 abouttext">
						<h4>${item.heading }</h4>
						<h5>${item.subHeading}</h5>
						<p>${item.description }</p>
						<!-- 						<a href="#" data-toggle="modal" data-target="#myModal1">EXPLORE</a> -->


						<security:authorize access="hasRole('ROLE_HEAD')">
							<a href="#" data-toggle="modal"
								data-target="#<c:out value='${item.id }'/>">CUSTOMIZE</a>
						</security:authorize>

						<security:authorize access="isAnonymous()">

							<a href="#" data-toggle="modal"
								data-target="#exp<c:out value='${item.id }'/>">EXPLORE</a>
						</security:authorize>


						<!-- Modal -->
						<div class="modal fade" id="<c:out value='${item.id }'/>"
							role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4>${item.heading }</h4>
										<hr>
										<img alt="Kangoo_image"
											src="data:image/jpeg;base64,${item.encodedImage }" />
										<!-- 							style="height: 363px; width: 541px;" /> -->
										<!-- 										<img -->
										<%-- 											src="<c:url value = '/resources/front_resources/images/b1m.jpg'/>"> --%>
										<h5>${item.subHeading}</h5>
										<p>${item.description }</p>

										<form:form modelAttribute="schoolProfile"
											enctype="multipart/form-data" action="${updateItem }"
											method="POST">

											<form:input path="about.items[${loop.index}].heading" />
											<form:input path="about.items[${loop.index}].subHeading" />
											<form:input path="about.items[${loop.index}].description" />
											<form:hidden path="about.items[${loop.index}].id" />
											<form:hidden path="about.items[${loop.index }].encodedImage" />
											<form:hidden path="schoolId" />

											<input type="hidden"
												value="${schoolProfile.about.items[loop.index].id}"
												id="itemId" name="itemId" />
											<input type="hidden" value="about" id="topic" name="topic" />

											<input type="file" id="image" name="image" />
											<input type="submit" value="click to update"
												style="width: 100%;" />

										</form:form>

									</div>
								</div>
							</div>
						</div>
						<!-- 						this will explore about item -->
						<div class="modal fade" id="exp<c:out value='${item.id }'/>"
							role="dialog">
							<div class="modal-dialog">
								Modal content
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>

										<p>${item.description }</p>


									</div>
								</div>
							</div>
						</div>

						<!-- 						this will customize for about item -->
						<div class="modal fade" id="<c:out value='${item.id }'/>"
							role="dialog">
							<div class="modal-dialog">
								Modal content
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4>${item.heading }</h4>
										<img
											src="<c:url value = '/resources/front_resources/images/b1m.jpg'/>">


										<form:form modelAttribute="schoolProfile"
											enctype="multipart/form-data" action="${updateItem }"
											method="POST">

											<form:input path="gallery.items[${loop.index}].heading" />
											<form:hidden path="gallery.items[${loop.index}].id" />
											<form:hidden path="gallery.items[${loop.index}].encodedImage" />
											<input type="hidden" value="gallery" id="topic" name="topic" />
											<form:hidden path="schoolId" />
											<input type="file" id="image" name="image" />
											<input type="submit" value="click to update"
												style="width: 100%;" />

										</form:form>

									</div>
								</div>
							</div>
						</div>

					</div>

					<c:if test="${loop.index ==2 or loop.index==3 }">
						<div class="col-md-3 col-sm-3 aboutimg arrow2">
							<img alt="Kangoo_image"
								src="data:image/jpeg;base64,${item.encodedImage }"
								style="width: 397px; height: 397px;">
						</div>
					</c:if>


				</c:forEach>
				<%-- 				</form:form> --%>

				<div class="clearfix"></div>
			</div>
		</div>

	</section>


	<%-- 	<form action="/project/j_spring_security_check" method="POST"> --%>
	<!-- 		Account Id:<input type="text" id="j_username" name="j_username" /> -->
	<!-- 		Password:<input type="text" id="j_password" name="j_password" /> -->
	<!-- 		<!-- 					Remmember Me:<input	type="checkbox" /> -->

	<!-- 		<!-- 					<input type = "submit" style = "width: 100%;"/> -->


	<!-- 		<input type="submit" value="click to login" style="width: 100px;" /> -->
	<!-- 		Remmember Me:<input type="checkbox" /> -->
	<%-- 	</form> --%>



	<!-- ///////////////////////////////////////////////Gallery Section /////////////////////////////////////////////////-->

	<c:forEach items="${schoolProfile.gallery.items }" var="item"
		varStatus="loop">

		<div class="modal fade" id="<c:out value='${item.id }'/>"
			role="dialog">
			<div class="modal-dialog">
				Modal content
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4>${item.heading }</h4>
						<hr>
						<img alt="Kangoo_image"
							src="data:image/jpeg;base64,${item.encodedImage }" />
						<!-- 						<img -->
						<%-- 							src="<c:url value = '/resources/front_resources/images/b1m.jpg'/>"> --%>


						<form:form modelAttribute="schoolProfile"
							enctype="multipart/form-data" action="${updateItem }"
							method="POST">

							<form:input path="gallery.items[${loop.index}].heading" />
							<form:hidden path="gallery.items[${loop.index}].id" />
							<form:hidden path="gallery.items[${loop.index}].encodedImage" />


							<input type="hidden"
								value="${schoolProfile.gallery.items[loop.index].id}"
								id="itemId" name="itemId" />
							<form:hidden path="schoolId" />
							<input type="hidden" value="gallery" id="topic" name="topic" />
							<input type="file" id="image" name="image" />
							<input type="submit" value="click to update" style="width: 100%;" />

						</form:form>

					</div>
				</div>
			</div>
		</div>


	</c:forEach>

	<div class="gallery" id="gallery">
		<div class="container">
			<h3>${schoolProfile.gallery.heading}</h3>
			<label class="line"></label>
			<h6>${fn:substring(schoolProfile.gallery.description,0,152) }</h6>

			<section id="gallery" class="parallax section">

				<div class="container">
					<div class="row">
						<c:forEach var="item" varStatus="loop"
							items="${schoolProfile.gallery.items}">


							<div class="col-md-4 col-sm-4 ggrids">

								<security:authorize access="hasRole('ROLE_HEAD')">
									<a href="#" data-toggle="modal" data-target="#${item.id }"
										id="exp">CUSTOMIZE</a>
								</security:authorize>

								<a href="#" title="This is the description" id="ga"> <img
									src="data:image/jpeg;base64,${item.encodedImage }"
									style="height: 240px; width: 360px;" />



									<div class="description">
										<span class="caption">${item.heading}</span> <span
											class="camera"></span>
										<div class="clearfix"></div>
									</div>
								</a>
							</div>

						</c:forEach>
					</div>
				</div>
			</section>
		</div>
	</div>
	<!-- ////////////////////////////////////////////Gallery Section (End)/////////////////////////////////////////////////-->












	<!-- //////////////////////////////////////////////Faculty Section//////////////////////////////////////////////////// -->

	<div class="faculty" id="faculty">
		<div class="container">
			<div class="ftop">
				<!-- 				<h3>OUR FACULTY</h3> -->
				<h3>${schoolProfile.faculty.heading}</h3>
				<label class="line"></label>
				<p>${fn:substring(schoolProfile.faculty.description,0,152) }
				<p>
			</div>


			<c:forEach var="item" varStatus="loop"
				items="${schoolProfile.faculty.items}">

				<div class="col-md-3 col-sm-3 fgrids">
					<div class="view view-fourth">

						<img src="data:image/jpeg;base64,${item.encodedImage }"
							style="height: 235px; width: 235px;">

						<div class="mask">
							<h6>@admaris_smith</h6>
							<label class="fline"></label>
							<p>A wonderful serenity has taken possession of my entire
								soul.</p>
							<ul>
								<li><a class="facebook" href="#"></a></li>
								<li><a class="twitter" href="#"></a></li>
								<li><a class="google" href="#"></a></li>

							</ul>

						</div>

						<security:authorize access="hasRole('ROLE_HEAD')">
							<a href="#" data-toggle="modal" data-target="#${item.id }"
								id="exp">CUSTOMIZE</a>
						</security:authorize>

					</div>
					<div class="ftext">
						<h4>${item.heading }</h4>
						<label class="fline"></label>
						<h5>${item.subHeading }</h5>

					</div>
				</div>

				<div class="modal fade" id="<c:out value='${item.id }'/>"
					role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4>${item.heading }</h4>
								<hr>
								<img alt="Kangoo_image"
									src="data:image/jpeg;base64,${item.encodedImage }" />
								<!-- 								<img -->
								<%-- 									src="<c:url value = '/resources/front_resources/images/b1m.jpg'/>"> --%>


								<form:form modelAttribute="schoolProfile"
									enctype="multipart/form-data" action="${updateItem }"
									method="POST">

									<form:input path="faculty.items[${loop.index}].heading" />
									<form:input path="faculty.items[${loop.index}].subHeading" />
									<form:hidden path="faculty.items[${loop.index}].id" />
									<form:hidden path="faculty.items[${loop.index}].encodedImage" />


									<form:hidden path="schoolId" />
									<input type="hidden" value="faculty" id="topic" name="topic" />
									<input type="hidden"
										value="${schoolProfile.faculty.items[loop.index].id}"
										id="itemId" name="itemId" />
									<input type="file" id="image" name="image" />
									<input type="submit" value="click to update"
										style="width: 100%;" />

								</form:form>

							</div>
						</div>
					</div>
				</div>

			</c:forEach>


		</div>
		<!--  Faculty Modal -->



		<div class="clearfix"></div>
	</div>

	<!-- //Faculty Section -->



	<!-- Stats Section -->
	<div class="stats" id="stats">
		<div class="container">
			<div class="stats-info">
				<!-- 				<div class="col-md-3 col-sm-3 stats-grid slideanim"> -->
				<!-- 					<div class='numscroller numscroller-big-bottom' data-slno='1' -->
				<!-- 						data-min='0' data-max='12' data-delay='.5' data-increment="100">12</div> -->
				<!-- 					<label class="line"></label> -->
				<!-- 					<p class="stats-info">Rank</p> -->
				<!-- 				</div> -->
				<!-- 				<div class="col-md-3 col-sm-3 stats-grid slideanim"> -->
				<!-- 					<div class='numscroller numscroller-big-bottom' data-slno='1' -->
				<!-- 						data-min='0' data-max='3000' data-delay='.5' data-increment="5">3000</div> -->
				<!-- 					<label class="line"></label> -->
				<!-- 					<p class="stats-info">Computers</p> -->
				<!-- 				</div> -->
				<!-- 				<div class="col-md-3 col-sm-3 stats-grid slideanim"> -->
				<!-- 					<div class='numscroller numscroller-big-bottom' data-slno='1' -->
				<!-- 						data-min='0' data-max='9000' data-delay='.5' data-increment="100">9000</div> -->
				<!-- 					<label class="line"></label> -->
				<!-- 					<p class="stats-info">Books</p> -->
				<!-- 				</div> -->
				<!-- 				<div class="col-md-3 col-sm-3 stats-grid slideanim"> -->
				<!-- 					<div class='numscroller numscroller-big-bottom' data-slno='1' -->
				<!-- 						data-min='0' data-max='169' data-delay='.5' data-increment="1">169</div> -->
				<!-- 					<label class="line"></label> -->
				<!-- 					<p class="stats-info">Awards</p> -->
				<!-- 				</div> -->
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- Stats Section -->




	<!-- ////////////////////////////Events Section/////////////////////////// -->




	<div class="events" id="events">
		<div class="container">
			<h3>${schoolProfile.event.heading }</h3>
			<label class="line"></label>
			<h6>${fn:substring(schoolProfile.gallery.description,0,152) }</h6>


			<c:forEach var="item" varStatus="loop"
				items="${schoolProfile.event.items}">



				<div class="col-md-6 col-sm-6 egrid">
					<div class="img">
						<img src="data:image/jpeg;base64,${item.encodedImage}"
							style="height: 259px; width: 259px;">
					</div>
					<div class="textt">
						<div class="date">
							<h5>15 AUG</h5>
						</div>
						<h3>${item.heading }</h3>
						<label class="eline"></label>
						<p>${item.description }</p>


						<security:authorize access="hasRole('ROLE_HEAD')">
							<a href="#" data-toggle="modal" data-target="#${item.id}"
								id="exp" style="width: 120px;">CUSTOMIZE</a>
						</security:authorize>

						<security:authorize access="isAnonymous()">
							<a href="#" id="exp" data-toggle="modal"
								data-target="#exp<c:out value='${item.id }'/>"
								style="width: 100px;">EXPLORE</a>
						</security:authorize>


						<!-- this will explore event item -->
						<div class="modal fade" id="exp<c:out value='${item.id }'/>"
							role="dialog">
							<div class="modal-dialog">
								Modal content
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>

										<p>${item.description }</p>


									</div>
								</div>
							</div>
						</div>

						<!-- This is will Customize for event item-->
						<div class="modal fade" id="${item.id }" role="dialog">
							<div class="modal-dialog">
								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4>${item.heading }</h4>
										<hr>
										<img alt="Kangoo_image"
											src="data:image/jpeg;base64,${item.encodedImage }" />
										<hr>
										<p>${item.description }</p>
										<form:form modelAttribute="schoolProfile"
											enctype="multipart/form-data" action="${updateItem }"
											method="POST">

											<form:input path="event.items[${loop.index}].heading" />

											<form:input path="event.items[${loop.index}].description" />
											<form:hidden path="event.items[${loop.index}].id" />
											<form:hidden path="event.items[${loop.index}].encodedImage" />
											<%-- 											<input type="hidden" value="${schoolProfile.event.items.id }" id="index" --%>
											<!-- 												name="index" /> -->

											<form:hidden path="schoolId" />
											<input type="hidden"
												value="${schoolProfile.event.items[loop.index].id}"
												id="itemId" name="itemId" />
											<input type="hidden" value="event" id="topic" name="topic" />

											<input type="file" id="image" name="image" />
											<input type="submit" value="click to update"
												style="width: 100%;" />

										</form:form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</c:forEach>
			<div class="clearfix"></div>
		</div>
	</div>


	<!-- Clients Section -->
	<div class="clients">
		<div class="container">
			<%-- 			<h3>${schoolProfile.fullName }</h3> --%>
			<!-- 			<ul> -->
			<!-- 				<li><a>WIPRO</a></li> -->
			<!-- 				<li><a>ATOS</a></li> -->
			<!-- 				<li><a>TECHM</a></li> -->
			<!-- 				<li><a>INFOSYS</a></li> -->
			<!-- 				<li><a>CTS</a></li> -->
			<!-- 			</ul> -->
		</div>
	</div>
	<!--// Clients Section -->
	<!-- Contact Section -->
	<div class="contact" id="contact">
		<div class="container">
			<div class="ctop">
				<h3>CONTACT</h3>
				<label class="line"></label>
				<p>Lorem Ipsum is simply dummy text of the printing and
					typesetting industry. Lorem Ipsum has been the industry's standard
					dummy text ever since the 1500s.</p>
			</div>
			<div class="cbottom">
				<div class="col-md-6 col-sm-6 cbl">
					<h4>OUR ADDRESS:</h4>
					<h5>
						flat no:42/16, <br>GM Chowdhury Road, <br>Khulna.
					</h5>
					<h4>Phone:</h4>
					<h5>+880 1918 691 601</h5>
					<h4>E-mail:</h4>
					<h5>
						<a href="mailto:info@example.com">mlsbd@live.com</a>
					</h5>
					<h4>Our Website:</h4>
					<h5>
						<a href="https://w3layouts.com/">www.w3layouts.com</a>
					</h5>
				</div>
				<div class="col-md-6 col-sm-6 cbr">
					<form action="#" method="post">
						<input type="text" name="your name" placeholder="your Name"
							required=""> <input type="text" name="your phone"
							placeholder="your Phone" required=""> <input type="text"
							name="your email" placeholder="your Email" required="">
						<textarea rows="5" cols="50" name="your comment"
							placeholder="Write Your Comment Here"></textarea>
						<br> <input type="submit" value="SEND MESSAGE">
					</form>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- //Contact Section -->
	<!-- Social Icons Section -->
	<div class="socialicons">
		<div class="container">
			<div class="col-md-4 col-sm-4 left">
				<div class="lt">
					<a class="google" href="${schoolProfile.googleAddress }"></a>
					<h3>GOOGLE</h3>
					<h5>We Have A Circle</h5>
				</div>
				<div class="lb">
					<a class="linkedin" href="${schoolProfile.linkedinAddress }"></a>
					<h3>LINKEDIN</h3>
					<h5>We Are Here Too</h5>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 middle">
				<a class="facebook" href="${schoolProfile.facebookAddress }"></a>
				<h3>FACEBOOK</h3>
				<h5>Join Our Community</h5>
			</div>
			<div class="col-md-4 col-sm-4 right">
				<div class="rt">
					<a class="twitter" href="${schoolProfile.twitterAddress }"></a>
					<h3>TWITTER</h3>
					<h5>Follow Us</h5>
				</div>
				<div class="rb">
					<a class="pinterest" href="${schoolProfile.pinterestAddress }"></a>
					<h3>PINTEREST</h3>
					<h5>Just Pin It</h5>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //Social Icons Section -->
	<!-- Footer Section -->
	<div class="footer">
		<div class="container">
			<p>
				&copy; 2016 IOwnSchool. All Rights Reserved | Design by <a
					href="http://facebook.com">IOwnSchool</a>
			</p>
		</div>
	</div>
	<!-- //Footer Section -->
	<!-- js files -->
	<script
		src="<c:url value = '/resources/front_resources/js/jquery.min.js'/>"></script>
	<script
		src="<c:url value = '/resources/front_resources/js/bootstrap.min.js'/>"></script>

	<!-- Scripts For Navbar -->

	<script
		src="<c:url value = '/resources/front_resources/js/jquery.scrollTo.min.js'/>"></script>

	<!--// Scripts For Navbar -->

	<!-- Scripts For Gallery Section -->
	<script
		src="<c:url value = '/resources/front_resources/js/jquery.localScroll.min.js'/>"></script>
	<script
		src="<c:url value = '/resources/front_resources/js/jquery.magnific-popup.min.js'/>"></script>
	<script
		src="<c:url value = '/resources/front_resources/js/common.js'/>"></script>
	<!--// Scripts For Gallery Section -->

	<script>
		$(document).ready(
				function() {
					// Add smooth scrolling to all links in navbar + footer link
					$(".navbar a, footer a[href='#myPage']").on('click',
							function(event) {

								// Store hash
								var hash = this.hash;

								// Using jQuery's animate() method to add smooth page scroll
								// The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
								$('html, body').animate({
									scrollTop : $(hash).offset().top,
								}, 900, function() {

									// Add hash (#) to URL when done scrolling (default click behavior)
									window.location.hash = hash;
								});
							});
				})
	</script>

	<!-- /js files -->
	<!-- Script For Number Scrolling -->
	<script type="text/javascript"
		src="<c:url value = '/resources/front_resources/js/numscroller-1.0.js'/>"></script>

	<!-- //Script For Number Scrolling -->
	<script
		src="<c:url value = '/resources/front_resources/js/responsiveslides.min.js'/>"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function() {
			// Slideshow 4
			$("#slider3").responsiveSlides({
				auto : true,
				pager : true,
				nav : false,
				speed : 500,
				namespace : "callbacks",
				before : function() {
					$('.events').append();
				},
				after : function() {
					$('.events').append();
				}
			});
		});
	</script>
</body>
</html>