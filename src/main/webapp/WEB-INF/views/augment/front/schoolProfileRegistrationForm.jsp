<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<form:form modelAttribute="newHead"
		action="/project/registration" method="POST">
		<table>
			<tr>
				<td>First Name</td>
				<td><form:input path="firstName" /></td>

			</tr>
			<tr>
				<td>Last Name</td>
				<td><form:input path="lastName" /></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><form:input path="email" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><form:input path="password" /></td>

			</tr>
			<tr>
				<td colspan="2">Fill School's Data</td>
			<tr>
				<td>School Id(Sub URL)</td>
				<td><form:input path="school.schoolId" /></td>
			</tr>
			<tr>
				<td>Full Name</td>
				<td><form:input path="school.fullName" /></td>
			</tr>

			<tr>
				<td>Short Name</td>
				<td><form:input path="school.shortName" /></td>
			</tr>


		</table>
		<input type="submit" />

	</form:form>
</body>
</html>