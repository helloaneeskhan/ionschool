<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ include file="/WEB-INF/views/augment/jsp-f/contextSetting.jspf"%>
<%@ include file="/WEB-INF/views/augment/jsp-f/urls.jspf"%>

<!DOCTYPE HTML>
<html>

<head>
<title>${context_school.fullName}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Augment Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />


<%@ include file="/WEB-INF/views/augment/jsp-f/design-css.jspf"%>
<%@ include file="/WEB-INF/views/augment/jsp-f/head-script.jspf"%>


<script type="text/javascript">
	$(function() {

		var availableTags = [ "ActionScript", "AppleScript", "Asp", "BASIC",
				"C", "C++", "Clojure", "COBOL", "ColdFusion", "Erlang",
				"Fortran", "Groovy", "Haskell", "Java", "JavaScript", "Lisp",
				"Perl", "PHP", "Python", "Ruby", "Scala", "Scheme" ];

		$("#classIdNewActivity").autocomplete({
			source : function(request, response) {
				$.ajax({
					url : "/project/static/school-class-ajax",
					type : "GET",
					data : {
						term : request.term
					},

					dataType : "json",

					minLength : 5,

					success : function(data) {
						response($.map(data, function(item) {
							return {
								label : item.classId,
								value : item.classId
							};
						}));
					}
				});
			}
		});

		$("#teacherIdNewActivity").autocomplete({
			source : function(request, response) {
				$.ajax({
					url : "/project/static/teacher-ajax",
					type : "GET",
					data : {
						term : request.term
					},

					dataType : "json",

					minLength : 5,

					success : function(data) {
						response($.map(data, function(item) {
							return {
								label : item.id,
								value : item.id
							};
						}));
					}
				});
			}
		});

		$("#accountSearchHeadersdf").autocomplete({
			source : availableTags
		})
	});
</script>

</head>


<body>
	<div class="page-container">
		<!--/content-inner-->
		<div class="left-content">
			<div class="inner-content">
				<!-- header-starts -->

				<%@ include file="/WEB-INF/views/augment/jsp-f/header.jspf"%>

<%-- 					<form:form method="GET"> --%>
<!-- 						<input type="text" name="jusername" /> <input type="text" -->
<!-- 							name="password" /> -->
<!-- 							<input type = "submit"/> -->
<%-- 					</form:form> --%>
				<!-- //header-ends -->
				<!--//outer-wp-->
				<div class="outter-wp">
					<!--/sub-heard-part-->
					<div class="sub-heard-part">
						<ol class="breadcrumb m-b-0">
							<li><a href="index.html">Home</a></li>
							<li class="active">Forms Validation</li>
						</ol>
					</div>
					<!--/sub-heard-part-->

					<c:choose>
						<c:when test="${activity == 'create_teacher'}">
							<%@ include
								file="/WEB-INF/views/augment/jsp-f/teacher-registration-form.jspf"%>
						</c:when>
						<c:when test="${activity == 'create_class'}">
							<%@ include
								file="/WEB-INF/views/augment/jsp-f/class-registration-form.jspf"%>
						</c:when>
						<c:when test="${activity == 'create_student' }">
							<%@ include
								file="/WEB-INF/views/augment/jsp-f/student-registration-form.jspf"%>
						</c:when>
						<c:when test="${activity == 'create_subject' }">
							<%@ include
								file="/WEB-INF/views/augment/jsp-f/subject-registration-form.jspf"%>
						</c:when>
					</c:choose>
					<!--//forms-->
				</div>
				

				<!--//outer-wp-->
				<!--footer section start-->
				<%@ include file="/WEB-INF/views/augment/jsp-f/footer.jspf"%>
				<!--footer section end-->
			</div>
		</div>
		<!--//content-inner-->
		<!--sidebar-menu-->
		<%@ include file="/WEB-INF/views/augment/jsp-f/sidebar.jspf"%>
	</div>
	<%@ include file="/WEB-INF/views/augment/jsp-f/body-script.jspf"%>
</body>
</html>