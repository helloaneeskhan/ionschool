<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ include file="/WEB-INF/views/augment/jsp-f/urls.jspf"%>

<!DOCTYPE HTML>
<html>
<head>
<title>${context_school.fullName}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Augment Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!-- //lined-icons -->
<%@ include file="/WEB-INF/views/augment/jsp-f/design-css.jspf"%>
<%@ include file="/WEB-INF/views/augment/jsp-f/head-script.jspf"%>
<!--clock init-->
</head>
<body>
	<div class="page-container">
		<!--/content-inner-->
		<div class="left-content">
			<div class="inner-content">
				<!-- header-starts -->

				<%@ include file="/WEB-INF/views/augment/jsp-f/header.jspf"%>

				<!-- //header-ends -->
				<!--//outer-wp-->
				<div class="outter-wp">
					<!--sub-heard-part-->
					<div class="sub-heard-part">
						<ol class="breadcrumb m-b-0">
							<li><a href="index.html">Home</a></li>
							<li class="active">Blank Page</li>
						</ol>
					</div>
					<!--//sub-heard-part-->
					<div class="graph-visual tables-main">
						<h2 class="inner-tittle">Blank Page</h2>
						<div class="graph">
							<div class="block-page">
								<h1>Its your work place</h1>
								<br>
								<h1>
									Testing message's code,
									<spring:message code="page.title.message" />
								</h1>
								<br>
								<div style="margin: 0px;">
									<div style="margin: 2.5%; width: 40%; border-style: dotted;">
										<h1>testing</h1>
										<h1>testing</h1>
										<h1>testing</h1>

									</div>
									<div style="width: 40%; border-style: dotted;">
										<h1>testing</h1>
										<h1>testing</h1>
										<h1>testing</h1>
									</div>

								</div>
							</div>

						</div>

					</div>
					<!--//graph-visual-->
					<div class="col-md-6 form-group1">
						<div class="col-md-6 form-group1" ><input type="text" placeholder="First Name:" style = ""/></div><div class="col-md-6 form-group1"><input type = "text" placeholder="Last Name:"/></div>
						<div class="col-md-12 form-group1 group-mail"><input type = "text" placeholder="Email Address:"/></div>
					</div>
					<div class="col-md-6 form-group1">
						<input type="text" />
					</div>
					<div class="col-md-12 form-group1 group-mail">
					<div class="col-md-6 form-group1">
				<center>		<input type="submit" value="register" id="sub" style = "width:100%" /></center>
					</div>
					<div class="col-md-6 form-group1">
						<center><input type="submit" value="register" id="sub" style = "width:100%"/></center>
					</div>
</div>

				</div>
				<!--//outer-wp-->
				<!--footer section start-->
				<%@ include file="/WEB-INF/views/augment/jsp-f/footer.jspf"%>
				<!--footer section end-->
			</div>
			<div style="height: 219px; width: 553px;"></div>
		</div>
		<!--//content-inner-->
		<!--/sidebar-menu-->
		<%@ include file="/WEB-INF/views/augment/jsp-f/sidebar.jspf"%>
	</div>
	<%@ include file="/WEB-INF/views/augment/jsp-f/body-script.jspf"%>
</body>
</html>