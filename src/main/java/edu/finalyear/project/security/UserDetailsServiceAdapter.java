package edu.finalyear.project.security;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.dialect.TeradataDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.finalyear.project.dao.AccountDAO;
import edu.finalyear.project.dao.ClassAndSubjectDAO;
import edu.finalyear.project.models.Account;
import edu.finalyear.project.models.Guardian;
import edu.finalyear.project.models.Head;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Subject;
import edu.finalyear.project.models.Teacher;
//import edu.finalyear.project.service.A;
import edu.finalyear.project.service.AccountService;
import edu.finalyear.project.service.ClassAndSubjectService;

@Service("userDetailsService")
@Transactional(readOnly = true)
public class UserDetailsServiceAdapter implements UserDetailsService {

	@Autowired
	@Qualifier("AccountDAO")
	private AccountDAO accountDao;

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;// only for teacher to extract
											// subjects

	@Autowired
	@Qualifier("ClassAndSubjectDAO")
	private ClassAndSubjectDAO csDao;

	List<String> subjectsId = new ArrayList<String>();
	List<String> classIds = new ArrayList<String>();

	@Override
	public UserDetails loadUserByUsername(String id)
			throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		Account account = accountDao.getAccount(id);

		System.out.println("UserDet.. account = " + account);
		if (SecurityUtils
				.hasRoleOrPermissionVIAAccount(account, "ROLE_STUDENT")) {
			// if (account instanceof Student) {
			System.out.println("UserDetailServiceAdapter, student instance");
			Iterator<Subject> iterator = csDao
					.getSchoolClass(account.getClassId(), account.getSchoolId())
					.getSubjects().iterator();

			if (!iterator.hasNext()) {
				subjectsId.add("No Subject Id");
			}

			while (iterator.hasNext())
				subjectsId.add(iterator.next().getSubjectId());
			account.setSubjectsIds(subjectsId);
		} else if (SecurityUtils.hasRoleOrPermissionVIAAccount(account,
				"ROLE_TEACHER")) {
			// if (account instanceof Teacher) {

			// do work for teacher;
			System.out.println("UserDetailServiceAdapter, teacher instance");
			// Teacher teacher =
			// accountService.getTeacherAccount(account.getId(),true);
			// Iterator<Subject> iterator =teacher.getSubjects().iterator();
			Iterator<Subject> iterator = accountDao
					.getTeacherAccount(account.getId()).getSubjects()
					.iterator();
			if (!iterator.hasNext()) {
				subjectsId.add("No Subject Id");
			}
			while (iterator.hasNext())
				subjectsId.add(iterator.next().getSubjectId());
			account.setSubjectsIds(subjectsId);
		} else if (SecurityUtils.hasRoleOrPermissionVIAAccount(account,
				"ROLE_GUARDIAN")) {
			// if (account instanceof Guardian) {
			System.out
					.println("UserDetailServiceAdapter, Guardian instance id= "
							+ account.getId());
			String sId = account.getId().split("guardian_")[1];
			System.out.println("UserDetailServiceAdapter, sId = " + sId);
			Student student = accountDao.getStudentAccount(sId);
			Iterator<Subject> iterator = csDao
					.getSchoolClass(student.getClassId(), account.getSchoolId())
					.getSubjects().iterator();
			if (!iterator.hasNext()) {
				subjectsId.add("No Subject Id");
			}
			while (iterator.hasNext()) {
				// System.out.println("UserDetailServiceAdapter, subId = "+
				// iterator.next().getSubjectId());
				subjectsId.add(iterator.next().getSubjectId());
			}
			account.setSubjectsIds(subjectsId);
		} else if (SecurityUtils.hasRoleOrPermissionVIAAccount(account,
				"ROLE_HEAD")) {
			Iterator<SchoolClass> iterator = csDao.getSchoolClassVIASchoolId(
					account.getSchoolId()).iterator();
			if (!iterator.hasNext()) {
				classIds.add("No Class Id");
			}
			while (iterator.hasNext()) {
				String temp = iterator.next().getClassId();
				classIds.add(temp);
				System.out
						.println("UserDetaisServicdAdapter:/loadUser.... classId = "
								+ temp);
			}
			account.setClassIds(classIds);
		}

		for (Role role : account.getRoles())
			System.out.println("UserDetailsServiceAdapter:/loadUser.... role="
					+ role);

		// if(account == null)
		// throw new
		// UsernameNotFoundException("Username with id"+id+" is empty");
		//
		// if(account.getAuthorities().isEmpty() )
		// throw new
		// UsernameNotFoundException("Username with id"+account.getId()+" has no role");
		System.out.println("UserDetaisServicdAdapter:/loadUser.... account="
				+ account);
		return account;
	}

}
