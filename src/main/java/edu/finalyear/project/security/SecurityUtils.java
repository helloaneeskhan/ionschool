package edu.finalyear.project.security;

import java.security.Principal;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import edu.finalyear.project.models.Account;
import edu.finalyear.project.models.Test;

public class SecurityUtils {

	public static Account getLogedInAccount() {
		Account currentAccount = (Account) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		System.out.println("SecurityUtils.getCurrentUser() Account ="
				+ currentAccount.getFirstName() + " "
				+ currentAccount.getLastName());
		return currentAccount;
	}

	public static Set<Role> getRoles() {
		Set<Role> role = getLogedInAccount().getRoles();
		return role;
	}

	public static boolean hasRoleOrPermissionVIAAccount(Account account, String auth) {
		boolean exists = false;
		for (Role x : account.getRoles()) {
			System.out.println("SecurityUtills:/ hasRoleOrPermission: role = "
					+ x);
			if (x.getName().equals(auth)) {
				exists = true;
				break;
			}
		}
		return exists;
	}

	public static boolean hasRoleOrPermission(String auth) {
		boolean exists = false;
		Iterator<Role> roleI = getLogedInAccount().getRoles().iterator();
		for (Role x : getLogedInAccount().getRoles()) {
			System.out.println("SecurityUtills:/ hasRoleOrPermission: role = "
					+ x);
			if (x.getName().equals(auth)) {
				exists = true;
				break;
			}
		}
		System.out.println("SecurityUtills:/ hasRoleOrPermission: existing");
		// while (roleI.hasNext()) {
		// System.out.println("SecurityUtills:/ hasRoleOrPermission: role = "+roleI.next());
		// if (roleI.next().getName().equals(auth)) {
		// exists = true;
		// break;
		// }
		// }
		return exists;
	}
}
