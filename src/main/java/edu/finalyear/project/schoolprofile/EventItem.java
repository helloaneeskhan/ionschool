package edu.finalyear.project.schoolprofile;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "items")
@DiscriminatorValue("EventItem")
public class EventItem extends Items implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2082608074626624009L;


	@Column(name = "subheading")
	private String subHeading;

	@Column(name = "description")
	private String description;


	public EventItem() {
	}

	public EventItem(String heading, String subHeading, String description, long eventId) {
		super(heading,eventId);
		this.subHeading = subHeading;
		this.description=description;
	}

	public String getSubHeading() {
		return subHeading;
	}

	public void setSubHeading(String subHeading) {
		this.subHeading = subHeading;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	

	public String getDescription() {
		return description;
	}


}
