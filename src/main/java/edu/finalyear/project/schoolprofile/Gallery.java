package edu.finalyear.project.schoolprofile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "link")
@DiscriminatorValue("Gallery")
public class Gallery extends Link {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@OneToMany( fetch = FetchType.EAGER)
	@JoinColumn(name = "link_id",insertable=true,updatable=false)
	private List<GalleryItem> items = new ArrayList<GalleryItem>();

	public Gallery() {
		super();
	}

	public Gallery(String discription) {
		super(discription);
	}

	public List<GalleryItem> getItems() {
		return items;
	}

	public void setItems(List<GalleryItem> items) {
		this.items = items;
	}

}
