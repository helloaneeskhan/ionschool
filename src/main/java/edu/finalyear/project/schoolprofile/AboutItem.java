package edu.finalyear.project.schoolprofile;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//
//@Entity
//@Table(name = "items")
//@DiscriminatorValue("AboutItem")
//public class AboutItem extends Items implements Serializable {
//

@Entity
//@Table(name = "about_item")
@Table(name = "items")
@DiscriminatorValue("AboutItem")
public class AboutItem extends Items implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7755926257718139675L;

//	@Id
//	@Column(name = "id")
//	@GeneratedValue(strategy = GenerationType.AUTO)
//	private long id;
	
	@Column(name = "subheading")
	private String subHeading;

	@Column(name = "description")
	private String description;

//	@ManyToOne
//	@JoinColumn(name = "link_id")
//	private About about;
	
	
	public AboutItem() {
	}

	public AboutItem(String heading, String subHeading, String description) {
		super(heading);
		this.subHeading = subHeading;
		this.description = description;
	}
	
	public AboutItem(String heading, String subHeading, String description,long aboutId) {
		super(heading,aboutId);
		this.subHeading = subHeading;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSubHeading() {
		return subHeading;
	}

	public void setSubHeading(String subHeading) {
		this.subHeading = subHeading;
	}

	
//	public long getId() {
//		return new Long(id);
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}

//	public About getAbout() {
//		return about;
//	}
//
//	public void setAbout(About about) {
//		this.about = about;
//	}

	@Override
	public String toString() {
		return "Item [ subHeading=" + subHeading + "]";
	}

}
