package edu.finalyear.project.schoolprofile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "link")
@DiscriminatorValue("Event")
public class Event extends Link {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2817248598250423946L;

	@OneToMany(  fetch = FetchType.EAGER)
	@JoinColumn(name = "link_id",insertable=true,updatable=false)
	private List<EventItem> items = new ArrayList<EventItem>();

	public Event(String description) {
		super(description);
	}

	public Event() {
	}

	public List<EventItem> getItems() {
		return items;
	}

	public void setItems(List<EventItem> items) {
		this.items = items;
	}

}
