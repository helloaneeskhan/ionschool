package edu.finalyear.project.schoolprofile;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.ws.FaultAction;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import edu.finalyear.project.models.Head;
import edu.finalyear.project.models.School;

@Component("SchoolProfileUtils")
public class SchoolProfileUtils {

	@Value("#{default.aboutDescription}")
	private String ABOUT_DESCRIPTION;
	
	@Value("#{default.aboutDescription}")
	private String aboutDescription2;
	
	
	@Value("#{default.aboutItemSubheading}")
	private String ABOUT_ITEM_SUBHEADING;
	
	@Value("#{default.aboutItemHeading}")
	private String ABOUT_ITEM_HEADING;
	
	@Value("#{default.aboutItemDescription}")
	private String ABOUT_ITEM_DESCRIPTION;
	
	@Value("#{default.wordAboutUs}")
	private String ABOUT_WORDS;
	
	//////////////////event//////////////////////
	
	@Value("#{default.eventDescription}")
	private String EVENT_DESCRIPTION;
	
	@Value("#{default.eventItemHeading}")
	private String EVENT_ITEM_HEADING;
	
	@Value("#{default.eventItemSubheading}")
	private String EVENT_ITEM_SUBHEADING;
	
	@Value("#{default.eventItemDescription}")
	private String EVENT_ITEM_DESCRIPTION;
	
	///////////////faculty/////////////////////
	
	@Value("#{default.facultyDescription}")
	private String FACULTY_DESCRIPTION;
	
	@Value("#{default.facultyItemHeading}")
	private String FACULTY_ITEM_HEADING;
	
	@Value("#{default.facultyItemSubheading}")
	private String FACULTY_ITEM_SUBHEADING;
	
	///////////////////gallery/////////////////
	
	@Value("#{default.galleryDescription}")
	private String GALLERY_DESCRIPTION;
	
	@Value("#{default.galleryItemHeading}")
	private String GALLERY_ITEM_HEADING;
	

	
	public static AboutItem getAboutItem(List<AboutItem> items, int id) {//filter return
		for (AboutItem item : items) {
			if (item.getId() == id) {
				return item;
			}

		}
		return null;
	}

	public About getAboutTemplate() {
		return new About(ABOUT_DESCRIPTION, ABOUT_WORDS);
	}

	public Gallery getGalleryTemplate() {
		return new Gallery(GALLERY_DESCRIPTION);

	}

	public Faculty getFacultyTemplate() {
		return new Faculty(FACULTY_DESCRIPTION);
	}

	public Event getEventTemplate() {
		return new Event(EVENT_DESCRIPTION);
	}

	
	public AboutItem getAboutItemTemplate(long aboutId) {
		return new AboutItem(ABOUT_ITEM_HEADING, ABOUT_ITEM_SUBHEADING,
				ABOUT_ITEM_DESCRIPTION, aboutId);

	}

	
	public GalleryItem getGalleryItemTemplate(long galleryId) {
		return new GalleryItem(GALLERY_ITEM_HEADING, galleryId);

	}
	
	
	public EventItem getEventItemTemplate(long eventId){
		return new EventItem(EVENT_ITEM_HEADING, EVENT_ITEM_SUBHEADING, EVENT_ITEM_DESCRIPTION, eventId);
	}

	
	public FacultyItem getFacultyItemTemplate(long facultyId){
		return new FacultyItem(FACULTY_ITEM_HEADING, FACULTY_ITEM_HEADING, facultyId);
	}
	
	
	public School getWithLinkCustomisable(School schoolProfile) {
		schoolProfile.setAbout(getAboutTemplate());
		schoolProfile.setEvent(getEventTemplate());
		schoolProfile.setFaculty(getFacultyTemplate());
		schoolProfile.setGallery(getGalleryTemplate());
		return schoolProfile;
	}

	public static SchoolProfile getWithItemsCustomisable(
			SchoolProfile schoolProfile) {
		// schoolProfile.setAbout(a);
		return schoolProfile;
	}

	public static String getGeneratedHeadId(Head head) {
		String headId = head.getEmail().split("@")[0] + "@"
				+ head.getSchool().getSchoolId();
		System.out.printf("Generated Head Id = %s\n", headId);
		return headId;
	}

}
