package edu.finalyear.project.schoolprofile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "link")
@DiscriminatorValue("Faculty")
public class Faculty extends Link {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6455008177248364791L;

	@OneToMany( fetch = FetchType.EAGER)
	@JoinColumn(name = "link_id",insertable=true,updatable=false)
	private List<FacultyItem> items = new ArrayList<FacultyItem>();

	public Faculty() {
		super();
	}

	public Faculty(String description) {
		super(description);

	}

	public List<FacultyItem> getItems() {
		return items;
	}

	public void setItems(List<FacultyItem> items) {
		this.items = items;
	}

}
