package edu.finalyear.project.schoolprofile;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "link")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "heading", discriminatorType = DiscriminatorType.STRING)
public abstract class Link implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3334610132944598602L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected long id;

	@Column(name = "description")
	protected String description;

	@Column(name = "heading", insertable=false, updatable = false)
	protected String heading;

	@OneToOne( orphanRemoval = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "school_id")
	protected SchoolProfile schoolProfile;
	public Link() {

	}

	public Link(String description) {
		super();
		this.description = description;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public SchoolProfile getSchoolProfile() {
		return schoolProfile;
	}

	public void setSchoolProfile(SchoolProfile schoolProfile) {
		this.schoolProfile = schoolProfile;
	}
	
	

}
