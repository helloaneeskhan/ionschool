package edu.finalyear.project.schoolprofile;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "items")
@DiscriminatorValue("GalleryItem")
public class GalleryItem extends Items{



	public GalleryItem() {
	}
	
	public GalleryItem(String heading){
		super(heading);
	}
	public GalleryItem(String heading,long galleryId) {
		super(heading, galleryId);
	}

}
