package edu.finalyear.project.schoolprofile;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "items")
@DiscriminatorValue("FacultyItem")
public class FacultyItem extends Items{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "subheading")
	private String subHeading;

	public FacultyItem(String heading, String subHeading, long facultyId) {
		super(heading,facultyId);
		this.subHeading = subHeading;
	}

	public FacultyItem() {
		super();
	}

	public String getSubHeading() {
		return subHeading;
	}

	public void setSubHeading(String subHeading) {
		this.subHeading = subHeading;
	}

}
