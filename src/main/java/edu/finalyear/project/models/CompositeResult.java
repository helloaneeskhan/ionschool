package edu.finalyear.project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "c_record")
public class CompositeResult {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "subject_id")
	private String subjectId;

	@Column(name = "student_id")
	private String studentId;

	@Column(name = "num_of_test")
	private int numOfTest;

	@Column(name = "com_percent")
	private double comPercent;

	public CompositeResult() {

	}

	public CompositeResult(String subjectId, String studentId, int numOfTest,
			double comPercent) {
		super();
		this.subjectId = subjectId;
		this.studentId = studentId;
		this.numOfTest = numOfTest;
		this.comPercent = comPercent;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public int getNumOfTest() {
		return numOfTest;
	}

	public void setNumOfTest(int numOfTest) {
		this.numOfTest = numOfTest;
	}

	public double getComPercent() {
		return comPercent;
	}

	public void setComPercent(double comPercent) {
		this.comPercent = comPercent;
	}

	@Override
	public String toString() {
		return "CompositeResult [subjectId=" + subjectId + ", studentId="
				+ studentId + ", numOfTest=" + numOfTest + ", comPercent="
				+ comPercent + "]";
	}

}
