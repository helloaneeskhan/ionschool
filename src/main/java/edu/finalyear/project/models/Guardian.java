package edu.finalyear.project.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name = "guardian")
public class Guardian extends Account{

	@Column(name = "relation")
	private String relation;

	@Column(name = "occupation")
	private String occupation;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@JoinColumn(name = "student_id")
	private Student student;
	
	@Column(name = "student_id", insertable=false, updatable= false)
	private String studentId;
	
	@Transient
	@JsonBackReference
	private List<String> subjectsIds = new ArrayList<String>();

	public Guardian() {

	}

	public Guardian(String guardianId,String firstName, String relation) {
		super();
		this.firstName = firstName;
		this.relation = relation;
		this.id = guardianId;
		
	}

	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	
		
	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	@Override
	public String toString() {
		return "StudentGuardian [guardianId="+id+"firstName=" + firstName + ", lastName="
				+ lastName + ", relation=" + relation + ", occupation="
				+ occupation + ", email=" + email + ", mobile=" + mobile
				+ ", address=" + address + "]";
	}

	@Override
	@Transient
	public List<String> getSubjectsIds() {
		// TODO Auto-generated method stub
		return this.subjectsIds;
	}

	@Override
	@Transient
	public void setSubjectsIds(List<String> subjectsIds) {
		// TODO Auto-generated method stub
		this.subjectsIds = subjectsIds;
	}

	@Override
	public String getClassId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getGuardianStudentId() {
		// TODO Auto-generated method stub
		return this.studentId;
	}

	@Override
	public List<String> getClassIds() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setClassIds(List<String> classIds) {
		// TODO Auto-generated method stub
		
	}

}
