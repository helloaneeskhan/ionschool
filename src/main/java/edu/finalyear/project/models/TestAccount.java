package edu.finalyear.project.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "account")
@Inheritance(strategy = InheritanceType.JOINED)
public class TestAccount implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3997486964815815625L;
	@Id
	@Column(name= "id")
	private String id;
	@Column(name = "first_name")
	private String name;
	
	public TestAccount() {
	
	}
	
	public TestAccount(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "TestAccount [id=" + id + ", name=" + name + "]";
	}
	
	
	
}
