package edu.finalyear.project.models;

import java.io.Serializable;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "teacher")
@NamedQuery(name = "findTeacherByTeacherId", query = "from Teacher where id like :teacherId and schoolId = :schoolId")
public class Teacher extends Account implements Serializable {

	/**
	 * Anees khanzada
	 * 
	 */
	private static final long serialVersionUID = 8885613605359494848L;


	@Column(name = "salary")
	private int salary;

	@Column(name = "qualification")
	private String qualification;
	
	@Column(name = "school_id")
	private String schoolId;

	@OneToMany(fetch = FetchType.LAZY, orphanRemoval = true,mappedBy = "teacher")
//	@JoinColumn(name = "teacher_id")
	@JsonBackReference
	private Set<Subject> subjects = new HashSet<Subject>();
	
	
	@Transient
	@JsonBackReference
	private List<String> subjectsIds = new ArrayList<String>();

	public Teacher() {
	}

	public Teacher(String id, String firstName, String password,
			String qualificition) {
		super(id, firstName, password);
		this.qualification = qualification;
	}


	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	
	

	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
		super.schoolId = schoolId;
	}

	@Transient
	public String getClassId(){
		return "ClassId:Null";
	}
	
	@Override
	@Transient
	public List<String> getSubjectsIds() {
		// TODO Auto-generated method stub
		return this.subjectsIds;
	}

	@Override
	@Transient
	public void setSubjectsIds(List<String> subjectsIds) {
		// TODO Auto-generated method stub
		this.subjectsIds = subjectsIds;
	}	
	
	public Set<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}

	@Transient
	@Override
	public String toString() {
		return "Teacher [id=" + super.getId() + ", firstName=" + super.getFirstName()
				+ ", lastName=" + super.getLastName() + ", email=" + super.getEmail() + ", salary="
				+ salary +", qualification = "+qualification+ "]";
	}

	@Override
	public String getGuardianStudentId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getClassIds() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setClassIds(List<String> classIds) {
		// TODO Auto-generated method stub
		
	}

}
