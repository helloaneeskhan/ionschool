package edu.finalyear.project.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javassist.expr.NewArray;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ManyToAny;

@Entity
@Table(name = "subject")
//@NamedQuery(name = "findSubjectByClassId", )
public class Subject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4681557778024022212L;

	@Id
	@Column(name = "subject_id")
	private String subjectId;

	@Column(name = "full_name")
	private String fullName;

	@Column(name = "short_name")
	private String shortName;

	@Column(name = "school_id")
	private String schoolId;
	
	@Column(name = "class_id")
	private String classId;
	
	@Column(name = "teacher_id", updatable = false,insertable = false)
	private String teacherId;
	
	@ManyToOne(fetch= FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="teacher_id")
	private Teacher teacher;

	@OneToMany
	@JoinColumn(name = "subject_id")
	private List<Post> posts = new ArrayList<Post>();

	@OneToOne(mappedBy = "subject",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Test test;
	
	
	public Subject() {
		

	}

	public Subject(String subjectId, String fullName, String shortName,
			String classId) {
		super();
		this.subjectId = subjectId;
		this.fullName = fullName;
		this.shortName = shortName;
		this.classId = classId;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	
	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	
	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	
	
//	public List<TestRecord> getTestRecords() {
//		return testRecords;
//	}
//
//	public void setTestRecords(List<TestRecord> testRecords) {
//		this.testRecords = testRecords;
//	}

	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}

	@Override
	public String toString() {
		return "Subject [subjectId=" + subjectId + ", fullName=" + fullName
				+ ", shortName=" + shortName + ", classId=" + classId + ", teacherId="+teacherId+"]";
	}

}
