package edu.finalyear.project.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "teacher_account")
public class TestTeacherAccount extends TestAccount implements Serializable{

	private static final long serialVersionUID = 2640068624448598788L;
	
	

	@Column(name = "teacher_class")
	private String teacherClass;

	public TestTeacherAccount() {


	}
	
	public TestTeacherAccount(String id, String accountName,String teacherClass) {
		super(id, accountName);
		this.teacherClass = teacherClass;
	}

	public String getTeacherClass() {
		return teacherClass;
	}

	public void setTeacherClass(String teacherClass) {
		this.teacherClass = teacherClass;
	}

	@Override
	public String toString() {
		return "TestTeacherAccount [teacherClass=" + teacherClass+//"id="+id+
				 ", toString()=" + super.toString() + "]";
	}
	
	
}
