package edu.finalyear.project.models;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Base64;

import edu.finalyear.project.security.Permission;
import edu.finalyear.project.security.Role;

@Entity
@Table(name = "account")
@NamedQuery(name = "findAccountByIds", query = "from Account account where account.schoolId=:schoolId and account.id like :id")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "account_type")
public abstract class Account implements UserDetails {

	
	private static final long serialVersionUID = 1106657964870283843L;

	@Id
	@Column(name = "id")
	protected String id;

	@Size(min = 3, max = 25)
	@Column(name = "first_name")
	protected String firstName;

	@Size(min = 3, max = 25)
	@Column(name = "last_name")
	protected String lastName;

	@Column(name = "profile_photo")
	protected byte[] profilePhoto;

	@Column(name = "email")
	@Email
	protected String email;

	@Column(name = "password")
	protected String password;

	@Column(name = "mobile")
	protected String mobile;

	@Column(name = "address")
	protected String address;

	@Column(name = "gender")
	protected String gender = "Male";

	@Column(name = "enabled")
	protected boolean enabled;

	@Column(name = "school_id")
	protected String schoolId;

	@Column(name = "school_name")
	protected String schoolName;

	@Transient
	private String fullName = firstName+" "+lastName;
	
	@Transient
	protected String encodedProfilePhoto;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "account_role", joinColumns = { @JoinColumn(name = "account_id") }, inverseJoinColumns = { @JoinColumn(name = "role_id") })
	protected Set<Role> roles = new HashSet<Role>();

	

	public Account() {
	}

	public Account(String id, String firstName, String password) {
		super();
		this.id = id;
		this.firstName = firstName;

	}

	public Account(String id,  String password,boolean enabled,String firstName) {
		super();
		this.id = id;
		this.password = password;
		this.enabled=enabled;
		this.firstName = firstName;

	}
	
	public abstract void setClassIds(List<String> classIds);
	
	public abstract List<String> getSubjectsIds();

	public abstract void setSubjectsIds(List<String> subjects);

	public abstract String getGuardianStudentId();
	
	public abstract List<String> getClassIds();
	
	
	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	@Transient
	public String getFullName() {
		return firstName + " " + lastName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public byte[] getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(byte[] profile_photo) {
		this.profilePhoto = profile_photo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

//	@Transient
//	public Set<Permission> getPermissions() {
//		Set<Permission> permissions = new HashSet<Permission>();
//		for (Role role : roles)
//			permissions.addAll(role.getPermissions());
//
//		return permissions;
//	}



	@Override
	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		Set<GrantedAuthority> authority = new HashSet<GrantedAuthority>();

		authority.addAll(roles);
//		authority.addAll(getPermissions());
		// Set<Permission> permissions = new HashSet<Permission>();
		// for (Role role : roles)
		// permissions.addAll(role.getPermissions());
		// authority.addAll(permissions);
		return authority;
	}

	@Override
	@Transient
	public String getUsername() {
		// TODO Auto-generated method stub
		return getId();
	}

	@Override
	@Transient
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	@Transient
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	@Transient
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return enabled;
	}

	@Transient
	public String getEncodedProfilePhoto() {
		if (this.profilePhoto == null || this.profilePhoto.length < 100)
			this.encodedProfilePhoto = "iVBORw0KGgoAAAANSUhEUgAAAygAAAJRCAYAAACurpNMAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABXESURBVHhe7dw9duPYuUDRnpYH4Wl02EOxB+LcyRtAd96h13LkxLlzvZJaVaIoXOCCBKTT0t5r3aj4AwKg8B1Sqp8eAAAAIgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKACT89+Hnv/7fw09X6+ffnv/5wj//9vZ2l2vpPmf41z9+XXz+H+tv/32+5X0qrxd4HwIFABIEyohAga9FoMAJ1i7asxfSzQv/4zro4n+Gqe1/Xn/5x/+e78WX9Z9/P/zl6rz4ekOnQBkRKPC1CBQ4wfpF+/eHfz7fbo1A4StZHEB/+ffDv57//WuYD5QXt9znHIvH8PCfUZ3XC5xHoMAJjvhUUaDwdSwPnT/99deHv//n+SZfgkDZJlDgKxAocKr/Pfz9l7cX030X1OvHmPsGpmOwD77cp+MM/fb72/Pj+wpH+PEEyjaBAl+BQIFTjQNlPjQECp/Z2nvkcf3Zzvf3JlAel0CBz0WgwKk2hq+pIV2g8Ikt/HH89TJ8rhEoj8s5Ap+LQIFTbX06PPP3FwKFz+tyqP35t+Xh07myRqA8LoECn4tAgVNtB8r2HwLfHyiLg8P3dfrwty9QVrd1YV8t3X4t+hb/eP+GfbD+nwDc88fdM+fMYE29jkEEPK53/3uPi2153vbl/Tq3P2f/Y4alYXZ83/X32/3nwfbxXh++7xvYjzyPZwLlnuP7hyMCpfQeAJYIFDjV1fDxy6/Lv86yOljeGigrF+HFddY3M+cEyuYwev34a3+I/bQmhqTNx7heBwx402vj+E38KtX39W7/q9rF/vzxnKPtnBgcN8+J53V3oBx6HnxAoJx0Hq8Gysxz3hHYU4FSfA8AiwQKnOo6UL5dgAcX6vEF8YZA2T2AvKx9n0TOOD5QZgf5H/t0dn+sDUg7hpvXa2a42x5SF9fsp703nA/nD2iXr/n1Plo+vtvn/bsEyuHnwfaxPzRQTjyPh4Gy5/zbjJQbAyX5HgBGBAqc6mr4eL74Lg9DswPMrZ+UXz3+yqBybKQMBrCbBpFfH37+29wQ+rQen2PXYLIyhC3srzf7afRcGyGxfD68Ps77zpkLg216ve3L+3rzse9xuT+v98/UNo8tB87c63nZz4Pbn3ge3DZ83x8oR23/4n4ffWu8stbD4IZ9VH0PAEMCBU61HCjjT02X4mNPoOx53EfLF/vNCNrlyEC5XK+3cftblde3H31iPjfYjffPTGy8Nj8YDT+hHtoxzC0MrtsD9e0uX8vb7bn1nHm29Foe155AGD3XaefBox3H64ed9zlx+9ffg9fn9Ohn1ePa+35Z20fd9wAwJlDgVFcX4cuhZ3qIur6Qr1y8b/nkeXCf43694fhAWd628e2Xh4xzBrvl47rySezS/h/tmz23/WZxyBzefuk4rQ+kt7vc98vPsTwgz3+ivTwsr9//8jnf/Tx4svOcfBI5j78ZBsquc+6PNX7N+15v9z0ArBEocKqrC97VhXF5CLu+2F5fNEcXzNHFfusCO7jfyuC7z62PvzyIjMNp8DzDT0CXb78+DE44IlBG27z02HuHv5VPhJcGzLv3x5KL1zw+nsvHf237X1k8Dmv3v9hfR5z7Nwz47xIosw4LlI2fP7s/INnzesPvAWCVQIFTXV0g3ww+gwvoq4v69W1GF/zbB7qbBotpg9d4U6CsDUjLzzMeLvbeflImUJbPh7VvxpbOg7Xb3+Zyv58w8P4wOO9G9784DocMpALlj7X582fvz609r7f6HgC2CBQ41dWQtDhMbl2grwetwYC1OFDMXVzv/XWadYNB8c8eKINPfpfXynbviY57Y+aGdfhwdrldW8Pr4DXce4zevqbLc2FnmB91HjzZM3x/d8t9Lhy4/bcFyt6fDzteb/U9AGwSKHCqq4vvnsHz2/rjont9AT8+UNaf/157B5DvioGyPBxtr7Xtnn+dSwPgcHsPGs62B8x9lr8V2bk2z53vBufe9Xvo4vyfG0bPOA8e7Ri+fzjuPtvrawbK0e8BYJtAgVNdXXxXBqvlwe1xILi+gAuUZcvPM34NO29/17CzPtjNfIO1eH6s7cPkcHbrYHy91vfnK4Nz+/J98bJvJ749OfE8ODI2PuI8FijAUQQKnOrq4rs6lA+Gt19+f/j51QX8+EDxK15btx+8hsd1PbwsHoftfbkcqGtrY5genA/rw+7JBrFwy5r7puPR4H31ff9d7Kftxzz7PNgZG0/23Ofc7f+zBMqHvgeAKQIFTnV18d0aygcX1NdrNJgOBrGJT/+Wh+OJT5On7B1Avlt6PR8UKIuD9WBbbhpMn00P8DPHZvl8mB/sj3a5vyf3x7N7z8+1/y3v5bEntun082BPbHy34z4nb/9tgbL359aefVR7DwCzBAqcamegfDMapl7WaDAbhMDmIDe438S2zrn18ZeGi7UBafl5xsPd/O0Xj8lo++8JlG/Wj/++aFwcGA87rjtd7pe92zAIt/XB/dJgCL5cEyF//nmwZ/j+bv4+Z2//TYGy+Dxrr3nfPkq9B4BpAgVOdTUET10YBwP9j7UypN4yyA3uc9ynjJ80UEaD162BcnW/Q/b/3YP9cS4Hxf3PPwiMHYPmevjNBeT558EHBMqB239LoCzeZzXEd+6j0HsAmCdQ4FS3BMo3g08V/1hrF+9R3IzuMxj8dn5Sv+6TBspgHy0PXBsD0ZvjPTcwb9t7Prz29Fq2PgGfcnksbzu3lo/Bjv209p6afI2nnwd7h+8n8/c5e/uX77NyjEbHZPV47N1HlfcAsIdAgVNdXRwP+cR348I6HMSuBoWVgW19INprMCD8iQJluK9evYbRIPS8Voac0TC4uabOp+WB7mkNtunV9hwxnF18in3zN0ODY7Dn8W4Zul85+Tw4O1A+9Dy+PlcH32xsh8Nx++hpvdd7ANhFoMAJxnHxsmYGq+UL/sQnf8OL//aaHtY2zOyD7+tyX6wOORfrx3auhNblenmOlWHl1Xq9n2e36+l+K9u0dNz37KvFtRkqs695Yd01nK0978R5/M3u82HN0nGZirwXx58HG0Hwar3ss13b8XyfR2eex/OPPVrjDyBufb0vPuo9ANxCoMAJjgqU5Yvq6AJ8be8FefZx53y2QHm0uW0Xw+7o9Y+O+73D3cz5dFMIfaZA+eb68W4J8mPPg/cNlEfHbv+Lt4/7R3DMbevat6P3vd5L7/8eAG4hUOAEMxfBmYHyyZtvQ+YGu0urF/ednyDP2jMIXO6L2UHkIwLlyeLz7bjtaNi541uvp7XrOG7tg/VhcV4vUF7t53vO/cPOg/cPlCcnnMdvt+n14422eebY3f1633iv9wBwC4EC8JHehMnEYLQYM7ODGQC0CRSAj7LwyfTcN2sLn/6e9E0YALw3gQLwQRZ/bWXm992XvkHxe/IAfBICBeCDDH+vfhgbo9+b9+tdAHweAgXgw2z9oe7M8se8AHwuAgXgQ+35X5yull/rAuATEigAFYv/9ev18o0JAJ+bQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABkCBQAACBDoAAAABkCBQAAyBAoAABAhkABAAAyBAoAAJAhUAAAgAyBAgAAZAgUAAAgQ6AAAAAZAgUAAMgQKAAAQIZAAQAAMgQKAACQIVAAAIAMgQIAAGQIFAAAIEOgAAAAGQIFAADIECgAAECGQAEAADIECgAAkCFQAACADIECAABEPDz8P1PJ2vPHyfEfAAAAAElFTkSuQmCC";
		else {
			byte[] encodedBase64 = Base64.encode(this.profilePhoto);
			this.encodedProfilePhoto = new String(encodedBase64);
		}
		
		return this.encodedProfilePhoto;
	}

//	public void setEncodedProfilePhoto(String encodedProfilePhoto) {
//		this.encodedProfilePhoto = encodedProfilePhoto;
//	}

	@Override
	@Transient
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public abstract String getClassId();

	@Override
	@Transient
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Account [id=" + id + ", firstName=" + firstName + ", lastName="
				+ lastName + "schoolId=" + schoolId + ", email=" + email+",password="+password
				+ ",mobile=" + mobile + ", address=" + address + ", enabled="
				+ enabled + "]";
	}


}
