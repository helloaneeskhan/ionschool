package edu.finalyear.project.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "t_record")
public class TestRecord implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8593277790144404949L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "npq")
	private double nPQ;

	@Column(name = "right_ans")
	private int right;

	@Column(name = "student_id")
	private String studentId;

	@Column(name = "subject_id")
	private String subjectId;

	@Column(name = "test_id")
	private long testId;

	@Column(name = "test_name")
	private String testName;

	@Column(name = "total_q")
	private int total;

	@Column(name = "wrong_ans")
	private int wrong;

	@Column(name = "date_id")
	private String dateId;
	
	@Transient
	private double percent;

	@Transient
	private double numSecured;

	@Transient
	private double totalNum;

	public TestRecord() {

	}

	public TestRecord(double nPQ, int right, int wrong, int total, long testId,
			String studentId, String subjectId, String testName, String dateId) {
		super();
		this.nPQ = nPQ;
		this.right = right;
		this.wrong = wrong;
		this.total = total;
		this.testId = testId;
		this.studentId = studentId;
		this.subjectId = subjectId;
		this.testName = testName;
		this.dateId = dateId;
	}


	public int getRight() {
		return right;
	}

	public void setRight(int right) {
		this.right = right;
	}

	public int getWrong() {
		return wrong;
	}

	public void setWrong(int wrong) {
		this.wrong = wrong;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getnPQ() {
		return nPQ;
	}

	public void setnPQ(double nPQ) {
		this.nPQ = nPQ;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public long getTestId() {
		return testId;
	}

	public void setTestId(long testId) {
		this.testId = testId;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
	

	public String getDateId() {
		return dateId;
	}

	public void setDateId(String dateId) {
		this.dateId = dateId;
	}

	@Transient
	public double getPercent() {

		percent = (getNumSecured() / getTotalNum()) * 100.0;
		System.out.println("TestRecord perc = " + percent);
		return percent;
	}

	@Transient
	public void setPercent(double percent) {
		this.percent = percent;
	}

	@Transient
	public double getNumSecured() {
		this.numSecured = nPQ * right;
		return numSecured;
	}

	@Transient
	public void setNumSecured(double numSecured) {
		this.numSecured = numSecured;
	}

	@Transient
	public double getTotalNum() {
		this.totalNum = total * nPQ;
		return totalNum;
	}

	@Transient
	public void setTotalNum(double totalNum) {
		this.totalNum = totalNum;
	}
	
	
	
	@Override
	@Transient
	public String toString() {
		return "TestRecord [nPQ=" + nPQ + ", right=" + right + ", wrong="
				+ wrong + ", total=" + total + ", testId=" + testId
				+ ",subjectId=" + subjectId + ",testName = " + testName + "]";
	}

}
