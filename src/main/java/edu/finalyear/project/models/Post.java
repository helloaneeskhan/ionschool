package edu.finalyear.project.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name = "posts")
@Entity
public class Post implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2808006468021855996L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "subject_id")
	private String subjectId;

	@Column(name = "account_id",insertable = false,updatable = false)
	private String accountId;

	@Column(name = "text")
	private String text;

	@Column(name = "author_type")
	private String authType;

	
	@Column(name = "date_created")
	private Date dateCreated;

	@ManyToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER)
	private Account account;
	
	@OneToMany(fetch = FetchType.EAGER, orphanRemoval = true)
	@JoinColumn(name = "post_id")
	private List<Comment> comments = new ArrayList<Comment>();
	
//	@ManyToOne
//	@JoinColumn(name = "subject_id")
//	private Subject subject;
//	
//	= new Account() {
//
//		private static final long serialVersionUID = 1L;
//
//		@Override
//		public void setSubjectsIds(List<String> subjects) {
//
//
//		}
//
//		@Override
//		public List<String> getSubjectsIds() {
//
//			return null;
//		}
//
//		@Override
//		public String getClassId() {
//
//			return null;
//		}
//	};

	public Post() {
	}

	public Post(String text, String subjectId,Account account) {
		this.subjectId = subjectId;
		this.text = text;
		this.account = account;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	
	
//	public Subject getSubject() {
//		return subject;
//	}
//
//	public void setSubject(Subject subject) {
//		this.subject = subject;
//	}

	
	@Override
	public String toString() {
		return "Post [id=" + id + ", subjectId=" + subjectId + ", accountId="
				+ accountId + ", text=" + text + ", dateCreated=" + dateCreated
				+ "]";
	}

	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

}
