package edu.finalyear.project.models;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import org.codehaus.jackson.annotate.JsonBackReference;
//import org.hibernate.annotations.NamedQuery;
import org.hibernate.validator.constraints.NotEmpty;

@NamedQuery(name = "findSchoolClassByClassId", query = "from SchoolClass where classId like :classId and schoolId = :schoolId")
@Table(name = "class")
@Entity
public class SchoolClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7678269924733128367L;

	@Id
	@Column(name = "class_id")
	@NotEmpty
	private String classId;

	@Column(name = "name")
	@NotEmpty
	private String name;

	@Column(name = "school_id")
	// , insertable = false , updatable = false)
	private String schoolId;

	@OneToMany(orphanRemoval = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "class_id")
	@JsonBackReference
	Set<Student> students = new HashSet<Student>();

	@OneToMany( orphanRemoval = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "class_id")
	@JsonBackReference
	Set<Subject> subjects = new HashSet<Subject>();

	public SchoolClass() {

	}

	public SchoolClass(String classId, String name) {
		super();
		this.name = name;
		this.classId = classId;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Set<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}

	@Override
	public String toString() {
		return "SchoolClass [classId=" + classId + ", name=" + name + "]";
	}

}
