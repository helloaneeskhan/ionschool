package edu.finalyear.project.models;

import java.io.Serializable;
import java.sql.Blob;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

import edu.finalyear.project.schoolprofile.SchoolProfile;

@Entity
@Table(name = "school")
@NamedQuery(name = "findBySchoolId", query = "from School where schoolId = :schoolId")
public class School extends SchoolProfile {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2861090604289612144L;

	@JsonBackReference
	@OneToMany(orphanRemoval = true)
	// ,fetch = FetchType.EAGER)
	@JoinColumn(name = "school_id", updatable = false)
	// ,nullable= false)
	private Set<Teacher> teachers = new HashSet<Teacher>();

	@JsonBackReference
	@OneToMany(orphanRemoval = true)
	// ,fetch = FetchType.EAGER)
	@JoinColumn(name = "school_id", updatable = false)
	private Set<Student> students = new HashSet<Student>();

	@JsonBackReference
	@OneToMany(orphanRemoval = true)// fetch = FetchType.EAGER)
	@JoinColumn(name = "school_id")
	private Set<SchoolClass> schoolClasses = new HashSet<SchoolClass>();

	@JsonBackReference
	@OneToOne(mappedBy = "school", fetch = FetchType.LAZY)
	private Head head;

	@JsonBackReference
	@OneToMany(orphanRemoval = true)
	@JoinColumn(name = "school_id")
	private Set<Account> accounts = new HashSet<Account>();

	public School() {

	}

	public School(String schoolId, String fullName) {
		super(schoolId, fullName);
	}

	public Set<Teacher> getTeachers() {
		return teachers;
	}

	public void setTeachers(Set<Teacher> teachers) {
		this.teachers = teachers;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public Set<SchoolClass> getSchoolClasses() {
		return schoolClasses;
	}

	public void setSchoolClasses(Set<SchoolClass> schoolClasses) {
		this.schoolClasses = schoolClasses;
	}

	public Set<Account> getAccounts() {
		return accounts;
	}

	public Head getHead() {
		return head;
	}

	public void setHead(Head head) {
		this.head = head;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

	@Override
	public String toString() {
		return "School [id=" + schoolId + ", fullName=" + fullName + "]";
	}

}
