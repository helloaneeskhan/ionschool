package edu.finalyear.project.models;

import java.io.Serializable;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "students")
@NamedQuery(name = "findByStudentId", query = "from Student where id = :studentId")
public class Student extends Account implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4532095861168826180L;

	@Column(name = "class_id")
	private String classId;

	@Column(name = "guardian_name")
	private String guardianName;

	@Column(name = "guardian_occupation")
	private String guardianOccupation;

	@Column(name = "guardian_email")
	private String guardianEmail;

	@Column(name = "guardian_mobile")
	private String guardianMobile;

	@Column(name = "guardian_relation")
	private String guardianRelation;
	
	@Column(name = "school_id")
	private String schoolId;

	@OneToOne(mappedBy = "student", orphanRemoval = true, fetch = FetchType.LAZY)
	@JsonBackReference
	private Guardian guardian;
	
	@Transient
	@JsonBackReference
	private List<String> subjectsIds = new ArrayList<String>();

	@OneToMany(orphanRemoval = true,fetch = FetchType.LAZY)
	@JoinColumn(name = "student_id")
	@JsonBackReference
	private List<TestRecord> testRecord = new ArrayList<TestRecord>();
	
	@OneToMany(orphanRemoval = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "student_id")
	@JsonBackReference
	private List<CompositeResult> compositeResult = new ArrayList<CompositeResult>();
	
	//	@Column(name = "school_id")
//	private String schoolId;
//
//	public String getSchoolId() {
//		return schoolId;
//	}
//
//	public void setSchoolId(String schoolId) {
//		this.schoolId = schoolId;
//	}

	public Student() {

	}

	public Student(String id, String firstName, String schoolId) {
		super(id, firstName, schoolId);
	}

	public Student(String id, String firstName, String schoolId, String classId) {
		super(id, firstName, schoolId);
		this.classId = classId;
	}


	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getGuardianName() {
		return guardianName;
	}

	public void setGuardianName(String guardianName) {
		this.guardianName = guardianName;
	}

	public String getGuardianOccupation() {
		return guardianOccupation;
	}

	public void setGuardianOccupation(String guardianOccupation) {
		this.guardianOccupation = guardianOccupation;
	}

	public String getGuardianEmail() {
		return guardianEmail;
	}

	public void setGuardianEmail(String guardianEmail) {
		this.guardianEmail = guardianEmail;
	}

	public String getGuardianMobile() {
		return guardianMobile;
	}

	public void setGuardianMobile(String guardianMobile) {
		this.guardianMobile = guardianMobile;
	}

	public String getGuardianRelation() {
		return guardianRelation;
	}

	public void setGuardianRelation(String guardianRelation) {
		this.guardianRelation = guardianRelation;
	}


	public Guardian getGuardian() {
		return guardian;
	}

	public void setGuardian(Guardian guardian) {
		this.guardian = guardian;
	}

	public List<CompositeResult> getCompositeResult() {
		return compositeResult;
	}

	public void setCompositeResult(List<CompositeResult> compositeResult) {
		this.compositeResult = compositeResult;
	}
	
	

	public String getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
		super.schoolId = schoolId;
	}

	@Override
	@Transient
	public List<String> getSubjectsIds() {
		// TODO Auto-generated method stub
		return this.subjectsIds;
	}

	@Override
	@Transient
	public void setSubjectsIds(List<String> subjectsIds) {
		// TODO Auto-generated method stub
		this.subjectsIds = subjectsIds;
	}

	
	public List<TestRecord> getTestRecord() {
		return testRecord;
	}

	public void setTestRecord(List<TestRecord> testRecord) {
		this.testRecord = testRecord;
	}

	@Override
	public String toString() {
		return "Student [getId()=" + getId() + ", getFirstName()="
				+ getFirstName() + ", getLastName()=" + getLastName()
				+ ", getSchoolId()=" + getSchoolId() + "]" + "classId="
				+ classId;
	}

	@Override
	public String getGuardianStudentId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getClassIds() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setClassIds(List<String> classIds) {
		// TODO Auto-generated method stub
		
	}

}
