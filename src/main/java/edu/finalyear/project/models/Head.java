package edu.finalyear.project.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name = "head")
@DiscriminatorValue("Head")
public class Head extends Account {

	public Head(String id, String password, boolean enabled, String firstName) {
		super(id, password, enabled, firstName);
	}

	@OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)//, fetch = FetchType.LAZY)
	@JoinColumn(name = "school_id", updatable = false, insertable = true)
	private School school;

	@Transient
	@JsonBackReference
	private List<String> classIds = new ArrayList<String>();

	
	public Head() {

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8795167030128347813L;

	@Override
	public List<String> getSubjectsIds() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSubjectsIds(List<String> subjects) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getClassId() {
		// TODO Auto-generated method stub
		return null;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
//		this.schoolId=school.getSchoolId();// schoolId will Automatic save
		this.school = school;
	}

	@Override
	public String toString() {
		return String.format(
				"Head [id=%s, password=%s, enabled=%s, firstName=%s,email=%s]",
				super.id, super.password, super.enabled, super.firstName,super.email);
	}

	@Override
	public String getGuardianStudentId() {//for Guardian Account
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setClassIds(List<String> classIds) {
		this.classIds = classIds;
	}

	@Override
	public List<String> getClassIds() {
		// TODO Auto-generated method stub
		return classIds;
	}

}
