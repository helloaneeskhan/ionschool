package edu.finalyear.project.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "test")
@NamedQuery(query = "from Test test where test.on=:on and test.subjectId = :subjectId", name = "getEnabledTest")
public class Test {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "name")
	private String testName;

	@Column(name = "date_conducted")
	private Date dateConducted;

	@Column(name = "subject_id", insertable = false, updatable = false)
	private String subjectId;
	
	@Column(name = "npq")
	private double nPQ;
	
	@Column(name = "on_off")
	private boolean on;

	@Column(name = "date_id")
	private String dateId;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@JoinColumn(name = "test_id")
	private List<MCQS> mcqs = new ArrayList<MCQS>();

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subject_id")
	private Subject subject;
	

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public Date getDateConducted() {
		return dateConducted;
	}

	public void setDateConducted(Date dateConducted) {
		this.dateConducted = dateConducted;
	}

	public List<MCQS> getMcqs() {
		return mcqs;
	}

	public void setMcqs(List<MCQS> mcqs) {
		this.mcqs = mcqs;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public double getnPQ() {
		return nPQ;
	}

	public void setnPQ(double nPQ) {
		this.nPQ = nPQ;
	}

	public boolean isOn() {
		return on;
	}

	public void setOn(boolean on) {
		this.on = on;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	
	
	public String getDateId() {
		return dateId;
	}

	public void setDateId(String dateId) {
		this.dateId = dateId;
	}

	@Override
	public String toString() {
		return "Test [id=" + id + ", testName=" + testName + ", dateConducted="
				+ dateConducted + ", subjectId=" + subjectId + ", nPQ=" + nPQ
				+ ", on=" + on + ", mcqs=" + mcqs + "]";
	}
	
	
	

}
