package edu.finalyear.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import edu.finalyear.project.dao.SchoolProfileDAO;
import edu.finalyear.project.models.School;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.schoolprofile.About;
import edu.finalyear.project.schoolprofile.Items;
import edu.finalyear.project.schoolprofile.Link;
import edu.finalyear.project.schoolprofile.SchoolProfile;
import edu.finalyear.project.schoolprofile.SchoolProfileUtils;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Teacher;

@Service("SchoolProfileService")
public class SchoolProfileServiceImpl implements SchoolProfileServic{

	@Autowired
	@Qualifier("SchoolProfileDAO")
	private SchoolProfileDAO dao;

	@Autowired
	@Qualifier("SchoolProfileUtils")
	private SchoolProfileUtils spUtils;
	
	@Override
	public SchoolProfile getSchoolProfile(String schoolId) {
		// TODO Auto-generated method stub
		return dao.getSchoolProfile(schoolId);
	}

	@Override
	public void saveOrUpdate(SchoolProfile schoolProfile) {
		// TODO Auto-generated method stub
		dao.saveOrUpdateSchoolProfile(schoolProfile);
	}

	@Override
	public void saveOrUpdateAbout(About about) {
		// TODO Auto-generated method stub
		dao.saveOrUpdateAbout(about);
	}

	@Override
	public void saveOrUpdateLink(Link link) {
		// TODO Auto-generated method stub
		dao.saveOrUpdateLink(link);
	}

	@Override
	public About getAbout(String schoolId, long id) {
		// TODO Auto-generated method stub
		return dao.getAbout(schoolId, id);
	}

	@Override
	public void saveOrUpdateItem(Items item) {
		// TODO Auto-generated method stub
		dao.saveOrUpdateItem(item);
	}

	@Override
	public void initializeNewSchoolProfile(School school) { //coded cascade.all in link of all in schoolprofile
		// TODO Auto-generated method stub
		school.setAbout(spUtils.getAboutTemplate());
		school.setEvent(spUtils.getEventTemplate());
		school.setGallery(spUtils.getGalleryTemplate());
		school.setFaculty(spUtils.getFacultyTemplate());
		saveOrUpdate(school);
		
//		School schoolForItems = getSchoolProfile(schoolId);
	}

}