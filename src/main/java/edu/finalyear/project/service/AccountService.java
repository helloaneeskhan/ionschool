package edu.finalyear.project.service;

//import edu.finalyear.project.dao.Head;
import java.util.List;

import edu.finalyear.project.models.Account;
import edu.finalyear.project.models.Guardian;
import edu.finalyear.project.models.Head;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.models.TestAccount;
import edu.finalyear.project.security.Role;

public interface AccountService {
	public void saveOrUpdateStudent(Student studnet);

	public void saveOrUpdateTeacher(Teacher teacher);
	
	public void saveOrUpdateHead(Head teacher);

	public Account getAccount(String id);

	public Teacher getTeacherAccount(String teacherId, boolean extractSubject);

	public Student getStudentAccount(String studentId, boolean extractGuardian, boolean extractComResult);

	public Account getAccountByIds(String schoolId, String accountId);

	public Head getHeadAccount(String headId);

	public void initializeSchoolProfileAndHead(Head head);

	public Role getRole(String roleId);

	public List<Teacher> getTeacherAccountLike(String term);

	public void saveOrUpdateGuardian(Guardian guardian);

	public List<Account> getAccountLike(String term);
	
	
}
