package edu.finalyear.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.finalyear.project.dao.TeacherDAO;
import edu.finalyear.project.models.Teacher;

@Service("teacherService")
@Transactional
public class TeacherServiceImpl implements TeacherService {

	@Autowired
	@Qualifier("teacherRepository")
	private TeacherDAO teacherDAO;

	@Transactional
	public void createTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		teacherDAO.createTeacher(teacher);
	}

	@Transactional
	public void updateTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		teacherDAO.updateTeacher(teacher);
	}

	@Transactional
	public void deleteTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		teacherDAO.deleteTeacher(teacher);
	}

	@Transactional
	public Teacher getTeacher(String teacherId) {
		// TODO Auto-generated method stub
		return teacherDAO.getTeacher(teacherId);
	}

	@Transactional
	public List<Teacher> getTeacherList() {
		// TODO Auto-generated method stub
		return teacherDAO.getTeacherList();
	}

}
