package edu.finalyear.project.service;

import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import edu.finalyear.project.dao.ClassAndSubjectDAO;
import edu.finalyear.project.models.Comment;
import edu.finalyear.project.models.CompositeResult;
import edu.finalyear.project.models.Post;
//import edu.finalyear.project.dao.SchoolClassDAO;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Subject;
import edu.finalyear.project.models.Test;
import edu.finalyear.project.models.TestRecord;
import edu.finalyear.project.utils.ContextUtils;

@Service("ClassAndSubjectService")
@Transactional(propagation = Propagation.REQUIRED)
public class ClassAndSubjectServiceImpl implements ClassAndSubjectService {

	@Autowired
	@Qualifier("ClassAndSubjectDAO")
	private ClassAndSubjectDAO dao;

	@Autowired
	@Qualifier("generatedValues")
	private edu.finalyear.project.utils.GeneratedValues generatedValues;

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils contextUtils;

	@Transactional
	public SchoolClass getSchoolClass(String classId, boolean extractStudent,boolean extractSubject) {
		// TODO Auto-generated method stub
		if (!classId.contains(generatedValues.getGeneratedClassOrSubjectId("")))
			classId = generatedValues.getGeneratedClassOrSubjectId(classId);

		SchoolClass schoolClass = dao.getSchoolClass(classId,contextUtils.getContextSchoolId());
		
		if(extractStudent)
			Hibernate.initialize(schoolClass.getStudents());
		if(extractSubject)
			Hibernate.initialize(schoolClass.getSubjects());
		
		return schoolClass;
	}

	@Override
	public Set<SchoolClass> getSchoolClassList() {
		// TODO Auto-generated method stub
		return dao.getSchoolClassList();
	}

	@Override
	public Subject getSubject(String subjectId, boolean extractPost,
			boolean extractTest) {
		// TODO Auto-generated method stub

		if (!subjectId.contains("@" + contextUtils.getContextSchoolId()))
			subjectId = subjectId + "@" + contextUtils.getContextSchoolId();

		Subject subject = dao.getSubject(subjectId);
		if (extractPost)
			Hibernate.initialize(subject.getPosts());

		if (extractTest)
			Hibernate.initialize(subject.getTest());

		// if(extractTestRecord)
		// Hibernate.initialize(subject.getTestRecords());

		return subject;// dao.getSubject(subjectId);
	}

	@Transactional
	public void initializeSubjectPost(Subject subjet) {
		// TODO Auto-generated method stub
		Hibernate.initialize(subjet.getPosts());
	}

	@Override
	public Set<Subject> getSubjectsListByClassId(String classId) {
		// TODO Auto-generated method stub
		return dao.getSubjectsListByClassId(classId);
	}

	@Override
	public void newPost(Post post) {
		// TODO Auto-generated method stub
		// if((post.getAccount()!=null &&
		// !post.getAccount().getId().contains(generatedValues.getGeneratedAccountId("")))){
		// post.getAccount().setId(generatedValues.getGeneratedAccountId(post.getAccount().getId()));
		// }

		// if(post.getSubjectId()!=null &&
		// !post.getSubjectId().contains(generatedValues.getGeneratedClassOrSubjectId(""))){
		// post.setSubjectId(generatedValues.getGeneratedClassOrSubjectId(post.getSubjectId()));
		// }

		dao.newPost(post);
	}

	@Override
	public void saveOrUpdateClass(SchoolClass schoolClass) {
		// TODO Auto-generated method stub
		String cId = schoolClass.getClassId();
		if(!cId.contains("@"+contextUtils.getContextSchoolId())){
			cId = cId+"@"+contextUtils.getContextSchoolId();
			schoolClass.setClassId(cId);
		}
		schoolClass.setClassId(generatedValues
				.getGeneratedClassOrSubjectId(schoolClass.getClassId()));
		dao.saveOrUpdateClass(schoolClass);
	}

	@Override
	public void saveOrUpdateSubject(Subject subject) {
		// TODO Auto-generated method stub

		if (!subject.getSubjectId().contains(
				"@" + contextUtils.getContextSchoolId()))
			subject.setSubjectId(generatedValues
					.getGeneratedClassOrSubjectId(subject.getSubjectId()));

		if ((subject.getTeacher() != null || subject.getTeacherId() != "")) {
			if (!subject.getTeacherId().contains(
					"@" + contextUtils.getContextSchoolId()))

				subject.setTeacherId(generatedValues
						.getGeneratedAccountId(subject.getTeacherId()));
		}

		if (subject.getClassId() != null || subject.getClassId() != "") {
			if (!subject.getClassId().contains(
					"@" + contextUtils.getContextSchoolId()))
				subject.setClassId(generatedValues
						.getGeneratedClassOrSubjectId(subject.getClassId()));
		}
		dao.saveOrUpdateClass(subject);

	}

	@Override
	public List<SchoolClass> getSchoolClassLike(String classId) {
		// TODO Auto-generated method stub
		return dao.getSchoolClassLike(classId);
	}

	@Override
	public void saveOrUpdate(Test test) {
		// TODO Auto-generated method stub
		dao.saveOrUpdate(test);
	}

	@Override
	public Test getTest(long id) {
		// TODO Auto-generated method stub
		return dao.getTest(id);
	}

	@Override
	public Test getEnableTest(boolean enable, String subjectId) {
		// TODO Auto-generated method stub
		if (!subjectId.contains("@" + contextUtils.getContextSchoolId()))
			subjectId = subjectId + "@" + contextUtils.getContextSchoolId();
		Test test = dao.getEnableTest(enable, subjectId);
		System.out.println("CSService Test = " + test);
		return dao.getEnableTest(enable, subjectId);
	}

	@Override
	public TestRecord getTestRecordOnTestId(String studentId,String dateId, long testId) {
		// TODO Auto-generated method stub
		return dao.getTestRecordOnTestId(studentId, dateId,testId);
	}

	@Override
	public void saveOrUpdateTestRecord(TestRecord testRecord) {
		// TODO Auto-generated method stub
		dao.saveOrUpdateClass(testRecord);
	}

	@Override
	public void saveOrUpdateComment(Comment comment) {
		// TODO Auto-generated method stub
		dao.saveOrUpdateClass(comment);
	}

	@Override
	public List<TestRecord> getTestRecords(String subjectId, long testId) {
		// TODO Auto-generated method stub
		return dao.getTestRecords(subjectId, testId);
	}

	@Override
	public List<TestRecord> getTestRecordsOnStuAndSubId(String subjectId,
			String studentId) {
		// TODO Auto-generated method stub
		return dao.getTestRecordsOnStuAndSubId(subjectId, studentId);
	}

	@Override
	public void deleteObject(Object object) {
		// TODO Auto-generated method stub
		if (object != null)
			dao.deleteObject(object);
	}

	@Override
	public CompositeResult getCompositeResult(String subjectId, String studentId) {
		// TODO Auto-generated method stub
		return dao.getCompositeResult(subjectId, studentId);
	}

	@Override
	public void saveOrUpdateCompositeResult(CompositeResult result) {
		// TODO Auto-generated method stub
		dao.saveOrUpdateClass(result);
	}

	@Override
	public List<SchoolClass> getSchoolClassVIASchoolId(String schoolId) {
		// TODO Auto-generated method stub
		return dao.getSchoolClassVIASchoolId(schoolId);
	}

	@Override
	public List<Subject> getAllSubjectsVIASchoolId(String schoolId) {
		// TODO Auto-generated method stub
		return dao.getAllSubjectsVIASchoolId(schoolId);
	}

}
