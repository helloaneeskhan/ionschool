package edu.finalyear.project.service;

import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import edu.finalyear.project.dao.SchoolDAO;
import edu.finalyear.project.dao.SchoolProfileDAO;
import edu.finalyear.project.models.School;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.schoolprofile.About;
import edu.finalyear.project.schoolprofile.AboutItem;
import edu.finalyear.project.schoolprofile.Items;
import edu.finalyear.project.schoolprofile.Link;
import edu.finalyear.project.schoolprofile.SchoolProfile;
import edu.finalyear.project.schoolprofile.SchoolProfileUtils;
import edu.finalyear.project.utils.ContextUtils;
import static edu.finalyear.project.utils.ContextUtils.*;

@Service("SchoolService")
@Transactional(propagation = Propagation.REQUIRED)
public class SchoolServiceImpl implements SchoolService {

	@Autowired
	@Qualifier("SchoolDAO")
	private SchoolDAO dao;

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils contextUtils;

	@Autowired
	@Qualifier("SchoolProfileUtils")
	private SchoolProfileUtils spUtils;

	@Transactional
	public void saveOrUpdateSchool(School school) {
		// TODO Auto-generated method stub
		dao.saveOrUpdateSchool(school);
	}

	@Transactional
	public School getSchool(String id) {
		// TODO Auto-generated method stub
		return dao.getSchool(id);
	}

	@Transactional
	public void addStudent(Student student) {
		// TODO Auto-generated method stu
		School school = dao.getSchool(contextUtils.getContextSchool()
				.getSchoolId());
		school.getStudents().add(student);
		saveOrUpdateSchool(school);
	}

	@Transactional
	public void addTeacher(Teacher teacher) {
		// TODO Auto-generated method stub

		School school = dao.getSchool(contextUtils.getContextSchoolId());// contextUtils.getContextSchool().getSchoolId());
		System.out.println("Student=" + school.getStudents());
		System.out.println("Teachers=" + school.getTeachers());
		school.getTeachers().add(teacher);
		saveOrUpdateSchool(school);

	}

	@Transactional
	public void addClass(SchoolClass schoolClass) {
		// TODO Auto-generated method stub

		School school = dao.getSchool(contextUtils.getContextSchool()
				.getSchoolId());
		school.getSchoolClasses().add(schoolClass);
		saveOrUpdateSchool(school);
	}

	// ////////////////School Profile Side///////////////////////
	@Transactional
	public SchoolProfile getSchoolProfile(String schoolId) {
		// TODO Auto-generated method stub
		return dao.getSchoolProfile(schoolId);
	}

	@Transactional
	public void saveOrUpdate(SchoolProfile schoolProfile) {
		// TODO Auto-generated method stub
		dao.saveOrUpdateSchoolProfile(schoolProfile);
	}

	@Transactional
	public void saveOrUpdateAbout(About about) {
		// TODO Auto-generated method stub
		dao.saveOrUpdateAbout(about);
	}

	@Transactional
	public void saveOrUpdateLink(Link link) {
		// TODO Auto-generated method stub
		dao.saveOrUpdateLink(link);
	}

	@Transactional
	public About getAbout(String schoolId, long id) {
		// TODO Auto-generated method stub
		return dao.getAbout(schoolId, id);
	}

	@Transactional
	public void saveOrUpdateItem(Items item) {
		// TODO Auto-generated method stub
		dao.saveOrUpdateItem(item);
	}

	@Override
	public void initializeNewSchool(School school) { // coded cascade.all
														// in link of all in
														// schoolprofile
		// TODO Auto-generated method stub
		school.setAbout(spUtils.getAboutTemplate());
		school.setEvent(spUtils.getEventTemplate());
		school.setGallery(spUtils.getGalleryTemplate());
		school.setFaculty(spUtils.getFacultyTemplate());
		saveOrUpdateSchool(school);

		School schoolForItems = getSchool(school.getSchoolId());

		Hibernate.initialize(schoolForItems.getAbout());
		Hibernate.initialize(schoolForItems.getEvent());
		Hibernate.initialize(schoolForItems.getFaculty());
		Hibernate.initialize(schoolForItems.getGallery());

		System.out.println("SchoolService: about=" + schoolForItems.getAbout()
				+ ",event=" + schoolForItems.getEvent());
		for (int i = 0; i < 4; i++) {
			saveOrUpdateItem(spUtils.getAboutItemTemplate(schoolForItems
					.getAbout().getId()));
			saveOrUpdateItem(spUtils.getEventItemTemplate(schoolForItems
					.getEvent().getId()));
			saveOrUpdateItem(spUtils.getFacultyItemTemplate(schoolForItems
					.getFaculty().getId()));
		}

		for (int i = 0; i < 6; i++)
			saveOrUpdateItem(spUtils.getGalleryItemTemplate(schoolForItems
					.getGallery().getId()));

	}

	@Transactional
	public List<SchoolProfile> getSchoolProfileLike(String arg) {
		// TODO Auto-generated method stub
		return dao.getSchoolProfileLike(arg);
	}

	@Transactional
	public Set<SchoolClass> getAllClass() {
		// TODO Auto-generated method stub
		School school = dao.getSchool(contextUtils.getContextSchoolId());
		Hibernate.initialize(school.getSchoolClasses());
		System.out.println("SSImpl after initialization");
		return school.getSchoolClasses();
	}

}