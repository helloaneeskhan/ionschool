package edu.finalyear.project.service;

import java.util.List;

import edu.finalyear.project.models.Student;

public interface StudentService {
	public void createStudent(Student student);
	public void updateStudent(Student student);
	public void deleteStudent(Student student);
	public Student getStudent(String id);
	public List<Student> getStudentList();

}
