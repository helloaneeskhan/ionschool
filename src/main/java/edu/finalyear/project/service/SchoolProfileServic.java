package edu.finalyear.project.service;

import edu.finalyear.project.models.School;
import edu.finalyear.project.schoolprofile.About;
import edu.finalyear.project.schoolprofile.Items;
import edu.finalyear.project.schoolprofile.Link;
import edu.finalyear.project.schoolprofile.SchoolProfile;

public interface SchoolProfileServic {
		public SchoolProfile getSchoolProfile(String schoolId);
		public void saveOrUpdate(SchoolProfile schoolProfile);
		public void saveOrUpdateAbout(About about);
		public void saveOrUpdateLink(Link link);
		public void saveOrUpdateItem(Items item);
		public About getAbout(String schoolId,long id);
		public void initializeNewSchoolProfile(School school);
	
		//		public void update postedSchoolProfile(SchoolProfile schoolProfile);
}

