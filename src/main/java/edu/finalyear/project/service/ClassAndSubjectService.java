package edu.finalyear.project.service;

import java.util.List;
import java.util.Set;

import edu.finalyear.project.models.Comment;
import edu.finalyear.project.models.CompositeResult;
import edu.finalyear.project.models.Post;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Subject;
import edu.finalyear.project.models.Test;
import edu.finalyear.project.models.TestRecord;

public interface ClassAndSubjectService {
	public Set<SchoolClass> getSchoolClassList();

	public SchoolClass getSchoolClass(String classId, boolean extractStudent, boolean extractSubject);

	public List<SchoolClass> getSchoolClassLike(String classId);

	public Subject getSubject(String subjectId, boolean extractPost,
			boolean extractTest);

	public Set<Subject> getSubjectsListByClassId(String classId);

	public void newPost(Post post);

	public void saveOrUpdateClass(SchoolClass schoolClass);

	public void saveOrUpdateSubject(Subject subject);

	public void initializeSubjectPost(Subject subjet);

	public void saveOrUpdate(Test test);

	public Test getTest(long id);

	public Test getEnableTest(boolean enable, String subjectId);

	public TestRecord getTestRecordOnTestId(String studentId,String dateId, long testId);

	public void saveOrUpdateTestRecord(TestRecord testRecord);

	public void saveOrUpdateComment(Comment comment);

	public List<TestRecord> getTestRecords(String subjectId, long testId);

	public List<TestRecord> getTestRecordsOnStuAndSubId(String subjectId,
			String studentId);
	public void deleteObject(Object object);
	
	public CompositeResult getCompositeResult(String subjectId, String studentId);
	
	public void saveOrUpdateCompositeResult(CompositeResult result);
	
	public List<SchoolClass> getSchoolClassVIASchoolId(String schoolId);

	public List<Subject> getAllSubjectsVIASchoolId(String schoolId);
}
