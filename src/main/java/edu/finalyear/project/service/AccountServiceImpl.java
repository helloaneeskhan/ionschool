package edu.finalyear.project.service;

import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.finalyear.project.dao.AccountDAO;
import edu.finalyear.project.models.Account;
import edu.finalyear.project.models.Guardian;
import edu.finalyear.project.models.Head;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.models.TestAccount;
import edu.finalyear.project.schoolprofile.SchoolProfile;
import edu.finalyear.project.security.Role;
import edu.finalyear.project.utils.ContextUtils;

@Service("AccountService")
@Transactional
public class AccountServiceImpl implements AccountService {

	@Autowired
	@Qualifier("AccountDAO")
	private AccountDAO dao;

	@Autowired
	@Qualifier("generatedValues")
	private edu.finalyear.project.utils.GeneratedValues generatedValues;

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils contextUtils;

	@Override
	public void saveOrUpdateStudent(Student student) {
		// TODO Auto-generated method stub

		String gApix = "guardian_";

		if (!student.getId().contains("@" + contextUtils.getContextSchoolId()))
			student.setId(generatedValues.getGeneratedAccountId(student.getId()));

		if (!student.getClassId().contains(
				"@" + contextUtils.getContextSchoolId()))
			student.setClassId(generatedValues
					.getGeneratedClassOrSubjectId(student.getClassId()));

		if ((student.getGuardian().getId() == null || student.getGuardian()
				.getId().equals("")))
			student.getGuardian().setId(gApix.concat(student.getId()));

		if (!student.getGuardian().getId().contains((gApix)))
			student.getGuardian().setId(
					gApix.concat(student.getGuardian().getId()));

		
		student.setSchoolId(contextUtils.getContextSchoolId());
		student.getGuardian().setSchoolId(student.getSchoolId());
		
		student.setEnabled(true);

		student.getRoles().add(dao.getRole("student"));

		student.getGuardian().getRoles().add(dao.getRole("guardian"));

		student.getGuardian().setEnabled(true);
		student.getGuardian().setStudent(student);

		dao.saveOrUpdate(student);
	}

	@Override
	public void saveOrUpdateTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		if (!teacher.getId().contains("@" + contextUtils.getContextSchoolId()))
			teacher.setId(generatedValues.getGeneratedAccountId(teacher.getId()));
		
		teacher.getRoles().add(dao.getRole("teacher"));
		teacher.setEnabled(true);
		teacher.setSchoolId(contextUtils.getContextSchoolId());
		dao.saveOrUpdate(teacher);
	}

	@Override
	public void saveOrUpdateHead(Head account) {
		// TODO Auto-generated method stub

		dao.saveOrUpdate(account);
	}

	@Override
	public Account getAccount(String id) {
		// TODO Auto-generated method stub
		return dao.getAccount(id);
	}

	@Override
	public Teacher getTeacherAccount(String teacherId,boolean extractSubject) {
		// TODO Auto-generated method stub
		if(!teacherId.contains("@"+contextUtils.getContextSchoolId()))
			teacherId = teacherId = "@"+contextUtils.getContextSchoolId();
		
		Teacher teacher = dao.getTeacherAccount(teacherId);
		
		if(extractSubject)
			Hibernate.initialize(teacher.getSubjects());
		System.out.println("*********AS/ getTeacherAccount");
		return teacher;
	}

	@Override
	public Student getStudentAccount(String studentId, boolean extractGuardian, boolean extractComResult) {
		// TODO Auto-generated method stub
		Student student = dao.getStudentAccount(studentId);
		if(extractGuardian)
			Hibernate.initialize(student.getGuardian());
		if(extractComResult)
			Hibernate.initialize(student.getCompositeResult());
		
		return student;
	}

	@Override
	public Account getAccountByIds(String schoolId, String accountId) {
		// TODO Auto-generated method stub
		return dao.getAccountByIds(schoolId, accountId);
	}

	@Override
	public Head getHeadAccount(String headId) {
		// TODO Auto-generated method stub
		return dao.getHeadAccount(headId);
	}

	@Override
	public void initializeSchoolProfileAndHead(Head head) {
		// TODO Auto-generated method stub
		SchoolProfile headSchoolProfile = head.getSchool();
	}

	@Override
	public Role getRole(String roleId) {
		// TODO Auto-generated method stub
		return dao.getRole(roleId);
	}

	@Override
	public List<Teacher> getTeacherAccountLike(String term) {
		// TODO Auto-generated method stub
		return dao.getTeacherAccountLike(term);
	}

	@Override
	public List<Account> getAccountLike(String term) {
		// TODO Auto-generated method stub
		return dao.getAccountLike(term);
	}

	@Override
	public void saveOrUpdateGuardian(Guardian guardian) {
		// TODO Auto-generated method stub
		
		dao.saveOrUpdateObject(guardian);
	}

}
