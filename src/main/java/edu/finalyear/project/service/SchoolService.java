package edu.finalyear.project.service;

import java.util.List;
import java.util.Set;

import edu.finalyear.project.models.School;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.schoolprofile.About;
import edu.finalyear.project.schoolprofile.Items;
import edu.finalyear.project.schoolprofile.Link;
import edu.finalyear.project.schoolprofile.SchoolProfile;

public interface SchoolService {

	public void saveOrUpdateSchool(School school);
	public School getSchool(String id);
	public void addStudent(Student student);
	public void addTeacher(Teacher teacher);
	public void addClass(SchoolClass schoolClass);
	
	public SchoolProfile getSchoolProfile(String schoolId);
	public void saveOrUpdate(SchoolProfile schoolProfile);
	public void saveOrUpdateAbout(About about);
	public void saveOrUpdateLink(Link link);
	public void saveOrUpdateItem(Items item);
	public About getAbout(String schoolId,long id);
	public void initializeNewSchool(School school);

	public List<SchoolProfile> getSchoolProfileLike(String arg);
	
	public Set<SchoolClass> getAllClass();
	
	//	public List<School> getAllSchool();

}
