package edu.finalyear.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.finalyear.project.dao.StudentDAO;
import edu.finalyear.project.models.Student;

@Service("studentService")
@Transactional
public class StudentServiceImpl implements StudentService{

	@Autowired
	@Qualifier("studentRepository")
	private StudentDAO studentDAO;
	
	@Transactional
	public void createStudent(Student student) {
		// TODO Auto-generated method stub
		studentDAO.createStudent(student);
	}

	@Transactional
	public void updateStudent(Student student) {
		// TODO Auto-generated method stub
		studentDAO.updateStudent(student);
	}

	@Transactional
	public void deleteStudent(Student student) {
		// TODO Auto-generated method stub
		studentDAO.deleteStudent(student);
	}

	@Transactional
	public Student getStudent(String id) {
		// TODO Auto-generated method stub
		return studentDAO.getStudent(id);
	}

	@Transactional
	public List<Student> getStudentList() {
		// TODO Auto-generated method stub
		return studentDAO.getStudentList();
	}

}
