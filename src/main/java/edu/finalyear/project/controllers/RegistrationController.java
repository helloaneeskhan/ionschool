package edu.finalyear.project.controllers;

import java.io.IOException;
import java.sql.Blob;

import javax.persistence.GeneratedValue;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import javax.xml.ws.BindingType;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Guardian;
import edu.finalyear.project.models.Subject;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.service.AccountService;
import edu.finalyear.project.service.ClassAndSubjectService;
import edu.finalyear.project.service.SchoolService;
import edu.finalyear.project.service.StudentService;
import edu.finalyear.project.utils.ContextUtils;
import edu.finalyear.project.utils.GeneratedValues;
import static edu.finalyear.project.utils.ContextUtils.*;

@Controller
@PreAuthorize("hasAnyRole('ROLE_HEAD')")
@RequestMapping(value = "/{schoolid}/registration")
public class RegistrationController {

	@Autowired
	private StudentService studentService;

	@Autowired
	private SchoolService schoolService;

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils contextUtils;

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;

	@Autowired
	@Qualifier("ClassAndSubjectService")
	private ClassAndSubjectService csService;

	@Autowired
	@Qualifier("generatedValues")
	private GeneratedValues generatedValues;

	
	@PreAuthorize("hasAnyRole('ROLE_HEAD')")
	@RequestMapping(value = "newactivity/{activity}")
	public String newActivity(@PathVariable String activity, Model model) {

		System.out.println("acitivity = :" + activity + "test :"
				+ activity.equals("newsubject"));
		if (activity.equals("newteacher")) {
			model.addAttribute("activity", "create_teacher");
			model.addAttribute("newTeacher", new Teacher());
		} else if (activity.equals("newclass")) {
			model.addAttribute("activity", "create_class");
			model.addAttribute("newSchoolClass", new SchoolClass());
		} else if (activity.equals("newsubject")) {
			model.addAttribute("activity", "create_subject");
			model.addAttribute("newSubject", new Subject());
		} else
			model.addAttribute("activity", "create_student");
		model.addAttribute("newStudent", new Student());
		// model.addAttribute("newGuardian", new Guardian());
		return "new-activity";
	}
	

	@PreAuthorize("hasAnyRole('ROLE_HEAD')")
	@RequestMapping(value = "/studentRegistration", method = RequestMethod.POST)
	public String studentRegistration(
			@Valid @ModelAttribute("newStudent") Student student,
			BindingResult result, @RequestParam("propic") MultipartFile file,
			Model model) {

		System.out.println("insid studentRegistration" + student);

		if (result.hasErrors()) {
			model.addAttribute("activity", "create_student");
			return "new-activity";
		}

		try {
			student.setProfilePhoto(file.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("insid studentRegistration/level_2");
		// student.setGuardian(new Guardian(student.getStudentId(),
		// null,null));
		student.getRoles().add(accountService.getRole("student"));

		student.getGuardian().setEnabled(true);
		student.getGuardian().getRoles()
				.add(accountService.getRole("guardian"));
		student.getGuardian().setId("guardian_".concat(student.getId()));
		student.setSchoolId(contextUtils.getContextSchoolId());
		student.setEnabled(true);

		System.out
				.println("RegestrationController/studentRegistration: schoolId="
						+ contextUtils.getContextSchoolId());

		accountService.saveOrUpdateStudent(student);
		accountService.saveOrUpdateGuardian(student.getGuardian());
		System.out.println("insid studentRegistration/level_3");
		return "redirect:/" + contextUtils.getContextSchoolId() + "/";
	}

	@PreAuthorize("hasAnyRole('ROLE_HEAD')")
	@RequestMapping(value = "/teacherRegistration", method = RequestMethod.POST)
	public String teacherRegistration(
			@Valid @ModelAttribute("newTeacher") Teacher teacher,
			BindingResult binding,
			@RequestParam("propic") MultipartFile propic, Model model) {
		System.out.println("====>Teacher registration" + teacher);
		if (binding.hasErrors()) {
			model.addAttribute("activity", "create_teacher");
			return "new-activity";
		}

		try {
			teacher.setProfilePhoto(propic.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("====>inside /teacherRegistration  schoolId"
				+ teacher.getSchoolId());
		accountService.saveOrUpdateTeacher(teacher);

		return "redirect:/" + contextUtils.getContextSchoolId() + "/";
	}

	@PreAuthorize("hasAnyRole('ROLE_HEAD')")
	@RequestMapping(value = "/classRegistration", method = RequestMethod.POST)
	public String classRegistration(
			@Valid @ModelAttribute("newSchoolClass") SchoolClass schoolClass,
			BindingResult binding, Model model) {
		System.out.println("====>classSchool registration" + schoolClass);
		if (binding.hasErrors()) {
			model.addAttribute("activity", "create_class");
			return "new-activity";
		}

		schoolClass.setSchoolId(contextUtils.getContextSchoolId());

		csService.saveOrUpdateClass(schoolClass);

		return "redirect:/" + contextUtils.getContextSchoolId() + "/";
	}

	@PreAuthorize("hasAnyRole('ROLE_HEAD')")
	@RequestMapping(value = "/subjectRegistration", method = RequestMethod.POST)
	public String subjectRegistration(
			@Valid @ModelAttribute("newSubject") Subject subject,
			BindingResult binding, Model model) {
		System.out
				.println("RegistrationController/subjectRegistration: registration"
						+ subject);
		if (binding.hasErrors()) {
			model.addAttribute("activity", "create_class");
			return "new-activity";
		}

		Teacher teacher = accountService.getTeacherAccount(
				subject.getTeacherId(), false);
		subject.setTeacher(teacher);
		// teacher.getSubjects().add(subject);
		csService.saveOrUpdateSubject(subject);

		return "redirect:/" + contextUtils.getContextSchoolId() + "/";
	}
}
