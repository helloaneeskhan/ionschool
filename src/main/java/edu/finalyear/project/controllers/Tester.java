package edu.finalyear.project.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import edu.finalyear.project.models.Head;
//import edu.finalyear.project.models.Item;
import edu.finalyear.project.models.Post;
import edu.finalyear.project.models.School;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.schoolprofile.About;
import edu.finalyear.project.schoolprofile.AboutItem;
import edu.finalyear.project.schoolprofile.Event;
import edu.finalyear.project.schoolprofile.EventItem;
import edu.finalyear.project.schoolprofile.Faculty;
import edu.finalyear.project.schoolprofile.FacultyItem;
import edu.finalyear.project.schoolprofile.Gallery;
import edu.finalyear.project.schoolprofile.GalleryItem;
import edu.finalyear.project.schoolprofile.Link;
import edu.finalyear.project.schoolprofile.SchoolProfile;
import edu.finalyear.project.service.AccountService;
import edu.finalyear.project.service.ClassAndSubjectService;
//import edu.finalyear.project.service.SchoolProfileService;
import edu.finalyear.project.service.SchoolService;

@Controller
@RequestMapping(value = "/tester")
public class Tester {

	@Value("#{default.aboutDescription}")
	private String aboutDescription;

	@Value("#{default.aboutDescription}")
	private static String aboutDescription2;

	@Autowired
	@Qualifier("ClassAndSubjectService")
	private ClassAndSubjectService csService;

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;

	// @Autowired
	// @Qualifier("SchoolProfileService")
	// private SchoolProfileService spService;

	@Autowired
	@Qualifier("SchoolService")
	private SchoolService schoolService;

	@RequestMapping(value = "/{n}")
	public String defaultTester(Model model, @PathVariable int n) {
		if (n == 1) {// if one then new would save
			// ///////////////Createing new School with TEST id and setting//
			// Links/////////////////////
			SchoolProfile schoolPro = new SchoolProfile("TEST",
					"Test Foundation School");
			About about = new About("discription", "wordAboutUs");
			Gallery gallery = new Gallery("gallery discription");
			Faculty faculty = new Faculty("Millat Faculty");

			about.getItems().add(
					new AboutItem("test-heading", "subHeading", "content"));
			about.getItems().add(
					new AboutItem("test-heading", "subHeading", "content"));
			about.getItems().add(
					new AboutItem("test-heading", "subHeading", "content"));

			gallery.getItems().add(new GalleryItem("test-lable 1"));
			gallery.getItems().add(new GalleryItem("test-lable 2"));

			faculty.getItems()
					.add(new FacultyItem("test-faculty_member", "test-nothing",
							232));
			faculty.getItems().add(
					new FacultyItem("test-name", "test-experties", 32));

			// about.setSchoolProfile(schoolPro);
			gallery.setSchoolProfile(schoolPro);
			faculty.setSchoolProfile(schoolPro);

			// schoolPro.setAbout(about);
			schoolPro.setGallery(gallery);
			schoolPro.setFaculty(faculty);

			schoolService.saveOrUpdate(schoolPro);

		}

		if (n == 2) {
			SchoolProfile schoolProfile = schoolService
					.getSchoolProfile("TEST");

			Event event = new Event("test-event");
			event.getItems()
					.add(new EventItem("test event item", "sdfa",
							"sub Heading", 23));
			event.getItems()
					.add(new EventItem("test event item", "sub Heading",
							"asdf", 23));

			event.setSchoolProfile(schoolProfile);
			schoolProfile.setEvent(event);
			schoolService.saveOrUpdate(schoolProfile);
		}

		// //////////////////////////////////////////////////////////////////////////////////////////////

		System.out.println("Tester:/defaultTester level 1");

		// for (AboutItem item : spService.getSchoolProfile("TEST").getAbout()
		// .getItems())
		// System.out.println("Tester:/defaultTester 2 AbboutItem=" + item);

		for (GalleryItem item : schoolService.getSchoolProfile("TEST")
				.getGallery().getItems())
			System.out.println("Tester:/defaultTester 3 GalleryItem=" + item);

		for (FacultyItem item : schoolService.getSchoolProfile("TEST")
				.getFaculty().getItems())
			System.out.println("Tester:/defaultTester 4 FacultyItem=" + item);

		for (EventItem item : schoolService.getSchoolProfile("TEST").getEvent()
				.getItems())
			System.out.println("Tester:/defaultTester 5 EventItem=" + item);

		System.out.println("Tester:/defaultTester 6");

		return "test";

	}

	@RequestMapping(value = "posttest")
	public void getPost(@ModelAttribute("schoolPro") SchoolProfile sProfile3) {

		// System.out.println("Tester:/defaultTester About="
		// + sProfile3.getAbout());

		// L<AboutItem> items = sProfile3.getAbout().getItems();
		// for (AboutItem item : items)
		// System.out.println("Tester:/defaultTester item=" + item);

		// model.addAttribute("about",sProfile3.getAbout());
	}

	@RequestMapping(value = "test-about")
	public void checkAbout(Model model) {

		System.out.println("Tester:/checkAbout level 1");

		// About about = spService.getAbout("", 1);
		// about.setWordAboutUs("update description");
		// System.out.println("Tester:/checkAbout level 2 about" + about);
		AboutItem item = new AboutItem("testing heading separately persist",
				"subHeading", "description");
		item.setId(1);

		// item.setDescription("updatedd des my dex");
		// item.setId();
		// about.getItems().add(item);
		// spService.saveOrUpdateAbout(about);
		schoolService.saveOrUpdateItem(item);
		// spService.saveOrUpdateLink(new Gallery("discription test-about"));

	}

	@RequestMapping(value = "test-head/{headId}/{schoolId}")
	public void testHead(Model model, @PathVariable("headId") String headId,
			@PathVariable("schoolId") String schoolId) {
		// accountService.saveOrUpdate(new Head(headId, "password", true,
		// "firstName"));
		// System.out.printf("\nHead=%s",accountService.getHeadAccount(headId));
		System.out.println("Tester/testHead:");
		// Head head = new Head(headId, "password", true,"firstName");
		// head.setSchool(new School(schoolId, "fullName testing"));
		// accountService.saveOrUpdate(head);
		schoolService.initializeNewSchool(new School(schoolId,
				"fullName testing"));

	}

	@RequestMapping(value = "index")
	public String getIndex(Model model) {
		// System.out.println("Tester/Tester: aboutDescription="+aboutDescription+",aboutDescription2="+aboutDescription2);

		System.out.println("Tester/index: isAuthenticated="
				+ SecurityContextHolder.getContext().getAuthentication()
						.isAuthenticated());
		return "iownschool/index";
	}

	@RequestMapping(value = "test-account")
	public void checkAccount(Model model) {
		System.out.println("Tester:/checkAccount level 1");
		Student student = accountService.getStudentAccount("student_test1",false,false);
		System.out.println("Student = " + student);
		student.setAddress("Updating in checkAccount");
		accountService.saveOrUpdateStudent(student);
	}

	// @Req
	
	
	@RequestMapping(value = "update-school-profile", method = RequestMethod.POST)
	public String updateSchoolProfile(
			@ModelAttribute("schoolProfile") SchoolProfile schoolProfile,
			@RequestParam("cover1") MultipartFile cover1){
//			@RequestParam("cover2") MultipartFile cover2,
//			@RequestParam("cover3") MultipartFile cover3) {
		
		System.out.println("StaticController/updateSchoolProfile: level 1");
		schoolService.saveOrUpdate(schoolProfile);
		
		try {
			schoolProfile.setCover1(cover1.getBytes());
//			schoolProfile.setCover2(cover2.getBytes());
//			schoolProfile.setCover3(cover3.getBytes());
			System.out.println("StaticController/updateSchoolProfile: cover1.size"+cover1.getSize());//+"\t cover2.size="+cover2.getSize());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		// spService.saveOrUpdateAbout(schoolProfile.getAbout());

		return "redirect:/" + schoolProfile.getSchoolId() + "/";
	}
	

}
