package edu.finalyear.project.controllers;

import static edu.finalyear.project.security.SecurityUtils.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.finalyear.project.models.Account;
import edu.finalyear.project.models.Comment;
import edu.finalyear.project.models.CompositeResult;
import edu.finalyear.project.models.MCQS;
import edu.finalyear.project.models.Post;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Subject;
import edu.finalyear.project.models.Test;
import edu.finalyear.project.models.TestRecord;
import edu.finalyear.project.security.Role;
import edu.finalyear.project.security.SecurityUtils;
import edu.finalyear.project.service.AccountService;
import edu.finalyear.project.service.ClassAndSubjectService;
//import edu.finalyear.project.service.SchoolClassService;
import edu.finalyear.project.utils.ContextUtils;

@Controller
@PreAuthorize("hasAnyRole('ROLE_STUDENT')")
@RequestMapping(value = "/{schoolId}/my-class")
public class ClassController {

	@Autowired
	@Qualifier("ClassAndSubjectService")
	private ClassAndSubjectService csService;

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils context;

	// return all subjects for /schoolid/my-class
	@RequestMapping(value = "")
	@PreAuthorize("hasAnyRole('ROLE_STUDENT')")
	public String getClassPage(Model model) {

		SchoolClass myClass = csService.getSchoolClass(getLogedInAccount()
				.getClassId(),true,true);
		Set<Student> students = myClass.getStudents();
		Set<Subject> subjects = myClass.getSubjects();

		model.addAttribute("subjects", subjects);
		model.addAttribute("students", students);
		model.addAttribute("schoolClass", myClass);
		System.out.println("ClassController: /{} Students=" + students);
		System.out.println("ClassController: /{} Teacher="
				+ subjects.iterator().next().getTeacher());

		return "classPage";
	}

	@RequestMapping(value = "/all-subjects")
	@PreAuthorize("hasAnyRole('ROLE_STUDENT')")
	public String getAllSubject(Model model) {
		System.out.println("ClassController:/getAllSubject");

		SchoolClass myClass = csService.getSchoolClass(getLogedInAccount()
				.getClassId(),false,true);
		Set<Student> students = myClass.getStudents();
		Set<Subject> subjects = myClass.getSubjects();

		model.addAttribute("subjects", subjects);
		model.addAttribute("schoolClass", myClass);
		System.out.println("ClassController:/getAllSubject");
		return "classPage";

	}

	@PreAuthorize("hasAnyRole('ROLE_STUDENT')")
	@RequestMapping(value = "/class-mates")
	public String getClassMates(Model model) {
		System.out.println("ClassController:/getClassMates");

		SchoolClass myClass = csService.getSchoolClass(getLogedInAccount()
				.getClassId(),true,false);

		Set<Student> students = myClass.getStudents();

		model.addAttribute("students", students);
		model.addAttribute("schoolClass", myClass);

		System.out.println("ClassController:/getAllSubject");
		return "classPage";

	}

	@PreAuthorize("hasAnyRole('ROLE_STUDENT')")
	@RequestMapping(value = "/{subjectId}", method = RequestMethod.GET)
	public String getSubject(Model model,
			@PathVariable("subjectId") String subjectId) {

		System.out.println("ClassController:/getSubject=> subjectId="
				+ subjectId);
		Test test = null;
		try {
			test = csService.getEnableTest(true, subjectId);
		} catch (Exception exception) {
			System.out.println("???Exception getEnabledTest");
		}

		String viewName = "subjectPage";

		System.out.println("ClassController:/getSubject=> test = " + test);

		if (test != null) {
			System.out.println("ClassController:/getSubject=> test!=null");
			TestRecord testRecord = csService.getTestRecordOnTestId(
					SecurityUtils.getLogedInAccount().getId(),
					test.getDateId(), test.getId());

			if (testRecord != null) { // if record is available
				// if (test.getId() == testRecord.getTestId()) {
				System.out
						.println("ClassController:/getSubject=> testRecord!=null");
				Subject subject = csService.getSubject(subjectId, true, false);
				Collections.reverse(subject.getPosts());
				viewName = "subjectPage";
				model.addAttribute("newPost", new Post());
				model.addAttribute("newComment", new Comment());
				model.addAttribute("subject", subject);

			} else {
				System.out
						.println("ClassController:/getSubject=> test level_2");
				for (int i = 0; i < test.getMcqs().size(); i++)
					test.getMcqs().get(i).setRightAnswer("");

				model.addAttribute("test", test);
				model.addAttribute("subject",
						csService.getSubject(subjectId, false, false));
				viewName = "createTest";
			}
		} else {
			System.out.println("ClassController:/getSubject=> test==null");
			viewName = "subjectPage";
			Subject subject = csService.getSubject(subjectId, true, false);
			Collections.reverse(subject.getPosts());
			model.addAttribute("newPost", new Post());
			model.addAttribute("newComment", new Comment());
			model.addAttribute("subject", subject);
		}
		return viewName;
	}

	@PreAuthorize("hasAnyRole('ROLE_STUDENT')")
	@RequestMapping(value = "/{subjectId}/chek-test", method = RequestMethod.POST)
	public String checkTest(Model model,
			@PathVariable("subjectId") String subjectId,
			@ModelAttribute("test") Test test) {
		int right = 0;
		int wrong = 0;

		Test test2 = csService.getTest(test.getId());
		int total = test2.getMcqs().size();

		for (int i = 0; i < test.getMcqs().size(); i++) {
			try {
				if (test.getMcqs()
						.get(i)
						.getRightAnswer()
						.equalsIgnoreCase(
								test2.getMcqs().get(i).getRightAnswer())) {
					right++;
					System.out.println("ClassController=>checkTest/ right = "
							+ right);
				} else {
					wrong++;
					System.out.println("ClassController=>checkTest/ wrong = "
							+ wrong);
				}
			} catch (NullPointerException e) {
				wrong++;
				System.out.println("Null pointer Ex in right option wrong ="
						+ wrong);
			}

		}

		TestRecord testRecord = new TestRecord(test2.getnPQ(), right, wrong,
				total, test2.getId(),
				SecurityUtils.getLogedInAccount().getId(), subjectId,
				test2.getTestName(), test2.getDateId());

		System.out.println("ClassController=>checkTest/ TestRecord = "
				+ testRecord);
		csService.saveOrUpdateTestRecord(testRecord);
		CompositeResult compResult = csService.getCompositeResult(subjectId,
				SecurityUtils.getLogedInAccount().getId());
		if (compResult != null) {
			double tempPer = (testRecord.getPercent()+compResult.getComPercent())/(compResult.getNumOfTest()+1);
			 compResult.setComPercent(tempPer);
			compResult.setNumOfTest(compResult.getNumOfTest() + 1);

		} else {
			compResult = new CompositeResult(subjectId, SecurityUtils
					.getLogedInAccount().getId(), 1, testRecord.getPercent());
		}
		csService.saveOrUpdateCompositeResult(compResult);
		model.addAttribute("testRecord", testRecord);
		return "testResult";

	}

	@PreAuthorize("hasAnyRole('ROLE_STUDENT')")
	@RequestMapping(value = "/{subjectId}/view-result-history", method = RequestMethod.GET)
	public String viewStudentResult(Model model,
			@PathVariable("subjectId") String subjectId) {
		System.out.println("SubjectController: /viewStudentResult ");

		String id = SecurityUtils.getLogedInAccount().getId();
		if(SecurityUtils.hasRoleOrPermission("ROLE_GUARDIAN")){
			
		}else{
			
		}
		
		
		List<TestRecord> records = csService.getTestRecordsOnStuAndSubId(
				subjectId, id);
		CompositeResult compResult = csService.getCompositeResult(subjectId, id);
		
		for (TestRecord x : records)
			System.out.println("ClassController/viewResultHistory record = "
					+ x);
		
		System.out.println("ClassController/viewResultHistory compRecord = "+ compResult);
	
		model.addAttribute("compResult",compResult);
		model.addAttribute("testRecords", records);

		return "testResult";
	}

}
