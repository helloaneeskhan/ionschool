package edu.finalyear.project.controllers;

import java.security.Principal;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.finalyear.project.models.Account;
import edu.finalyear.project.models.Head;
import edu.finalyear.project.models.School;
import edu.finalyear.project.models.TestRecord;
import edu.finalyear.project.schoolprofile.SchoolProfile;
import edu.finalyear.project.schoolprofile.SchoolProfileUtils;
import edu.finalyear.project.security.Role;
import edu.finalyear.project.security.SecurityUtils;
import edu.finalyear.project.service.AccountService;
//import edu.finalyear.project.service.SchoolProfileService;
import edu.finalyear.project.service.SchoolService;
import edu.finalyear.project.utils.ContextUtils;
import static edu.finalyear.project.security.SecurityUtils.*;

@Controller
public class HomeController {

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils contextUtils;

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;

	@Autowired
	@Qualifier("SchoolService")
	private SchoolService schoolService;

	

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String getIndexGet(Model model) {
		System.out.printf("HomeController/getIndexGet: \n");
		List<SchoolProfile> list = schoolService.getSchoolProfileLike("testschool");
		model.addAttribute("schoolList", list.subList(0, 3));
		model.addAttribute("newHead", new Head());
		return "iownschool/index";
	}

	
	@RequestMapping(value = "", method = RequestMethod.POST)
	public String getIndexPost(@ModelAttribute("newHead") Head newHead,
			Model model) {

		newHead.setId(SchoolProfileUtils.getGeneratedHeadId(newHead));
		
		accountService.saveOrUpdateHead(newHead);
		schoolService.saveOrUpdateSchool(newHead.getSchool());
		System.out.printf(
				"HomeController/getIndexPost: newHead=%s, newHead.school=%s\n",
				newHead, newHead.getSchool());
		return "redirect:/";
	}

	
	
	@RequestMapping(value = "login-failed")
	public String getLoginPage(Model model) {
		System.out.println("inside HomeController/getLoginPage");
		// return "redirect:" + contextUtils.getContextSchool().getSchoolId()
		// + "/login?status=failled";

		return "redirect:" + contextUtils.getContextSchool().getSchoolId()
				+ "/";
	}

	@RequestMapping(value = "denied")
	public String getDeniedPage(Model model, @PathVariable("role") String rol) {
		
		// System.out.println("Role:"+hasRoleOrPermission(rol));
		// return "test-page";
		return "redirect:/"+contextUtils.getContextSchoolId()+"/"+SecurityUtils.getLogedInAccount().getId()+"/";

	}

	// @PreAuthorize("isAuthenticated()")
	private void setInitialSetting() {
		contextUtils.setContextSchoolById(SecurityUtils.getLogedInAccount()
				.getSchoolId());

	}

	@RequestMapping(value = "authorize-setting")
	public String authorizeInitialSetting(Model model) {
		System.out.println("HomeController: getTForward");
		setInitialSetting();
		return "redirect:/" + contextUtils.getContextSchoolId()
				+ "/";
	}

	@RequestMapping(value = "target")
	public String getTargetPage(Model model, Principal principal) {
		System.out.println("HomeController: getTargetPage");

		return "forward:authorize-setting";
	}

	@RequestMapping(value = "logout")
	public String logout(Model model, Principal principal) {
		System.out.println("HomeController/logout");
		removeInitialSetting();

		return "redirect:/";
	}

//	@PreAuthorize("isAnonymous()")
	private void removeInitialSetting() {
		contextUtils.removeAttributeByName("context_school");
	}

	@RequestMapping(value = "authenticating-mail")
	public String authorizingSchoolProfile(Model model,
			@RequestParam("headId") String headId) {
		// please enabled the user account;
		// please enabled the schoolProfile, as in mail direction
	
		System.out.println("HomeController/authorizingSchoolProfile 1 headId = "+headId);
		Head head = accountService.getHeadAccount(headId);
		System.out.println("HomeController/authorizingSchoolProfile 1/Head = "+head);
		head.setEnabled(true);
		head.setSchoolId(head.getSchool().getSchoolId());//schoolId for Account,persisting
		System.out.println("HomeController/authorizingSchoolProfile 2");
		head.getRoles().add(accountService.getRole("head"));
		System.out.println("HomeController/authorizingSchoolProfile 3");
		accountService.saveOrUpdateHead(head);

		System.out.println("HomeController/authorizing...: Head=" + head);

		System.out.println("HomeController/authorizing...:head.school="
				+ head.getSchool());

		schoolService.initializeNewSchool(head.getSchool());

		return "redirect:/" + head.getSchool().getSchoolId() + "/";
	}



	
	@RequestMapping(value = "testResult", method = RequestMethod.GET)
	public String getInsdexGet(Model model) {
		System.out.printf("HomeController/getIndexGet: \n");
//		model.addAttribute("testRecord", new TestRecord(5.0,92,20, 100, 23l, "Student Id", "Subject Id","My Test Name"));
		return "testResult";
	
		
	}
	
	
	

}
