package edu.finalyear.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.finalyear.project.models.CompositeResult;
import edu.finalyear.project.models.TestRecord;
import edu.finalyear.project.security.SecurityUtils;
import edu.finalyear.project.service.AccountService;
import edu.finalyear.project.service.ClassAndSubjectService;
import edu.finalyear.project.utils.ContextUtils;

@Controller
@PreAuthorize("hasAnyRole('ROLE_GUARDIAN')")
@RequestMapping(value = "/{schoolId}/school-boy")
public class GuardianController {

	@Autowired
	@Qualifier("ClassAndSubjectService")
	private ClassAndSubjectService csService;

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils context;

	
	@PreAuthorize("hasAnyRole('ROLE_GUARDIAN')")
	@RequestMapping(value = "/{subjectId}/view-result-history", method = RequestMethod.GET)
	public String viewStudentResult(Model model,
			@PathVariable("subjectId") String subjectId) {
		System.out.println("SubjectController: /viewStudentResult ");

		String id = SecurityUtils.getLogedInAccount().getGuardianStudentId();

		List<TestRecord> records = csService.getTestRecordsOnStuAndSubId(
				subjectId, id);
		CompositeResult compResult = csService
				.getCompositeResult(subjectId, id);

		for (TestRecord x : records)
			System.out.println("ClassController/viewResultHistory record = "
					+ x);

		System.out.println("ClassController/viewResultHistory compRecord = "
				+ compResult);

		model.addAttribute("compResult", compResult);
		model.addAttribute("testRecords", records);

		return "testResult";
	}
}