package edu.finalyear.project.controllers;

import static edu.finalyear.project.security.SecurityUtils.getLogedInAccount;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.finalyear.project.models.Comment;
import edu.finalyear.project.models.MCQS;
import edu.finalyear.project.models.Post;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Subject;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.models.Test;
import edu.finalyear.project.models.TestRecord;
import edu.finalyear.project.security.SecurityUtils;
import edu.finalyear.project.service.AccountService;
import edu.finalyear.project.service.ClassAndSubjectService;
import edu.finalyear.project.utils.ContextUtils;

//import edu.finalyear.project.service.SchoolClassService;

@Controller
@RequestMapping(value = "/{schoolId}/my-subject")
public class SubjectContoller {

	@Autowired
	@Qualifier("ClassAndSubjectService")
	private ClassAndSubjectService csService;

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils context;

	@RequestMapping(value = "/all-subjects")
	@PreAuthorize("hasAnyRole('ROLE_TEACHER')")
	public String getAllSubject(Model model) {
		System.out.println("ClassController:/getAllSubject");

		Teacher teacher = accountService.getTeacherAccount(SecurityUtils
				.getLogedInAccount().getId(), true);

		model.addAttribute("subjects", teacher.getSubjects());
		// model.addAttribute("schoolClass", myClass);
		System.out.println("ClassController:/getAllSubject");
		return "classPage";

	}
	
	
	
	@PreAuthorize("hasAnyRole('ROLE_TEACHER')")
	@RequestMapping(value = "/{subjectId}", method = RequestMethod.GET)
	public String getSubject(Model model,
			@PathVariable("subjectId") String subjectId) {

		System.out.println("ClassController:/getSubject=> subjectId="
				+ subjectId);
		Subject subject = csService.getSubject(subjectId, true, false);
		Collections.reverse(subject.getPosts());
		System.out.println("ClassController/getSubject subject = " + subject
				+ ", subjectId=" + subjectId);
		// csService.initializeSubjectPost(subject);
		model.addAttribute("newPost", new Post());
		model.addAttribute("subject", subject);
		model.addAttribute("newComment", new Comment());
		return "subjectPage";

	}

	
	@PreAuthorize("hasAnyRole('ROLE_TEACHER')")
	@RequestMapping(value = "/{subjectId}/all-test", method = RequestMethod.GET)
	public String getTest(Model model,
			@PathVariable("subjectId") String subjectId,
			@RequestParam(required = false, value = "testId") Integer testId) {

		System.out.println("ClassController:/getAllSubject subjectId="
				+ subjectId + ",testId = " + testId);
		Test newTest = null;
		String viewName = "all-test";
		if (testId != null) {
			newTest = csService.getTest(testId);
			System.out.println("ClassController:/getAllSubject test ="
					+ newTest);

			if (newTest != null) {
				System.out.println("ClassController:/getAllSubject level 1");
				model.addAttribute("newTest", newTest);
				Subject subject = csService.getSubject(subjectId, false, false);
				model.addAttribute("subject", subject);
				viewName = "createTest";
			} else {
				System.out.println("ClassController:/getAllSubject 2");
				Subject subject = csService.getSubject(subjectId, false, true);

				model.addAttribute("subject", subject);
			}
		} else {

			System.out.println("ClassController:/getAllSubject 3");
			Subject subject = csService.getSubject(subjectId, false, true);
			System.out.println("ClassController/getAllSubject subject.test = "
					+ subject.getTest());
			model.addAttribute("subject", subject);
		}
		System.out.println("ClassController/getAllSubject subject = ");// +subject+", subjectId="+subjectId);

		return viewName;

	}


	@PreAuthorize("hasAnyRole('ROLE_TEACHER')")
	@RequestMapping(value = "/{subjectId}/create-test", method = RequestMethod.GET)
	public String createTestGet(Model model,
			@PathVariable("subjectId") String subjectId) {

		System.out
				.println("ClassController:/createTest subjectId=" + subjectId);
		// Subject subject = csService.getSubject(subjectId);

		Test test = new Test();
		for (int i = 0; i < 10; i++)
			test.getMcqs().add(new MCQS());

		model.addAttribute("newTest", test);

		model.addAttribute("subject",
				csService.getSubject(subjectId, false, false));

		System.out.println("ClassController/createTestGet subject = ");// +subject+", subjectId="+subjectId);

		return "createTest";

	}
	
	
	@PreAuthorize("hasAnyRole('ROLE_TEACHER')")
	@RequestMapping(value = "/{subjectId}/create-test", method = RequestMethod.POST)
	public String createTestPost(Model model,
			@PathVariable("subjectId") String subjectId,
			@ModelAttribute("newTest") Test test) {

		System.out
				.println("ClassController:/createTest subjectId=" + subjectId);
		Subject tempsubj = csService.getSubject(subjectId, false, true);

		test.setSubject(tempsubj);
		if (tempsubj.getTest() != null)
			test.setId(tempsubj.getTest().getId());
		test.setDateId(new Date().toString().trim());
		csService.saveOrUpdate(test);

		System.out.println("ClassController/createTestPost subject = "
				+ tempsubj);

		return "redirect:/" + context.getContextSchoolId() + "/my-subject/"
				+ subjectId;

	}

	
	@PreAuthorize("hasAnyRole('ROLE_TEACHER')")
	@RequestMapping(value = "/{subjectId}/view-students-result", method = RequestMethod.GET)
	public String viewStudentResult(Model model,
			@PathVariable("subjectId") String subjectId) {
		System.out.println("SubjectController: /viewStudentResult ");

		Subject subject = csService.getSubject(subjectId, false, true);
		List<TestRecord> records;
		try {
			records = csService.getTestRecords(
					subject.getSubjectId(), subject.getTest().getId());
		} catch (Exception e) {
			// TODO: handle exception
			records = null;
		}

		model.addAttribute("testRecords", records);

		return "testResult";
	}

	
	
	@RequestMapping(value = "")
	@PreAuthorize("hasAnyRole('ROLE_TEACHER')")
	public String getSubject(Model model) {
		String id = getLogedInAccount().getId();
		Student student = accountService.getStudentAccount(id, false, false);

		System.out.println("SubjectController:/getSubject getLogedInAccount="
				+ getLogedInAccount());
		System.out.println("SubjectController: /getSubject Student=" + student);

		// classService.getSchoolClass(student.getClassId());

		return "main";
	}

}
