package edu.finalyear.project.controllers;

import java.util.List;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import edu.finalyear.project.models.Account;
import edu.finalyear.project.models.Comment;
import edu.finalyear.project.models.Post;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Subject;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.models.Test;
import edu.finalyear.project.models.TestRecord;
import edu.finalyear.project.schoolprofile.About;
import edu.finalyear.project.schoolprofile.AboutItem;
import edu.finalyear.project.schoolprofile.EventItem;
import edu.finalyear.project.schoolprofile.FacultyItem;
import edu.finalyear.project.schoolprofile.GalleryItem;
import edu.finalyear.project.schoolprofile.SchoolProfile;
import edu.finalyear.project.schoolprofile.SchoolProfileUtils;
import edu.finalyear.project.security.SecurityUtils;
import edu.finalyear.project.service.AccountService;
import edu.finalyear.project.service.ClassAndSubjectService;
//import edu.finalyear.project.service.SchoolProfileService;
import edu.finalyear.project.service.SchoolService;
import edu.finalyear.project.utils.ContextUtils;

@Controller
@Scope("session")
@RequestMapping(value = "/static")
public class StaticController {

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;

	@Autowired
	@Qualifier("SchoolService")
	private SchoolService schoolService;

	@Autowired
	@Qualifier("ClassAndSubjectService")
	private ClassAndSubjectService csService;

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils context;

	@PreAuthorize("hasAnyRole('ROLE_HEAD')")
	@RequestMapping(value = "update-item", method = RequestMethod.POST)
	public String updateItem(
			@ModelAttribute("schoolProfile") SchoolProfile schoolProfile,
			@RequestParam("topic") String topic,
			@RequestParam(value = "itemId") int itemId,
			@RequestParam("image") MultipartFile image) throws IOException {

		System.out.println("StaticController/updateItem: ");

		if (topic.equals("about")) {
			for (AboutItem item : schoolProfile.getAbout().getItems()) {
				if (item.getId() == itemId) {
					if (!image.isEmpty())
						item.setImage(image.getBytes());
					schoolService.saveOrUpdateItem(item);
					;
				}
			}

		} else if (topic.equals("gallery")) {
			System.out.println("StaticController/updateItem: gallery");
			for (GalleryItem item : schoolProfile.getGallery().getItems()) {
				if (item.getId() == itemId) {
					if (!image.isEmpty())
						item.setImage(image.getBytes());
					schoolService.saveOrUpdateItem(item);

				}
			}

		} else if (topic.equals("faculty")) {

			System.out.println("StaticController/updateItem: faculty");
			for (FacultyItem item : schoolProfile.getFaculty().getItems()) {
				if (item.getId() == itemId) {
					if (!image.isEmpty())
						item.setImage(image.getBytes());
					schoolService.saveOrUpdateItem(item);
				}

			}
		} else if (topic.equals("event")) {
			System.out.println("StaticController/updateItem: event id="
					+ itemId);

			System.out.println("Event=" + schoolProfile.getEvent());
			for (EventItem item : schoolProfile.getEvent().getItems()) {
				if (item.getId() == itemId) {
					if (!image.isEmpty())
						item.setImage(image.getBytes());
					schoolService.saveOrUpdateItem(item);
				}

			}

		} else if (topic.equals("ga")) {

		}

		return "redirect:/" + schoolProfile.getSchoolId() + "/school-profile";
	}

	
	@PreAuthorize("hasAnyRole('ROLE_HEAD')")
	@RequestMapping(value = "update-school-profile", method = RequestMethod.POST)
	public String updateSchoolProfile(
			@ModelAttribute("schoolProfile") SchoolProfile schoolProfile,
			@RequestParam("logo_image") MultipartFile logo,
			@RequestParam("coverOne_image") MultipartFile cover1,
			@RequestParam("coverTwo_image") MultipartFile cover2,
			@RequestParam("coverThree_image") MultipartFile cover3,
			@RequestParam("centerImage") MultipartFile centerImage) {

		System.out
				.println("StaticController/updateSchoolProfile: level 1/ cover1.isEmpty()"
						+ cover1.isEmpty());

		try {
			if (!logo.isEmpty())
				schoolProfile.setLogo(logo.getBytes());
			if (!cover1.isEmpty())
				schoolProfile.setCover1(cover1.getBytes());
			if (!cover2.isEmpty())
				schoolProfile.setCover2(cover2.getBytes());
			if (!cover3.isEmpty())
				schoolProfile.setCover3(cover3.getBytes());
			if (!centerImage.isEmpty())
				schoolProfile.getAbout().setCenterImage(centerImage.getBytes());
			System.out
					.println("StaticController/updateSchoolProfile: cover1.size"
							+ cover1.getSize());// +"\t cover2.size="+cover2.getSize());
		} catch (IOException e) {

			e.printStackTrace();
		}

		// spService.saveOrUpdateAbout(schoolProfile.getAbout());

		schoolService.saveOrUpdate(schoolProfile);
		return "redirect:/" + schoolProfile.getSchoolId() + "/school-profile";
	}

	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "get-profile-photo/{id}", method = RequestMethod.GET)
	public void getProfilePhoto(HttpServletRequest request,
			HttpServletResponse responce, @PathVariable("id") String id) {

		responce.setContentType("image/jpeg");
		System.out.println("StaticController:/getProfilePhoto() level_1");
		try {
			responce.getOutputStream().write(
					accountService.getAccount(id).getProfilePhoto());

		} catch (IOException e) {

			e.printStackTrace();
		}
		System.out.println("StaticController:/getProfilePhoto level_2");
	}



	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "search", method = RequestMethod.GET)
	// headers="Accept=*/*")
	public @ResponseBody List<SchoolProfile> createSchool(
			@RequestParam("term") String term, HttpServletResponse responce) {

		System.out.println("******************" + term);

		List<SchoolProfile> list = schoolService.getSchoolProfileLike(term);

		System.out.println("list.size=" + list.size());
		return list;
	}

	@RequestMapping(value = "school-id", method = RequestMethod.POST)
	public String schoolThroughSearch(@RequestParam("mysearch") String mysearch) {
		return "redirect:/" + mysearch + "/";
	}

	@PreAuthorize("hasAnyRole('ROLE_HEAD')")
	@RequestMapping(value = "school-class-ajax", method = RequestMethod.GET)
	// headers="Accept=*/*")
	public @ResponseBody List<SchoolClass> schoolClassAjax(
			@RequestParam("term") String term, HttpServletResponse responce) {

		System.out.println("*********schoolClassAjax*********" + term);

		List<SchoolClass> list = csService.getSchoolClassLike(term);

		System.out.println("list.size=" + list.size());
		return list;
	}

	@PreAuthorize("hasAnyRole('ROLE_HEAD')")
	@RequestMapping(value = "teacher-ajax", method = RequestMethod.GET)
	// headers="Accept=*/*")
	public @ResponseBody List<Teacher> teacherAjax(
			@RequestParam("term") String term, HttpServletResponse responce) {

		System.out.println("*********teacherAjax*********" + term);

		List<Teacher> list = accountService.getTeacherAccountLike(term);

		System.out.println("list.size=" + list.size());
		return list;
	}

	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "account-ajax", method = RequestMethod.GET)
	public @ResponseBody List<Account> accountAjax(
			@RequestParam("term") String term, HttpServletResponse responce) {
		System.out.println("*********accountAjax*********" + term);
		List<Account> list = accountService.getAccountLike(term);
		System.out.println("list.size=" + list.size());
		return list;
	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/new-post", method = RequestMethod.POST)
	public String newPost(@ModelAttribute("newPost") Post newPost,
			@RequestParam("subjectId") String subjectId, Model model) {
		System.out.println("ClassController:/newPost=> subjectId=" + subjectId);

		String viewName = "redirect:/" + context.getContextSchoolId() + "/";

		if (SecurityUtils.hasRoleOrPermission("ROLE_STUDENT")) {
			newPost.setAuthType("Student");
			viewName = "redirect:/" + context.getContextSchoolId()
					+ "/my-class/" + subjectId;
		}

		if (SecurityUtils.hasRoleOrPermission("ROLE_TEACHER")) {
			newPost.setAuthType("Teacher");
			viewName = "redirect:/" + context.getContextSchoolId()
					+ "/my-subject/" + subjectId;
		}

		newPost.setSubjectId(subjectId);
		newPost.setAccount(SecurityUtils.getLogedInAccount());
		System.out.println("StaticController:/newPost subjectId=" + subjectId);
		csService.newPost(newPost);

		model.addAttribute("newPost", new Post());
		Subject subject = csService.getSubject(subjectId, true, false);

		model.addAttribute("subject", subject);

		return viewName;

	}

	
	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/new-comment", method = RequestMethod.POST)
	public String newComment(@ModelAttribute("newComment") Comment comment,
			@RequestParam("subjectId") String subjectId, Model model) {

		comment.setAccountId(SecurityUtils.getLogedInAccount().getId());
		comment.setAuthorName(SecurityUtils.getLogedInAccount().getFullName());
		System.out.println("StaticController/newComment comment = " + comment);

		csService.saveOrUpdateComment(comment);

		String viewName = "redirect:/" + context.getContextSchoolId() + "/";
		if (SecurityUtils.hasRoleOrPermission("ROLE_STUDENT"))
			viewName = "redirect:/" + context.getContextSchoolId()
					+ "/my-class/" + subjectId;

		if (SecurityUtils.hasRoleOrPermission("ROLE_TEACHER"))
			viewName = "redirect:/" + context.getContextSchoolId()
					+ "/my-subject/" + subjectId;

		return viewName;

	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "/{subjectId}/determine-role-subject")
	public String subjectAndRole(Model model,
			@PathVariable("subjectId") String subjectId) {

		String viewName = "redirect:/"
				+ SecurityUtils.getLogedInAccount().getId() + "/";
		if (SecurityUtils.hasRoleOrPermission("ROLE_TEACHER"))
			viewName = "redirect:/" + context.getContextSchoolId()
					+ "/my-subject" + "/" + subjectId;

		if (SecurityUtils.hasRoleOrPermission("ROLE_STUDENT"))
			viewName = "redirect:/" + context.getContextSchoolId()
					+ "/my-class" + "/" + subjectId;

		return viewName;

	}

	// ??????????????????????????????? Testing???????????????????????????????

	@RequestMapping(value = "scopes")
	public void chekScope(Model model) {

		csService.saveOrUpdateTestRecord(new TestRecord());
		// csService.saveOrUpdate(new Test());
		// System.out.println("**************static scope student = " +
		// testRecord);
	}


	@RequestMapping(value = "scopet")
	public void chekScopeT(Model model) {
		String gmt = new Date().toGMTString();
		String local = new Date().toLocaleString();
		String toStri = new Date().toString();
		System.out.println("**************static scope name Date = " + new Date().toString().trim()+" \n local ="+local+"\n gmt ="+gmt);
	}

	@Secured("hasRole('ROLE_STUDENT')")
	public void chekMethodStudent(String name) {
		System.out.println("**************STUDENT static scope");
		name = "student is here****************";
	}

	@Secured("hasRole('ROLE_TEACHER')")
	public void chekMethodteacher(String name) {
		name = "teacher is here****************";
		System.out.println("**************TEACHER static scope");
	}

	// ??????????????????????????????? Testing???????????????????????????????
}
