package edu.finalyear.project.controllers;

import java.io.IOException;
import java.security.Principal;
import java.text.DateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Null;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import edu.finalyear.project.models.Account;
import edu.finalyear.project.models.Comment;
import edu.finalyear.project.models.Guardian;
import edu.finalyear.project.models.Head;
import edu.finalyear.project.models.Post;
import edu.finalyear.project.models.School;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Subject;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.schoolprofile.About;
import edu.finalyear.project.schoolprofile.Gallery;
import edu.finalyear.project.schoolprofile.SchoolProfile;
import edu.finalyear.project.security.Role;
import edu.finalyear.project.security.SecurityUtils;
import edu.finalyear.project.service.AccountService;
import edu.finalyear.project.service.ClassAndSubjectService;
//import edu.finalyear.project.service.SchoolProfileService;
//import edu.finalyear.project.service.SchoolClassService;
import edu.finalyear.project.service.SchoolService;
import edu.finalyear.project.service.StudentService;
import edu.finalyear.project.service.TeacherService;
import edu.finalyear.project.utils.ContextUtils;
import static edu.finalyear.project.security.SecurityUtils.*;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("/{schoolId}")
public class MainController {

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils contextUtils;

	@Autowired
	@Qualifier("ClassAndSubjectService")
	private ClassAndSubjectService csService;

	@Autowired
	@Qualifier("SchoolService")
	private SchoolService schoolService;

	@Autowired
	@Qualifier("studentService")
	private StudentService studentService;

	@Autowired
	@Qualifier("teacherService")
	private TeacherService teacherService;

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;

	@RequestMapping(value = "/")
	public String getMySchoolPage(Principal principal, Model model,
			@PathVariable("schoolId") String schoolId) {

		System.out.println("MainController: \"\"");

		String page = "";
		if (principal == null) {// for Anonymous

			System.out.println("inside /{SchoolId}/ authentication=false");

			SchoolProfile schoolProfile = schoolService
					.getSchoolProfile(schoolId);

			if (schoolProfile != null) {// profile exits then return profile

				contextUtils.setContextSchoolId(schoolId);

				
				model.addAttribute("schoolProfile", schoolProfile);

				return "/front/schoolProfile";
			} else

				return "schoolNotFound";
		} else {

			for (Role role : SecurityUtils.getRoles())
				System.out.println("MainController: \"\" role = " + role);

			System.out.println("inside /{SchoolId}/ authentication=true");

			if (SecurityUtils.hasRoleOrPermission("ROLE_GUARDIAN")) {
				Student student = accountService.getStudentAccount(
						SecurityUtils.getLogedInAccount()
								.getGuardianStudentId(), true, true);
				model.addAttribute("compResult", student.getCompositeResult());
				model.addAttribute("student", student);
				page = "guardianProfile";

			}

			if (SecurityUtils.hasRoleOrPermission("ROLE_STUDENT")) {
				Student student = accountService.getStudentAccount(
						SecurityUtils.getLogedInAccount().getId(), true, true);
				model.addAttribute("student", student);
				page = "studentProfile";
			}

			if (SecurityUtils.hasRoleOrPermission("ROLE_HEAD")) {
				page = "test-page";
			}

			if (SecurityUtils.hasRoleOrPermission("ROLE_TEACHER")) {
				model.addAttribute("teacher", accountService.getTeacherAccount(
						SecurityUtils.getLogedInAccount().getId(), false));
				page = "teacherProfile";
			}
//			return "redirect:/" + contextUtils.getContextSchoolId() + "/"
//					+ SecurityUtils.getLogedInAccount().getId();
			return page;
		}
	}

	@RequestMapping(value = "/{accountId}")
	@PreAuthorize("isAuthenticated()")
	public String accountProfile(Model model,
			@PathVariable("accountId") String accountId) {

		String page = "redirect:/" + getLogedInAccount().getSchoolId() + "/";
		Account account = accountService.getAccountByIds(contextUtils
				.getContextSchool().getSchoolId(), accountId);
		if (account instanceof Student) {
			Student student = accountService.getStudentAccount(account.getId(),
					true, true);
			model.addAttribute("student", student);
			page = "studentProfile";
		}

		if (account instanceof Teacher) {
			model.addAttribute("teacher",
					accountService.getTeacherAccount(account.getId(), false));
			page = "teacherProfile";
		}

		if (account instanceof Guardian) {
			Student student = accountService.getStudentAccount(
					account.getGuardianStudentId(), true, true);
			model.addAttribute("compResult", student.getCompositeResult());
			model.addAttribute("student", student);
			page = "guardianProfile";

		}

//		if (account instanceof Head) {

			// Student student =
			// accountService.getStudentAccount(account.getGuardianStudentId(),
			// true,true);
			// Head head = accountService.getHeadAccount(accountId);
			// model.addAttribute("head",head);
			// model.addAttribute("compResult",student.getCompositeResult());
			// model.addAttribute("student",student);
//			page = "test-pag";
//		}
		System.out.println("Account = " + account);
		System.out.println("Instance of Student="
				+ (account instanceof Student));
		System.out.println("Instance of Teacher="
				+ (account instanceof Teacher));
		return page;

	}

	@PreAuthorize("isAuthenticated()")
	@RequestMapping(value = "school-profile")
	public String getSchoolProfile(Model model) {
		System.out.println("inside /{SchoolId}/ authentication=false");

		SchoolProfile schoolProfile = schoolService
				.getSchoolProfile(contextUtils.getContextSchoolId());

		if (schoolProfile != null) {// profile exits then return profile

			Hibernate.initialize(schoolProfile.getAbout());
			Hibernate.initialize(schoolProfile.getEvent());
			Hibernate.initialize(schoolProfile.getFaculty());
			Hibernate.initialize(schoolProfile.getGallery());

			model.addAttribute("schoolProfile", schoolProfile);

			return "/front/schoolProfile";
		}

		return "/schoolNotFound";

	}

	@RequestMapping(value = "main")
	public String testingMainPage(Principal principal, Model model,
			@PathVariable("schoolId") String schoolId) {
		System.out.println("MainController: /{SchoolId}/main");
		return "main";
	}

	@RequestMapping(value = "login")
	public String getLoginPage(@PathVariable("schoolId") String schoolId,
			Model model, HttpServletRequest request, Principal principal) {
		if (principal == null) {
			contextUtils.setContextSchoolById(schoolId);
			String status = request.getParameter("status");// login?status=false
															// from
															// sprint-security
			model.addAttribute("status", status);// set to identify in login
													// page, for validation
			System.out
					.println("====>> inside MainController/getLoginPage status="
							+ status);

			return "login";
		} else {
			return "redirect:/" + getLogedInAccount().getSchoolId() + "/";// return
																			// to
																			// {schoolId}/
																			// if
																			// logged
																			//
		}
	}

	@RequestMapping(value = "security_check")
	public String securityCheck(Model model) {
		System.out.println("====>> inside MainController/secutiyCheck");
		return "redirect:/j_spring_security_check";
	}

	@RequestMapping(value = "view-class/{classId}", method = RequestMethod.GET)
	public String showSchoolClass(Model model,
			@PathVariable("classId") String classId) {

		System.out
				.println("===>>inside /{schoolId}/view-class/classId *(level_1)*");

		return "view-class";
	}

	@RequestMapping(value = "getstudent/{studentId}", method = RequestMethod.GET)
	public String getStudent(Model model,
			@PathVariable("studentId") String studentId) {

		School school = schoolService.getSchool(contextUtils.getContextSchool()
				.getSchoolId());

		System.out
				.println("===>>inside /{schoolId}/getclass/classId (level_1);School:"
						+ school);

		Student student = contextUtils.getStudent(studentId,
				school.getStudents());
		model.addAttribute("get_student", student);
		System.out.println("===>>inside /{schoolId}/getclass/classId (level_2)"
				+ student);
		return "show-student";
	}

	@RequestMapping(value = "viewclasses", method = RequestMethod.GET)
	public String viewClass(Model model) {

		// School school = contextUtils.getContextSchool();
		School school = schoolService.getSchool(contextUtils.getContextSchool()
				.getSchoolId());
		System.out
				.println("===>>inside /{schoolId}/viewclasses (level_1):ContextSchool=>"
						+ school);

		// Hibernate.initialize(school.getSchoolClasses());
		System.out
				.println("===>>inside /{schoolId}/viewclasses (level_2):ContextSchoolAllClass"
						+ school.getSchoolClasses());

		model.addAttribute("classList", school.getSchoolClasses());

		return "view-classes";
	}

	@RequestMapping(value = "all-classes")
	public String getAllClasses(Model model) {

		// Set<SchoolClass> schoolClass = schoolService.getSchool(
		// getLogedInAccount().getSchoolId()).getSchoolClasses();
		// School school = schoolService.getSchool(
		// getLogedInAccount().getSchoolId());
		// Hibernate.initialize(school.getSchoolClasses());
		model.addAttribute("classList", schoolService.getAllClass());
		return "showClasses";
	}

	
	@RequestMapping(value = "all-classes/{classId}")
	public String getClass(@PathVariable("classId") String classId, Model model) {

		SchoolClass myClass = csService.getSchoolClass(classId,true,true); // enable it
																	// after
																	// fixing
		// SchoolClass myClass = csService.getSchoolClass("aps_class");//for
		// testing
		Set<Student> students = myClass.getStudents();
		Set<Subject> subjects = myClass.getSubjects();

		model.addAttribute("subjects", subjects);
		model.addAttribute("students", students);
		model.addAttribute("schoolClass", myClass);
		System.out.println("MainController: /{} Students=" + students);

		return "classPage";

	}
	
	@RequestMapping(value = "all-subjects",method = RequestMethod.GET)
	public String getAllSubjects(Model model){
		//work here
		model.addAttribute("subjects", csService.getAllSubjectsVIASchoolId(contextUtils.getContextSchoolId()));

		System.out.println("MainControllerller:/getAllSubject");
		return "classPage";
	}

	@RequestMapping(value = "all-subjects/{subjectId}",method = RequestMethod.GET)
	public String getSubject(Model model,@PathVariable("subjectId") String subjectId){
		//work here
		System.out.println("ClassController:/getSubject=> subjectId="
				+ subjectId);
		Subject subject = csService.getSubject(subjectId, true, false);
		Collections.reverse(subject.getPosts());
		System.out.println("MainController/getSubject subject = " + subject
				+ ", subjectId=" + subjectId);
		// csService.initializeSubjectPost(subject);
		model.addAttribute("newPost", new Post());
		model.addAttribute("subject", subject);
		model.addAttribute("newComment", new Comment());
		return "subjectPage";
	}
	
	
	@RequestMapping(value = "set_context_school_id")
	public String getClsdfass(@PathVariable("schoolId") String schoolId) {
		System.out.println("MainController: /{} set_context_school_id"
				+ schoolId);

		contextUtils.setContextSchoolId(schoolId);

		System.out.println("MainController: /{} set_context_school_id");

		return "redirect:/+authorize-setting";

	}
	
	
	
	@RequestMapping(value = "delete")
	public String deleteCriteria(Model model, @RequestParam(value = "subjectId", required = false) String subjectId, @RequestParam(value = "classId",required = false) String classId){
		String viewName = "redirect:/"+contextUtils.getContextSchoolId()+"/";
		if(subjectId!=null){
			Subject subject = csService.getSubject(subjectId,false,false);
			csService.deleteObject(subject);
			viewName = viewName+"all-classes";
		}
		if(classId!=null){
			SchoolClass sClass = csService.getSchoolClass(classId,false,false);
			csService.deleteObject(sClass);
			viewName = viewName+"all-subjects";
		}
		return viewName;
	}
	

	
	// cheking main controller to jsp
	@RequestMapping(value = "index")
	public String checkIndex(@PathVariable("my") String my, Model model) {
		model.addAttribute("test", "Test from MainController");
		System.out.println("path varialble :" + my);
		return "home";
	}

	@RequestMapping(value = "register")
	public String testing(Model model) {
		model.addAttribute("test", "Test from MainController");
		return "register";
	}

	@RequestMapping(value = "profile")
	public String profile(Model model) {
		return "profile";
	}

	@RequestMapping(value = "myfile")
	public String testMyfile(Model model) {
		model.addAttribute("newTeacher", new Teacher());
		return "myfile";
	}


}
