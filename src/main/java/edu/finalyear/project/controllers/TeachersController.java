package edu.finalyear.project.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.service.TeacherService;

@Controller
@RequestMapping(value = "/teachers")
public class TeachersController {

	@Autowired
	@Qualifier("teacherService")
	private TeacherService teacherService;
	
	
	
	//*************************************************Testing Area*************************************************
	
	@RequestMapping(value = "teacherspage")
	public String testingTeachers(Model model){
		model.addAttribute("newTeacher", new Teacher() );
		return "teachers";
		
	}
	
	@RequestMapping(value = "teacher_list")
	private @ResponseBody String teacherList(Model model) {
		System.out.println("Teacher List: " + teacherService.getTeacherList());
		return "Teacher List:" +teacherService.getTeacherList();
	}
	
	
//	cheking TeacherController to jsp
	@RequestMapping(value = "index")
	public String checkIndex(Model model){
		model.addAttribute("test", "Test from TeachersController");
		return "home";
		
	}
	
	
	public void insertTeacher() {
		for (int i = 0; i < 10; i++) {
//			teacherService.createTeacher(new Teacher("studentId"+i,"SchoolId"+i,"firstName"+i, "lastName"+i, "email"+i, 20000+i));
		}
	}
	
}
