package edu.finalyear.project.controllers;

import java.io.IOException ;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import edu.finalyear.project.models.Account;
import edu.finalyear.project.models.Guardian;
import edu.finalyear.project.models.School;
import edu.finalyear.project.schoolprofile.SchoolProfile;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.TestAccount;
import edu.finalyear.project.models.TestTeacherAccount;
import edu.finalyear.project.service.AccountService;
//import edu.finalyear.project.service.SchoolProfileService;
import edu.finalyear.project.service.SchoolService;
//import edu.finalyear.project.service.TestAccountService;

@Controller
@RequestMapping(value = "cheking")
public class CheckController {

	@Autowired
	@Qualifier("SchoolService")
	private SchoolService schoolService;

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;

//	@Autowired
//	@Qualifier("SchoolProfileService")
//	private SchoolProfileService spService;

	@RequestMapping(value = "view-subject")
	public String viewClass(Model model) {
		System.out.println("inside cheking/view-class");
		return "view-subject";
	}

	@RequestMapping(value = "addTeacher")
	public void addTeacher(Model model) {

		// schoolService.saveOrUpdateSchool(new School("APS",
		// "Army Public School"));
		System.out.println("School=>");
		School school = schoolService.getSchool("APS");
		System.out.println("School=>" + school);
		System.out
				.println("Teacher list of school id 1" + school.getTeachers());
		school.getTeachers().add(
				new Teacher("TeacherId_test", "firstName", "lastName","qualification"));
		schoolService.saveOrUpdateSchool(school);
	}

	@RequestMapping(value = "addStudent")
	public void addStudent() {
		System.out.println("==>Student befor persistant");
		School school = schoolService.getSchool("APS");
		System.out.println("==>Student after persistant = " + school);
		for (int i = 1; i < 30; i++) {
			Student student = new Student("StudentId" + Math.random(),
					"firstName", "schoolId");

			System.out.println("==>Student after persistant = " + student);
			school.getStudents().add(student);
		}
		schoolService.saveOrUpdateSchool(school);
	}

	@RequestMapping(value = "myprofile")
	public String test1(Model model) {
		System.out.println("inside test/tes1");
		return "profile-templet";
	}

	@RequestMapping(value = "test2")
	public String test2(Model model) {
		System.out.println("inside test/tes2");
		return "test/test2";
	}

	@RequestMapping(value = "profile")
	public String pro(Model model) {
		System.out.println("inside test/pro");
		return "profile";
	}

	@RequestMapping(value = "testimage")
	public String testimage(Model model) {
		// System.out.println("inside test/pro");
		System.out.println("inside testimage");
		return "test-page";
	}

	@RequestMapping(value = "loadimage", method = RequestMethod.GET)
	public String loadimage(Model model) {
		// System.out.println("inside test/pro");
		System.out.println("inside loadimage");
		return null;// "test-page";
	}

	@RequestMapping(value = "newschool")
	public String newSchool(Model model) {
		model.addAttribute("newSchoolProfile", new SchoolProfile());
		return "school-registration";
	}



	// /////////////////////////cheking augment////////\
	@RequestMapping(value = "aug")
	public String getPage() {
		return "new-activity";
	}

	@RequestMapping(value = "{id}")
	public void getAccount(@PathVariable String id) {
		System.out.println("Account"+accountService.getAccount(id));
	}



	@RequestMapping(value = "test-account")
	public void testAccount(){
//Random ran=new Random int arra[] = {12,23,45,34,453,4345,345,23,23,534,523,35,23,23,45,42,35,23,35,234,35,35,35,35};
		accountService.saveOrUpdateTeacher(new Teacher("teacher_test"+1, "firstName", "password", "qualificition"));//Account will insert too
		accountService.saveOrUpdateStudent(new Student("student_test"+1, "firstName", "APS","aps_class"));
//		accountService.saveOrUpdate(new Account("account_test", "firstName", "password"));//Only Account will insert
		
	}
}
