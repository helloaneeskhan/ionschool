package edu.finalyear.project.controllers;

import javax.persistence.Transient;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.experimental.theories.ParametersSuppliedBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import edu.finalyear.project.models.School;
import edu.finalyear.project.service.SchoolService;
import edu.finalyear.project.utils.ContextUtils;


public class SchoolHandler implements HandlerInterceptor {

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils contextUtils;

	@Autowired
	@Qualifier("SchoolService")
	private SchoolService schoolService;

	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res,
			Object handler) throws Exception {
		String schoolId = "handler-wrong-school-id";
		try {
			schoolId = req.getRequestURI().split("/")[2]; // /project/schooid/****
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		
		System.out.println("SchoolHandler: Authority.isAuthenticated()="
				+ SecurityContextHolder.getContext().getAuthentication()
						.isAuthenticated());

		
		
		
		//		if (!SecurityContextHolder.getContext().getAuthentication()
//				.getAuthorities().isEmpty())
//			forAnonymous(schoolId);

		return true;
	}

	private void forAnonymous(String schoolId) {
		System.out.println("SchoolHandler: forAnonymous");
		if (isWhiteListURL(schoolId)) {// return "true", if doesn't matched with
										// fixed urls
			System.out.println("SchoolHandler:isWhiteListURL" + true);
			if (!isSchoolExists(schoolId)) {// isSchoolExists then doesn't enter
											// in this method
				System.out.println("SchoolHandler:schoolExists"
						+ isSchoolExists(schoolId));
				contextUtils.setContextSchoolById(schoolId);

			}

		}

	}

	private boolean isWhiteListURL(String schoolId) {
		String[] blackListSchoolId = { "resources", "static" }; // add
																// blackListSchooId
																// here,
		boolean matched = true;
		for (String x : blackListSchoolId) {
			if (x.equals(schoolId))
				matched = false;// matched with blackList
		}

		return matched;// return "true", if doesn't matched with fixed urls

	}

	private boolean isSchoolExists(String schoolId) {
		return contextUtils.getContextSchool().getSchoolId().equals(schoolId);
	}

}
