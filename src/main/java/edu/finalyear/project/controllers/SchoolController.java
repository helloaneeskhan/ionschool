package edu.finalyear.project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.finalyear.project.models.School;
//import edu.finalyear.project.service.SchoolClassService;
import edu.finalyear.project.service.SchoolService;

@Controller("/school")
public class SchoolController {

	@Autowired
	@Qualifier("SchoolService")
	private SchoolService schoolService;
	


	
	@RequestMapping(value = "getSchool")
	public @ResponseBody String getSchool(Model model){
		
		return "School:=>"+schoolService.getSchool("APS");
		
	}
	
	@RequestMapping(value = "addSchools")
	public String addSchools(Model model){
		for(int i = 0; i<10; i++)
			schoolService.saveOrUpdateSchool(new School("testSchool"+i,"fullName "+ i));
			return "home";
	}	
	
	
}
