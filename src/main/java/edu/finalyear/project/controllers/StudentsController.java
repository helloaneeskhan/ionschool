package edu.finalyear.project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.finalyear.project.models.Student;
import edu.finalyear.project.service.StudentService;

@Controller
@RequestMapping("/students")
public class StudentsController {

	@Autowired
	@Qualifier("studentService")
	private StudentService studentService;

	// *************************************************Testing Area*************************************************

	@RequestMapping(value = "studentspage")
	private String studentPage(Model model) {
		// System.out.println("Student List: " +
		// studentService.getStudentList());
		model.addAttribute("newStudent", new Student());
		return "students";
	}

	@RequestMapping(value = "student_list")
	private @ResponseBody String studentList(Model model) {
		// System.out.println("Student List: " +
		// studentService.getStudentList());
		return "Student List: " + studentService.getStudentList();
	}

	// just making initial insertion
	public void insertStudent() {
		for (int i = 0; i < 10; i++) {
			studentService.createStudent(new Student("StudentId"+Math.random(), "firstName","schoolId"));
		}
	}

	// cheking StudentsController to jsp
	@RequestMapping(value = "index")
	public String checkIndex(Model model) {
		model.addAttribute("test", "Test from StudentsController");
		return "home";

	}
}
