package edu.finalyear.project.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.finalyear.project.models.Student;

@Repository("studentRepository")
public class StudentDAOImp implements StudentDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void createStudent(Student student) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().persist(student);
	}

	@Override
	public void updateStudent(Student student) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(student);
	}

	@Override
	public void deleteStudent(Student student) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(student);
	}

	@Override
	public Student getStudent(String id) {
		// TODO Auto-generated method stub
		Query query = getSession().getNamedQuery("findByStudentId");
		query.setParameter("studentId", id);
		return (Student) query.uniqueResult();
	}

	@Override
	public List<Student> getStudentList() {
		// TODO Auto-generated method stub
		return (List<Student>) sessionFactory.getCurrentSession().createCriteria(Student.class).list();
	}

	private Session getSession(){
		return sessionFactory.getCurrentSession();
	}
}
