package edu.finalyear.project.dao;

import static edu.finalyear.project.security.SecurityUtils.getLogedInAccount;

import java.util.Set;
import java.util.List;

import edu.finalyear.project.models.CompositeResult;
import edu.finalyear.project.models.Post;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Subject;
import edu.finalyear.project.models.Test;
import edu.finalyear.project.models.TestRecord;

public interface ClassAndSubjectDAO {

	public Set<SchoolClass> getSchoolClassList();

	public SchoolClass getSchoolClass(String classId, String schoolId);

	public List<SchoolClass> getSchoolClassLike(String classId);

	public Subject getSubject(String subjectId);

	public Set<Subject> getSubjectsListByClassId(String classId);

	public void newPost(Post post);

	public void saveOrUpdateClass(Object schoolClass);

	public void saveOrUpdate(Test test);

	public Test getTest(long id);

	public Test getEnableTest(boolean enable, String subjectId);

	public TestRecord getTestRecordOnTestId(String studentId,String dateId, long testId);

	public List<TestRecord> getTestRecords(String subjectId, long testId);
	
	public List<TestRecord> getTestRecordsOnStuAndSubId(String subjectId, String studentId);

	public void deleteObject(Object object);

	public CompositeResult getCompositeResult(String subjectId, String studentId);

	public List<SchoolClass> getSchoolClassVIASchoolId(String schoolId);
	
	public List<Subject> getAllSubjectsVIASchoolId(String schoolId);
	


}
