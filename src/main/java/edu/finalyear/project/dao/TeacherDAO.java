package edu.finalyear.project.dao;

import java.util.List;

import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Teacher;

public interface TeacherDAO {

	public void createTeacher(Teacher teacher);
	public void updateTeacher(Teacher teacher);
	public void deleteTeacher(Teacher teacher);
	public Teacher getTeacher(String teacherId);
	public List<Teacher> getTeacherList();

}
