package edu.finalyear.project.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.finalyear.project.models.Teacher;

@Repository("teacherRepository")
public class TeacherDAOImpl implements TeacherDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void createTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(teacher);
		
	}

	@Override
	public void updateTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(teacher);
	}

	@Override
	public void deleteTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(teacher);
	}

	@Override
	public Teacher getTeacher(String teacherId) {
		// TODO Auto-generated method stub
		return (Teacher) sessionFactory.getCurrentSession().get(Teacher.class, teacherId);
	}

	@Override
	public List<Teacher> getTeacherList() {
		// TODO Auto-generated method stub
		return (List<Teacher>) sessionFactory.getCurrentSession().createCriteria(Teacher.class).list();
	}

}
