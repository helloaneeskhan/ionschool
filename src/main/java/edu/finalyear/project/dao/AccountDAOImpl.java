package edu.finalyear.project.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import edu.finalyear.project.models.Account;
import edu.finalyear.project.models.Head;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.models.TestAccount;
import edu.finalyear.project.security.Role;
import edu.finalyear.project.utils.ContextUtils;

@Repository("AccountDAO")
public class AccountDAOImpl implements AccountDAO {

	@Autowired
	private SessionFactory sessionF;

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils contextUtils;

	@Override
	public void saveOrUpdate(Account account) {
		// TODO Auto-generated method stub
		sessionF.getCurrentSession().saveOrUpdate(account);
	}

	@Override
	public Account getAccount(String id) {
		// TODO Auto-generated method stub
		return (Account) sessionF.getCurrentSession().get(Account.class, id);

	}

	@Override
	public Student getStudentAccount(String studentId) {
		return (Student) sessionF.getCurrentSession().get(Student.class,
				studentId);
	}

	@Override
	public Teacher getTeacherAccount(String teacherId) {
		return (Teacher) sessionF.getCurrentSession().get(Teacher.class,
				teacherId);
	}

	@Override
	public Account getAccountByIds(String schoolId, String accountId) {
		// TODO Auto-generated method stub
		Query query = sessionF.getCurrentSession().getNamedQuery(
				"findAccountByIds");
		// sessionF.getCurrentSession().createCriteria("from student").addOrder(O);
		query.setParameter("schoolId", schoolId);
		query.setParameter("id", accountId);

		Account account = (Account) query.uniqueResult();
		return account;
	}

	@Override
	public Head getHeadAccount(String headId) {
		// TODO Auto-generated method stub
		return (Head) sessionF.getCurrentSession().get(Head.class, headId);
	}

	@Override
	public Role getRole(String roleId) {
		// TODO Auto-generated method stub
		return (Role) sessionF.getCurrentSession().get(Role.class, roleId);
	}

	public List<Teacher> getTeacherAccountLike(String term) {
		Query query = sessionF.getCurrentSession().getNamedQuery(
				"findTeacherByTeacherId");
		query.setParameter("teacherId", "%" + term + "%");
		query.setParameter("schoolId", contextUtils.getContextSchoolId());
		List<Teacher> list = query.list();

		return list;
	}

	@Override
	public List<Account> getAccountLike(String term) {
		// TODO Auto-generated method stub
		Query query = sessionF.getCurrentSession().getNamedQuery(
				"findAccountByIds");
		query.setParameter("schoolId", contextUtils.getContextSchoolId());
		query.setParameter("id", "%" + term + "%");
		List<Account> list = query.list();
		return list;
	}
	
	public void saveOrUpdateObject(Object object){
		sessionF.getCurrentSession().saveOrUpdate(object);
	}

}
