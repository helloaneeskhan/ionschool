package edu.finalyear.project.dao;

import edu.finalyear.project.schoolprofile.About;
import edu.finalyear.project.schoolprofile.Items;
import edu.finalyear.project.schoolprofile.Link;
import edu.finalyear.project.schoolprofile.SchoolProfile;

public interface SchoolProfileDAO {

	public SchoolProfile getSchoolProfile(String schoolId);
	public void saveOrUpdateSchoolProfile(SchoolProfile schoolProfile);
	public void saveOrUpdateAbout(About about);
	public void saveOrUpdateLink(Link link);
	public About getAbout(String schoolId,long id);
	public void saveOrUpdateItem(Items item);

}
