package edu.finalyear.project.dao;

import java.util.List;

import edu.finalyear.project.models.School;
import edu.finalyear.project.schoolprofile.About;
import edu.finalyear.project.schoolprofile.Items;
import edu.finalyear.project.schoolprofile.Link;
import edu.finalyear.project.schoolprofile.SchoolProfile;

public interface SchoolDAO {

	public void saveOrUpdateSchool(School school);
	public School getSchool(String id);

	public SchoolProfile getSchoolProfile(String schoolId);
	public void saveOrUpdateSchoolProfile(SchoolProfile schoolProfile);
	public void saveOrUpdateAbout(About about);
	public void saveOrUpdateLink(Link link);
	public About getAbout(String schoolId,long id);
	public void saveOrUpdateItem(Items item);

	public List<SchoolProfile> getSchoolProfileLike(String arg);
	
	//	public List<School> getAllSchool();
}
