package edu.finalyear.project.dao;

import java.util.List;

import edu.finalyear.project.models.Student;

public interface StudentDAO {

	public void createStudent(Student student);
	public void updateStudent(Student student);
	public void deleteStudent(Student student);
	public Student getStudent(String studentId);
	public List<Student> getStudentList();

}
