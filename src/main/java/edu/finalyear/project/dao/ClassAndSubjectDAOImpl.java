package edu.finalyear.project.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import edu.finalyear.project.models.CompositeResult;
import edu.finalyear.project.models.Post;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Subject;
import edu.finalyear.project.models.Test;
import edu.finalyear.project.models.TestRecord;
import edu.finalyear.project.utils.ContextUtils;

@Repository("ClassAndSubjectDAO")
public class ClassAndSubjectDAOImpl implements ClassAndSubjectDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils contextUtils;

	@Override
	public SchoolClass getSchoolClass(String classId,String schoolId) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().getNamedQuery(
				"findSchoolClassByClassId");
		query.setParameter("classId", classId);
		query.setParameter("schoolId", schoolId);
		SchoolClass schoolClass = (SchoolClass) query.uniqueResult();
		return schoolClass;
	}

	@Override
	public Set<SchoolClass> getSchoolClassList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Subject getSubject(String subjectId) {
		// TODO Auto-generated method stub
		return (Subject) sessionFactory.getCurrentSession().get(Subject.class,
				subjectId);
	}

	@Override
	public Set<Subject> getSubjectsListByClassId(String classId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void newPost(Post post) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(post);
	}

	@Override
	public void saveOrUpdateClass(Object object) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(object);
	}

	@Override
	public List<SchoolClass> getSchoolClassLike(String classId) {
		// TODO Auto-generated method stub
		Query query = sessionFactory.getCurrentSession().getNamedQuery(
				"findSchoolClassByClassId");
		query.setParameter("classId", "%"+classId+"%");
		System.out.println("CSDaoImpl/contextSchoolId = "+contextUtils.getContextSchoolId());
		query.setParameter("schoolId", contextUtils.getContextSchoolId());
		List<SchoolClass> schoolClass = query.list();
		return schoolClass;
	}

	@Override
	public void saveOrUpdate(Test test) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(test);
	}

	@Override
	public Test getTest(long id) {
		// TODO Auto-generated method stub
		return (Test) sessionFactory.getCurrentSession().get(Test.class, id);
	}

	@Override
	public Test getEnableTest(boolean enable, String subjectId) {
		// TODO Auto-generated method stub
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Test.class);
		criteria.add(Restrictions.eq("on",true));
		criteria.add(Restrictions.eq("subjectId", subjectId));
		criteria.addOrder(Order.asc("id"));
		
//		Query query = sessionFactory.getCurrentSession().getNamedQuery("getEnabledTest");
//		query.setParameter("on", enable);
//		query.setParameter("subjectId",subjectId);
		Test test = (Test) criteria.uniqueResult();
		List<Test> tests = (List<Test>) criteria.list();
		
		
		return tests.get(0);
	}

	@Override
	public TestRecord getTestRecordOnTestId(String studentId,String dateId, long testId) {
		// TODO Auto-generated method stub
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TestRecord.class);
		criteria.add(Restrictions.eq("testId",testId));
		criteria.add(Restrictions.eq("studentId", studentId));
		criteria.add(Restrictions.eq("dateId",dateId));
		TestRecord testRecord = (TestRecord) criteria.uniqueResult();
		
		
//		Query query = sessionFactory.getCurrentSession().getNamedQuery("getTestRecordOnTestId");
//		query.setParameter("testId",testId);
//		TestRecord testRecord = (TestRecord) query.uniqueResult();
		return testRecord;
	}

	@Override
	public List<TestRecord> getTestRecords(String subjectId, long testId) {
		// TODO Auto-generated method stub
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TestRecord.class);
		criteria.add(Restrictions.eq("subjectId",subjectId));
		criteria.add(Restrictions.eq("testId",testId));
		
		return ((List<TestRecord>) criteria.list());
	}

	@Override
	public List<TestRecord> getTestRecordsOnStuAndSubId(String subjectId,
			String studentId) {
		// TODO Auto-generated method stub

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(TestRecord.class);
		criteria.add(Restrictions.eq("subjectId",subjectId));
		criteria.add(Restrictions.eq("studentId",studentId));
		
		return ((List<TestRecord>) criteria.list());
	}

	@Override
	public void deleteObject(Object object) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(object);
	}

	@Override
	public CompositeResult getCompositeResult(String subjectId, String studentId) {
		// TODO Auto-generated method stub
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(CompositeResult.class);
		criteria.add(Restrictions.eq("studentId",studentId));
		criteria.add(Restrictions.eq("subjectId",subjectId));
		CompositeResult result = (CompositeResult) criteria.uniqueResult();
		return result;
	}

	@Override
	public List<SchoolClass> getSchoolClassVIASchoolId(String schoolId) {
		// TODO Auto-generated method stub
	Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SchoolClass.class);
	criteria.add(Restrictions.eq("schoolId",schoolId));
	List<SchoolClass> classes = criteria.list();
		return classes;
	}

	@Override
	public List<Subject> getAllSubjectsVIASchoolId(String schoolId) {
		// TODO Auto-generated method stub
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Subject.class);
		criteria.add(Restrictions.eq("schoolId",schoolId));
		List<Subject> list = criteria.list();
		return list;
	}
	
	

}
