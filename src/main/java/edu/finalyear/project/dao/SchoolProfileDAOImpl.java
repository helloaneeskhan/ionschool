package edu.finalyear.project.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import edu.finalyear.project.schoolprofile.About;
import edu.finalyear.project.schoolprofile.Items;
import edu.finalyear.project.schoolprofile.Link;
import edu.finalyear.project.schoolprofile.SchoolProfile;

@Repository("SchoolProfileDAO")
@Transactional
public class SchoolProfileDAOImpl implements SchoolProfileDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	public SchoolProfile getSchoolProfile(String schoolId) {
		// TODO Auto-generated method stub
		SchoolProfile schoolProfile = (SchoolProfile) sessionFactory.getCurrentSession().get(SchoolProfile.class, schoolId);
		return schoolProfile;
	}

	
	@Transactional
	public void saveOrUpdateSchoolProfile(SchoolProfile schoolProfile) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(schoolProfile);
	}


	@Override
	public void saveOrUpdateAbout(About about) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(about);
	}


	@Override
	public void saveOrUpdateLink(Link link) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(link);
	}


	@Override
	public About getAbout(String schoolId, long id) {
		// TODO Auto-generated method stub
		return (About) sessionFactory.getCurrentSession().get(About.class, new Long(id));
	}


	@Override
	public void saveOrUpdateItem(Items item) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(item);
	}

	
}
