package edu.finalyear.project.dao;

import java.util.List;

import edu.finalyear.project.models.Account;
import edu.finalyear.project.models.Head;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.models.TestAccount;
import edu.finalyear.project.security.Role;

public interface AccountDAO {
	public void saveOrUpdate(Account account);

	public Account getAccount(String id);

	public Student getStudentAccount(String studentId);

	public Teacher getTeacherAccount(String teacherId);

	public Account getAccountByIds(String schoolId, String accountId);

	public Head getHeadAccount(String headId);

	public Role getRole(String roleId);

	public List<Teacher> getTeacherAccountLike(String term);
	
	public List<Account> getAccountLike(String term);
	
	public void saveOrUpdateObject(Object object);
}
