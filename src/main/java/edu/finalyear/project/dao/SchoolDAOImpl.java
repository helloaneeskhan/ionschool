package edu.finalyear.project.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import edu.finalyear.project.models.School;
import edu.finalyear.project.schoolprofile.About;
import edu.finalyear.project.schoolprofile.Items;
import edu.finalyear.project.schoolprofile.Link;
import edu.finalyear.project.schoolprofile.SchoolProfile;

@Repository("SchoolDAO")
public class SchoolDAOImpl implements SchoolDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void saveOrUpdateSchool(School school) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(school);
	}

	@Override
	public School getSchool(String id) {
		// TODO Auto-generated method stub
		org.hibernate.Query query = sessionFactory.getCurrentSession()
				.getNamedQuery("findBySchoolId");// get(School.class,new
													// Integer(id));
		query.setParameter("schoolId", id);
		School school = (School) query.uniqueResult();
		return school;

	}

	@Transactional
	public SchoolProfile getSchoolProfile(String schoolId) {
		// TODO Auto-generated method stub
		SchoolProfile schoolProfile = (SchoolProfile) sessionFactory
				.getCurrentSession().get(SchoolProfile.class, schoolId);
		return schoolProfile;
	}

	@Transactional
	public void saveOrUpdateSchoolProfile(SchoolProfile schoolProfile) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(schoolProfile);
	}

	@Override
	public void saveOrUpdateAbout(About about) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(about);
	}

	@Override
	public void saveOrUpdateLink(Link link) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(link);
	}

	@Override
	public About getAbout(String schoolId, long id) {
		// TODO Auto-generated method stub
		return (About) sessionFactory.getCurrentSession().get(About.class,
				new Long(id));
	}

	@Override
	public void saveOrUpdateItem(Items item) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(item);
	}

	@Override
	public List<SchoolProfile> getSchoolProfileLike(String arg) {
		// TODO Auto-generated method stub

//		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SchoolProfile.class);
//		criteria.add(Restrictions.like("schoolId",arg));
//		return criteria.list();		

		
		org.hibernate.Query query = sessionFactory.getCurrentSession()
				.getNamedQuery("findSchoolProfileBySchoolId");
		 
		query.setParameter("schoolId", "%"+arg+"%");
		return query.list();

	}

}
