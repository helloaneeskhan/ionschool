package edu.finalyear.project.utils;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import edu.finalyear.project.service.AccountService;
import edu.finalyear.project.service.SchoolService;

@Component
public class TestServices {

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;

	@Autowired
	@Qualifier("SchoolService")
	private SchoolService schoolService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		System.out.println("Testing/test: ");
		System.out.println("Student = "+accountService.getAccount("teacher_test"));
		System.out.println("School = "+schoolService.getSchool("APS"));
		assertTrue("true", true);
//		fail("Not yet implemented");
	}

}
