package edu.finalyear.project.utils;

public interface ConstantValues {
	public static final String EVENT = "event";
	public static final String ABOUT = "about";
	public static final String TEACHERS = "teacher";
	public static final String GALLERY = "gallery";
	public static final String CONTACT = "contact";
	
}
