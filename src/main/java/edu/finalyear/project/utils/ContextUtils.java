package edu.finalyear.project.utils;

import java.util.Set;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import edu.finalyear.project.models.School;
import edu.finalyear.project.models.SchoolClass;
import edu.finalyear.project.models.Student;
import edu.finalyear.project.models.Teacher;
import edu.finalyear.project.security.SecurityUtils;
import edu.finalyear.project.service.AccountService;
import edu.finalyear.project.service.ClassAndSubjectService;
import edu.finalyear.project.service.SchoolService;
import static edu.finalyear.project.security.SecurityUtils.*;

@Component("ContextSetting")
public class ContextUtils {

	@Autowired
	private ServletContext context;

	@Autowired
	@Qualifier("SchoolService")
	private SchoolService schoolService;

	@Autowired
	@Qualifier("ClassAndSubjectService")
	private ClassAndSubjectService csService;

	@Autowired
	@Qualifier("AccountService")
	private AccountService accountService;

	public boolean isSchoolExists() {
		return context.getAttribute("context_school") == null ? false : true;
	}

	// /////////////For Better and Anonymous//////////////
	public void setContextSchoolId(String schoolId) {
		context.setAttribute("context_schoolId", schoolId);
	}

	public String getContextSchoolId() {
		return (String) SecurityUtils.getLogedInAccount().getSchoolId();
	}

	// /////////////End For Better and Anonymous//////////////
	public School getContextSchool() {
		if (isSchoolExists())
			return (School) context.getAttribute("context_school");
		else
			return new School("********", "Null");
	}

	public void setContextSchoolById(String schoolId) {
		School school = schoolService.getSchool(schoolId);
		context.removeAttribute("context_school");
		context.setAttribute("context_school", school);
		System.out.println("ContextUtils: setContextSchool(), contextSchoolId="
				+ schoolId);

	}

	// @PreAuthorize("hasRole('ROLE_STUDENT')")
	// public void setStudentContextSetting() {
	// setContextSchoolById(getLogedInAccount().getSchoolId());
	// Student student = accountService.getStudentAccount(getLogedInAccount()
	// .getId());
	// System.out.println("ContextUtils:/setContextStudent=> level 1");
	// context.setAttribute("context_student", student);
	// System.out.println("ContextUtils:/setContextStudent=> level 2");
	// context.setAttribute("context_subjects",
	// csService.getSchoolClass(student.getClassId()).getSubjects());
	// System.out.println("ContextUtils:/setContextStudent=> level 3");
	// }
	//
	// @PreAuthorize("hasRole('ROLE_STUDENT')")
	// public void removeStudentContextSetting() {
	// context.removeAttribute("context_subjects");
	// context.removeAttribute("context_student");
	// }
	//
	// @PreAuthorize("hasRole('ROLE_TEACHER')")
	// public void setTeacherContextSetting() {
	// setContextSchoolById(getLogedInAccount().getSchoolId());
	// System.out.println("ContextUtils: setContextTeacher");
	// // context.setAttribute("context_teacher", teacher);
	// }
	//
	// @PreAuthorize("hasRole('ROLE_TEACHER')")
	// public void removeTeacherContextSetting() {
	//
	// }

	// public void setInitialContextSetting() {
	// setStudentContextSetting();
	// setTeacherContextSetting();
	// }
	//
	// public void removeInitialContextSetting() {
	// removeStudentContextSetting();
	// removeTeacherContextSetting();
	// }

	public Student getContextStudent() {
		return (Student) context.getAttribute("context_student");
	}

	public Teacher getContextTeacher() {
		return (Teacher) context.getAttribute("context_teacher");
	}

	public void removeAttributeByName(String name) {
		context.removeAttribute(name);
	}

	// /////////////////////////////////////////////////////////////////////////////////////////////

	public static Student getStudent(String studentId, Set<Student> items) {
		for (Student x : items) {
			if (x.getId().equals(studentId))
				return x;
		}
		return null;

	}

	public static Teacher getTeacher(String teacherId, Set<Teacher> items) {

		for (Teacher x : items) {
			if (x.getId().equals(teacherId))
				return x;
		}
		return null;

	}

	public static SchoolClass getSchoolClass(String id, Set<SchoolClass> items) {

		for (SchoolClass x : items) {
			if (x.getClassId().equals(id))
				return x;
		}
		return null;

	}

}
