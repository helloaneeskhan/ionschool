package edu.finalyear.project.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("generatedValues")
public class GeneratedValues {


	@Autowired
	@Qualifier("ContextSetting")
	private ContextUtils contextUtils;

	public String getGeneratedClassOrSubjectId(String classId){
		return classId.concat("@"+contextUtils.getContextSchoolId());
	}
	
	
	public String getGeneratedAccountId(String accountId ){
		return accountId.concat("@"+contextUtils.getContextSchoolId());
	}
	
	
}
